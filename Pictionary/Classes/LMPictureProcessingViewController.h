//
//  LMPictureProcessingViewController.h
//  Pictionary
//
//  Created by Kiril Kiroski on 7/16/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMBaseViewController.h"

@interface LMPictureProcessingViewController : LMBaseViewController

@property (strong, nonatomic) NSString *photoPath;
@property (nonatomic) BOOL isLandscapeImage;

@end
