//  LMResultsViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMResultsViewController.h"


@interface LMResultsViewController ()
@end


@implementation LMResultsViewController

- (void)configureAppearance {
  [super configureAppearance];
}

- (void)configureUI {
  [super configureUI];
}

- (void)configureObservers {
  [super configureObservers];
}

- (void)configureNavigation {
  [super configureNavigation];
}

- (void)configureData {
  [super configureData];
}

- (void)loadData {
  [super loadData];
}

- (void)layout {
  [super layout];
}

- (void)dismissObservers {
  [super dismissObservers];
}
@end
