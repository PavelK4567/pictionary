//
//  LMFirstViewController.h
//  Pictionary
//
//  Created by Darko Trpevski on 11/6/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMBaseViewController.h"
#import "LMPhotoButtonCell.h"
#import "LMSearchCell.h"
#import "LMItemCell.h"
#import "MLPAutoCompleteTextField.h"
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "LMsearchDatasource.h"
#import "MLPAutoCompleteTextFieldDataSource.h"
#import "LMCaptureViewController.h"
#import "LMItemCell.h"
#import "LMDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LMSuggestionCell.h"
#import "LMSearchViewController.h"
#import "LMSettingsViewController.h"
#import "CaptureViewController.h"

@interface LMFirstViewController : LMBaseViewController<LMPhotoButtonCellDelegate,MLPAutoCompleteTextFieldDelegate, UITextFieldDelegate, MLPAutoCompleteTextFieldDataSource,LMItemCellDelegate>
{
    LMPhotoButtonCell *cameraButtonView;
    UIButton *expandButton;
    UICollectionViewCell *selectedCell;
    IBOutlet LMSearchDatasource *searchDataSource;
    IBOutlet MLPAutoCompleteTextField *_searchTextField;
    IBOutlet UIView *_deleteView;
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *containerView;
@property (strong, nonatomic) NSString *searchString;
@property (assign, nonatomic) BOOL expandedStatus;
@property (assign, nonatomic) BOOL isItemSelected;
@property (strong, nonatomic) NSMutableArray *itemsNames;
@property (strong, nonatomic) NSMutableArray *itemsNamesForSearch;
@property (strong, nonatomic) NSMutableArray *resultItemsArray;
@property (strong, nonatomic) LMItem *selectItem;

@end
