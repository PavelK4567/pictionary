//  LMItemCell.m
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//  Edited by Darko Trpevski


#import "LMItemCell.h"
#import "LMItem.h"
#import "LMTag.h"
#import "UIImageView+WebCache.h"

@interface LMItemCell (){
  UILongPressGestureRecognizer *_gestureLong;
}

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemTitle;
@property (weak, nonatomic) IBOutlet UILabel *selectedLabel;


@property (weak, nonatomic) LMItem *item;
@property (nonatomic) BOOL isSelected;
@end


@implementation LMItemCell

- (void)awakeFromNib {
	[super awakeFromNib];
	[self setupUI];
	[self setupAppearance];
}

- (void)setupUI {
}

- (void)setupAppearance {
	self.layer.cornerRadius = kCornerRadius;
	self.layer.masksToBounds = YES;
	self.layer.borderColor = [UIColor colorWithRed:0.340 green:0.076 blue:0.438 alpha:0.250].CGColor;
	self.layer.borderWidth = 0.5;
	self.clipsToBounds = YES;
	_gestureLong = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
	_gestureLong.minimumPressDuration = 1.0;
	[self addGestureRecognizer:_gestureLong];
}

- (void)configureWithModel:(LMItem *)item {
	self.item = item;
	NSString *title = 0;
	[self.itemTitle setHidden:NO];
	[self.selectedLabel setHidden:YES];
  
	for (NSString *tagUUID in item.items) {
		if (title) {
			title = [title stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] native]];
		}
		else {
			title = [[ITEM_MANAGER tagWithId:tagUUID] native];
		}
	}
	[self.itemImage sd_cancelCurrentImageLoad];
	[self.itemTitle setText:title];
	[self.itemImage setImage:[UIImage imageWithContentsOfFile:[[ITEM_MANAGER destinationPath] stringByAppendingFormat:@"/%@.png", item.imageThumb]]];
	DLog(@"ITEM\n\tLoaded title: %@\n\tLoaded image: %@", title, [[ITEM_MANAGER destinationPath] stringByAppendingFormat:@"/%@.png", item.imageThumb]);
	// add long press
	self.isSelected = NO;
}

- (void)configureWithDictionary:(NSDictionary *)dictionary {
	[self.itemTitle setText:@""];
	[self.itemTitle setHidden:YES];
	[self.selectedLabel setHidden:YES];
	NSString *imgUrlString = [dictionary objectForKey:kParserTagThumbnail];
	[self.itemImage sd_setImageWithURL:[NSURL URLWithString:imgUrlString] placeholderImage:[UIImage imageNamed:@"img_error"]];
}

#pragma mark LongPress on cell
- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
	DLog(@"handleLongPress");
 
  [self removeGestureRecognizer:_gestureLong];
  if (self.item.defaultImage) {
    [UIAlertView alertWithCause:kAlertPredefinedPictureCanBeDeleted];
    [self addGestureRecognizer:_gestureLong];
    return;
  }
	if (!self.isSelected) {
		DLog(@"select");
		//if (!self.item.defaultImage) {
    if([self.delegate canSelectItem]){
			[self.delegate selectItemCellIsSelectedWithItem:self.item];
			[self.delegate selectItemCell:self];
			[self.selectedLabel setHidden:NO];
			self.isSelected = YES;
    }else{
      [UIAlertView alertWithCause:kAlertPictureCanBeDeletedInClosedGallery];
      [self addGestureRecognizer:_gestureLong];
    }
	}
}

- (void)removeSelection {
	if (self.isSelected) {
		[self.selectedLabel setHidden:YES];
		self.isSelected = NO;
    [self addGestureRecognizer:_gestureLong];
	}
}

@end
