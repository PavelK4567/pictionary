//  LMItemCell.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@class LMItem;

@protocol LMItemCellDelegate <NSObject>

- (void)selectItemCellIsSelectedWithItem:(LMItem *)selectItem;
- (void)selectItemCell:(UICollectionViewCell *)selectCell;
- (BOOL)canSelectItem;
@end


@interface LMItemCell : UICollectionViewCell

@property (assign, nonatomic) id <LMItemCellDelegate> delegate;

- (void)configureWithModel:(LMItem *)model;
- (void)configureWithDictionary:(NSDictionary *)dictionary;
- (void)removeSelection;
@end
