//  LMLoginCodeViewController.h
//  Created by Dimitar Tasev on 02.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMBaseViewController.h"


@interface LMLoginCodeViewController : LMBaseViewController

@property(strong, nonatomic) NSString *msisdn;

@end
