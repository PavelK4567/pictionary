//  LMSearchViewController.m
//  Created by Dimitar Tasev on 09.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//  Updated by Darko Trpevski on 14.11.2014


#import "LMSearchViewController.h"
#import "LMDetailViewController.h"

@implementation LMSearchViewController

static NSString *resultCellIdentifier = @"ResultCellIdentifier";
static NSString *itemCellIdentifier = @"ItemCellIdentifier";


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:_collectionView.backgroundColor];
}

#pragma mark - Layout

- (void)layout
{

    [super layout];
}


#pragma mark - LoadSearchTextField

- (void)loadSearchTextField
{
    _searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, 310, 30)];
    _searchTextField.backgroundColor = [UIColor whiteColor];
    _searchTextField.returnKeyType = UIReturnKeyGo;
    _searchTextField.placeholder = Localized(@"T070");
    _searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _searchTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchTextField.delegate = self;
    _searchTextField.text = _searchString;
    
    [_searchTextField setLeftView:[self searchIconLabel]];
    [_searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_searchTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    [self.view addSubview:_searchTextField];
    
    if(self.searchType == ImagesFromPhone)
        [_searchTextField setEnabled:NO];
    else
        [_searchTextField setEnabled:YES];
}

#pragma mark - LoadMessageLabel

- (void)loadMessageLabel
{
    NSString *messagetText = (self.searchType == ImagesFromGoogle) ? messagetText = [NSString stringWithFormat:Localized(@"T113"),self.items.count] : [NSString stringWithFormat:Localized(@"T114"),self.items.count];
    
    _messageLabel = [[UILabel alloc] initWithFrame:_searchTextField.frame];
    _messageLabel.numberOfLines = 2;
    _messageLabel.textAlignment = NSTextAlignmentCenter;
    _messageLabel.textColor = [UIColor grayColor];
    
    [_messageLabel setFont:[UIFont fontWithName:@"Noteworthy-Light" size:17.0]];
    [_messageLabel setText:messagetText];
    [_messageLabel sizeToFit];

    [self.view addSubview:_messageLabel];
}


#pragma mark - ConfigureUI

- (void)configureUI
{
    [super configureUI];
    
    [self loadSearchTextField];
    [self loadMessageLabel];
    
    [_messageLabel placeCenterAndBelowView:_searchTextField withPadding:1.0];
    _collectionView.frame = CGRectMake(0, _messageLabel.height, 320.0, self.view.height -(_messageLabel.height + _messageLabel.y));

    [_collectionView placeBelowView:_messageLabel withPadding:kViewsPadding * 2];

    [_collectionView registerClass:[LMResultCell class] forCellWithReuseIdentifier:resultCellIdentifier];
    [_collectionView registerNib:[UINib nibWithNibName:@"LMResultCell" bundle:0] forCellWithReuseIdentifier:resultCellIdentifier];
    [_collectionView registerClass:[LMItemCell class] forCellWithReuseIdentifier:itemCellIdentifier];
    [_collectionView registerNib:[UINib nibWithNibName:@"LMItemCell" bundle:0] forCellWithReuseIdentifier:itemCellIdentifier];
}


#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = (self.searchType == ImagesFromGoogle) ? CGSizeMake(100,100) : CGSizeMake(310,100);
    
    return cellSize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if(self.searchType == ImagesFromPhone)
    {
        cell = (LMResultCell *)[collectionView dequeueReusableCellWithReuseIdentifier:resultCellIdentifier forIndexPath:indexPath];
        if(!cell)
            cell = [LMResultCell getFromNib];
        [(LMResultCell *)cell configureWithModel:[self.items objectAtIndex:indexPath.item]];
    }
    else
    {
        cell = (LMItemCell *)[collectionView dequeueReusableCellWithReuseIdentifier:itemCellIdentifier forIndexPath:indexPath];
        if(!cell)
            cell = [LMItemCell getFromNib];
        [(LMItemCell *)cell configureWithDictionary:(NSDictionary *)[self.items objectAtIndex:indexPath.item]];
    }
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.searchType == ImagesFromPhone)
    {
        LMDetailViewController *controler = [LMDetailViewController new];
        [controler configureWithItem:[self.items objectAtIndex:indexPath.item]];
        [self.navigationController pushViewController:controler animated:YES];
    }
    else
    {
        NSDictionary *object = [self.items objectAtIndex:indexPath.item];
        NSString *result = [object objectForKey:kParserTagOriginalImage];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:result]];
        UIImage *image = [UIImage imageWithData:imageData];
        [image resizedImageWithMinimumSize:CGSizeMake(640, 640)];
        LMItem *item = [ITEM_MANAGER makeItemWithImage:image withTags:self.tagItems isLandscapeImage:NO];
        LMDetailViewController *controler = [LMDetailViewController new];
        [controler configureWithItem:item];
        [controler addSaveButton];
        
        [self.navigationController pushViewController:controler animated:YES];
    }
}


#pragma mark - Setters

- (void)setItems:(NSArray *)items
{
    _items = items;
    [_collectionView reloadData];
}

#pragma mark - SearchIconLabel

- (UILabel *)searchIconLabel
{
    UILabel *searchIcon = [UILabel new];
    
    [searchIcon setText:[[NSString alloc] initWithUTF8String:"  \xF0\x9F\x94\x8D"]];
    [searchIcon sizeToFit];
    
    return searchIcon;
}


#pragma mark - UITextField delegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if ([USER_MANAGER canGoogle])
  {
    [searchDatasource getGoogleImagesForString:textField.text withCompletitionBlock:^(NSArray *result, NSError *error)
     {
       self.items = result;
       self.tagItems = searchDatasource.apiResultsTagsArray;
       [_collectionView reloadData];
     }];
  }
  else
  {
    if ([USER_MANAGER getUserStatus] == KAUserGuest)
    {
      [UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForGuestUser];
    }
    else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
    {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];

    }
    else if ([USER_MANAGER getUserStatus] == KAUserStatusActive)
    {
      [UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForActiveUser];
    }
  }

  [textField resignFirstResponder];
  
  return YES;
}

#pragma mark -
@end
