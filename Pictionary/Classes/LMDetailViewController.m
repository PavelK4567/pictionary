//  LMDetailViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMDetailViewController.h"
#import "LMItem.h"
#import "LMTag.h"

@interface LMDetailViewController ()

@property IBOutlet UIButton *shareButton;
@property IBOutlet UIButton *translateButton;
@property IBOutlet UIButton *saveButton;
@property BOOL haveSaveButton;
@property (nonatomic, strong) IBOutlet UIImageView *photoImageView;
@property (nonatomic, strong) UIImageView *firstBubbleImageView;
@property (nonatomic, strong) UIImageView *secondBubbleImageView;
@property (nonatomic, strong) UIImageView *helpImageView;
@property (nonatomic, strong) IBOutlet UILabel *itemInfoLabel;
@property (nonatomic, strong) IBOutlet UIScrollView *itemInfoScrollView;
@property (nonatomic, strong) LMItem *item;
@property (nonatomic, strong) NSMutableArray *localizedWords;

@end


@implementation LMDetailViewController

- (void)configureAppearance {
	[super configureAppearance];
	self.view.backgroundColor = [APPSTYLE otherViewColorForKey:@"View_Background"];
	[self setImageAndText];
}

- (void)configureUI {
	[super configureUI];
  
	[APPSTYLE applyStyle:@"Detail_Inf_Title" toLabel:self.itemInfoLabel];
	[self.shareButton setHidden:(!self.item.canShare)];
}

- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
  self.navigationItem.title = Localized(@"T069");
}

- (void)configureData {
	[super configureData];
}

- (void)loadData {
	[super loadData];
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
}

#pragma mark - configure Interface

- (void)configureWithItem:(LMItem *)item {
	self.item = item;
}

- (void)addSaveButton {
	self.haveSaveButton = YES;
}

#pragma mark - Interface Builder Actions
- (IBAction)saveButtonTapped:(UIButton *)sender {
	[self doneAction];
}

- (IBAction)shareButtonTapped:(UIButton *)sender {
	[self share];
}

- (IBAction)translateButtonTapped:(UIButton *)sender {
	if (self.firstBubbleImageView.alpha == 0.0) {
		[self translateShowButtonTapped];
	}
	else {
		[self translateHideButtonTapped];
	}
  
	/*[UIView animateWithDuration:0.4
   delay:0
   options:0
   animations: ^{
   if (self.firstBubbleImageView.alpha == 0.0) {
   self.firstBubbleImageView.alpha = 1.0;
   self.secondBubbleImageView.alpha = 1.0;
   self.firstBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(1, 1);
   self.secondBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(1, 1);
   }
   else {
   self.firstBubbleImageView.alpha = 0.0;
   self.secondBubbleImageView.alpha = 0.0;
   self.firstBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
   self.secondBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
   }
   }
   
   completion: ^(BOOL finished) {}];*/
}

- (void)translateShowButtonTapped {
	[UIView animateWithDuration:0.4
	                      delay:0
	                    options:0
	                 animations: ^{
                     self.firstBubbleImageView.alpha = 1.0;
                     self.helpImageView.alpha = 1.0;
                     self.helpImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(1, 1);
                     self.firstBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(1, 1);
                   }
   
	                 completion: ^(BOOL finished) {
                     [UIView animateWithDuration:0.4
                                           delay:0
                                         options:0
                                      animations: ^{
                                        self.secondBubbleImageView.alpha = 1.0;
                                        self.secondBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(1, 1);
                                      }
                      
                                      completion: ^(BOOL finished) {}];
                   }];
}

- (void)translateHideButtonTapped {
	[UIView animateWithDuration:0.4
	                      delay:0
	                    options:0
	                 animations: ^{
                     self.firstBubbleImageView.alpha = 0.0;
                     self.secondBubbleImageView.alpha = 0.0;
                     self.helpImageView.alpha = 0.0;
                     self.firstBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
                     self.secondBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
                     self.helpImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
                   }
   
	                 completion: ^(BOOL finished) {}];
}

- (IBAction)cameraButtonTapped:(UIButton *)sender {
}
- (IBAction)googleTranslateButtonTapped:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://translate.google.com"]];
}

#pragma mark - Set Info Text Position


- (void)setImageAndText {
	[self.saveButton setHidden:(!self.haveSaveButton)];
	NSString *title = 0;
	NSString *localizedString = 0;
	for (NSString *tagUUID in self.item.items) {
		if (title) {
			title = [title stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] native]];
			localizedString = [localizedString stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] localized]];
		}
		else {
			title = [[ITEM_MANAGER tagWithId:tagUUID] native];
			localizedString = [[ITEM_MANAGER tagWithId:tagUUID] localized];
		}
	}
	[self takeLocalizedWords:localizedString];
	[self.itemInfoLabel setText:title];
	//[self.itemInfoLabel setText:@"Background Enciklopedia"];
   
    UIImage *tempImage = [UIImage imageWithContentsOfFile:[[ITEM_MANAGER destinationPath] stringByAppendingFormat:@"/%@.png", self.item.imageThumb]];
    
    UIImage *imageToDisplay = [UIImage imageWithCGImage:[tempImage CGImage]
                                                  scale:1.0
                                            orientation:self.item.isLandscapeImage ? UIImageOrientationLeft : UIImageOrientationUp];
    
    [LMHelper removeImagesFromDocumentsFolder];
	[self.photoImageView setImage:imageToDisplay];
	[self setTextFramesAndPosition];
}

- (void)setTextFramesAndPosition {
	self.itemInfoLabel.backgroundColor = [UIColor clearColor];
	[self.itemInfoLabel sizeToFit];
	[self.itemInfoLabel layoutIfNeeded];
  

	self.itemInfoScrollView.contentSize = CGSizeMake(320, 0);
  
	CGRect viewFrame = self.itemInfoLabel.frame;
	viewFrame.origin.x = (self.itemInfoScrollView.contentSize.width - viewFrame.size.width) / 2;
	self.itemInfoLabel.frame = viewFrame;
		self.firstBubbleImageView = [[UIImageView alloc]
	                             initWithFrame:CGRectMake((self.itemInfoScrollView.contentSize.width - viewFrame.size.width) / 2, viewFrame.origin.y - viewFrame.size.height, 75, 32)];
	self.helpImageView =
  [[UIImageView alloc] initWithFrame:CGRectMake((self.itemInfoScrollView.contentSize.width - 25) / 2, viewFrame.origin.y - viewFrame.size.height, 25, 32)];
	[self.helpImageView setImage:[UIImage imageNamed:@"img_bubble_up"]];
	[self.itemInfoScrollView addSubview:self.helpImageView ];
	[self.itemInfoScrollView addSubview:self.firstBubbleImageView];
	[self.itemInfoScrollView addSubview:self.secondBubbleImageView];

	[self.firstBubbleImageView setImage:[[UIImage imageNamed:@"img_bubble_full"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0]];
	[self.secondBubbleImageView setImage:[[UIImage imageNamed:@"img_bubble_up"] stretchableImageWithLeftCapWidth:15.0 topCapHeight:0.0]];
	///////
	[self setBubbleText1];
	[self.secondBubbleImageView setHidden:YES];

	self.firstBubbleImageView.alpha = 0.0;
	self.secondBubbleImageView.alpha = 0.0;
	self.helpImageView.alpha = 0.0;
	self.firstBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
	self.secondBubbleImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
  self.helpImageView.viewForBaselineLayout.transform = CGAffineTransformMakeScale(0, 0);
}

- (void)setBubbleText1 {
	CGRect commentTextViewFrame = CGRectMake(5, -2, 200, 5);
	UITextView *firstText = [[UITextView alloc] initWithFrame:commentTextViewFrame];
	[APPSTYLE applyStyle:@"Detail_Localized_popups" toTextView:firstText];
	[self.firstBubbleImageView addSubview:firstText];
	firstText.userInteractionEnabled = NO;
	firstText.backgroundColor = [UIColor clearColor];
	// firstText.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0);
	firstText.text = self.item.localizedStringReverse; //[self.localizedWords objectAtIndex:0];
  
	[firstText sizeToFit];
	[firstText layoutIfNeeded];
  
  
	CGRect viewFrame = self.firstBubbleImageView.frame;
	viewFrame.size.width = firstText.contentSize.width + 10;
	viewFrame.origin.x = (self.itemInfoScrollView.contentSize.width - viewFrame.size.width) / 2;
	self.firstBubbleImageView.frame = viewFrame;
}

- (void)setBubbleText2 {
	CGRect commentTextViewFrame = CGRectMake(5, 0, 200, 5);
	UITextView *firstText = [[UITextView alloc] initWithFrame:commentTextViewFrame];
	[APPSTYLE applyStyle:@"Detail_Localized_popups" toTextView:firstText];
	[self.secondBubbleImageView addSubview:firstText];
	firstText.userInteractionEnabled = NO;
	firstText.backgroundColor = [UIColor clearColor];
	firstText.contentInset = UIEdgeInsetsMake(-4, 0, 0, 0);
	firstText.text = [self.localizedWords objectAtIndex:1];
	[firstText sizeToFit];
	[firstText layoutIfNeeded];
  
  
	CGRect viewFrame = self.secondBubbleImageView.frame;
	viewFrame.size.width = firstText.contentSize.width + 10;
	// viewFrame.origin.x = viewFrame.origin.x - (firstText.contentSize.width / 2) + 10;
	viewFrame.origin.x = (viewFrame.size.width - viewFrame.origin.x) / 2;
	self.secondBubbleImageView.frame = viewFrame;
	self.secondBubbleImageView.alpha = 0.0;
}

#pragma mark - Set localized words
- (void)takeLocalizedWords:(NSString *)localizedString {
	self.localizedWords = [NSMutableArray new];
	NSArray *tempWords = [localizedString componentsSeparatedByString:@" "];
	for (NSString *word in tempWords) {
		[self.localizedWords addObject:[word lowercaseString]];
	}
  
}

#pragma mark - done Action
- (void)doneAction {
	if ([ITEM_MANAGER addItem:self.item]) {
    
		//[UIAlertView alertWithCause:kItemSavedSuccessfullyConfirmation];
		//[self.navigationController popToRootViewControllerAnimated:YES];
    [APP_DELEGATE buildMainStack];
    
	}
	else {
		[UIAlertView alertWithCause:kItemSavedFailConfirmation];
	}
}

#pragma mark - share Action
- (void)share {
	NSString *info = Localized(@"T105");
	UIImage *shareImage = self.photoImageView.image; //[UIImage imageNamed:@"captech-logo.jpg"];
	// NSURL *shareUrl = [NSURL URLWithString:@"http://www.kanto.com"];
	NSArray *activityItems = [NSArray arrayWithObjects:info, shareImage, nil];
	// NSArray *activityItems = [NSArray arrayWithObjects:info, shareUrl, nil];
	UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
	/* NSArray *excludedActivities = @[UIActivityTypePrint,UIActivityTypeMail,UIActivityTypeCopyToPasteboard,
   UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
   UIActivityTypePostToWeibo,
   UIActivityTypeMessage, UIActivityTypeMail,
   UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
   UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
   UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
   UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
	 */
	controller.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
	[self presentViewController:controller animated:YES completion:nil];
}

@end
