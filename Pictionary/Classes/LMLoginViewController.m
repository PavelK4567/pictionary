//  LMLoginViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMLoginViewController.h"
#import "LMLoginCodeViewController.h"
#import "LMBuyInAppViewController.h"

@interface LMLoginViewController () {
  IBOutlet UIView *subscriptionPopUpView;
  UIView *subscriptionBlackView;
  IBOutlet UITextView *subscriptionExplanationText, *subscriptionIntroductionText;
  IBOutlet UILabel *subscriptionExplanationLabel, *subscriptionIntroductionLabel;
  IBOutlet UIButton *subscriptionPopUpCancelButton, *subscriptionPopUpsubscribeButton;
  IBOutlet UILabel *subscriptionPopUpTitle;
  IBOutlet UILabel *subscriptionExpiredLabel;
  UITapGestureRecognizer *tap;
}

@property(weak, nonatomic) IBOutlet KeyboardAvoidingScrollView *contentScroll;
@property(weak, nonatomic) IBOutlet UIView *contentView;
@property(weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property(weak, nonatomic) IBOutlet UILabel *userLoginTitleLabel;
@property(weak, nonatomic) IBOutlet UIView *userPhoneNumberView;
@property(weak, nonatomic) IBOutlet UITextField *userPhonePrefixField;
@property(weak, nonatomic) IBOutlet UITextField *userPhoneNumberField;
@property(weak, nonatomic) IBOutlet UIButton *userLoginButton;
@property(weak, nonatomic) IBOutlet UILabel *infoLabe;
@property(weak, nonatomic) IBOutlet UIView *separatorView;
@property(weak, nonatomic) IBOutlet UILabel *guestLoginTitleLabel;
@property(weak, nonatomic) IBOutlet UIButton *guestLoginButton;
@property(weak, nonatomic) IBOutlet UIButton *inAppButton;
@property(weak, nonatomic) IBOutlet UIButton *restoreButton;


- (IBAction)userLoginTapped:(id)sender;
- (IBAction)guestLoginTapped:(id)sender;

@end


@implementation LMLoginViewController

- (id)initWithNibName:(NSString *)nib bundle:(NSBundle *)budnle
{
  self = [super initWithNibName:nib bundle:budnle];
  if (self) {
    self.title = Localized(@"Login");
  }
  return self;
}

- (void)configureAppearance
{
  [super configureAppearance];

  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.userLoginButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.guestLoginButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.inAppButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.restoreButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyStyle:@"Button_Login_Page" toButton:subscriptionPopUpsubscribeButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.userLoginButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.guestLoginButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.inAppButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.restoreButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:subscriptionPopUpsubscribeButton];

  [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:self.userLoginTitleLabel];
  [APPSTYLE applyStyle:@"Login_Label2_Body" toLabel:self.guestLoginTitleLabel];
  [APPSTYLE applyStyle:@"Login_Label2_Body" toLabel:self.infoLabe];
  [APPSTYLE applyStyle:@"Login_Title" toLabel:subscriptionPopUpTitle];
  // subscriptionPopUpTitle.textColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  [APPSTYLE applyStyle:@"Login_Label2_Body" toLabel:subscriptionExplanationLabel];


  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhonePrefixField];
  [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:self.userPhoneNumberField];
}

- (void)configureUI
{
  [super configureUI];
  [self.contentScroll addSubview:self.contentView];
  [self.contentScroll setContentSize:self.contentView.frame.size];
  [self.contentScroll contentSizeToFit];

  //[self.userLoginButton setCornerRadius:2.0];
  //[self.guestLoginButton setCornerRadius:2.0];

  [self.userPhonePrefixField setCornerRadius:4.0];
  [self.userPhonePrefixField setBorder:1.0 color:[UIColor blackColor]];
  [self.userPhoneNumberField setCornerRadius:4.0];
  [self.userPhoneNumberField setBorder:1.0 color:[UIColor blackColor]];

  self.userLoginTitleLabel.text = Localized(@"T001");
  self.guestLoginTitleLabel.text = Localized(@"T004");
  [APPSTYLE applyTitle:Localized(@"T002") toButton:self.userLoginButton];
  [APPSTYLE applyTitle:Localized(@"T005") toButton:self.guestLoginButton];

  [APPSTYLE applyTitle:Localized(@"T003.3") toButton:self.inAppButton];
  [APPSTYLE applyTitle:Localized(@"T018") toButton:subscriptionPopUpCancelButton];
  [APPSTYLE applyTitle:Localized(@"T017") toButton:subscriptionPopUpsubscribeButton];

  subscriptionExplanationText.text = Localized(@"T016");
  subscriptionExplanationLabel.text = Localized(@"T016");
  subscriptionPopUpTitle.text = Localized(@"T015");


  NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];


  [self.restoreButton setHidden:YES];
  [self.restoreButton setEnabled:NO];
  [self.inAppButton setHidden:NO];
  [self.inAppButton setEnabled:YES];
  self.guestLoginTitleLabel.text = Localized(@"T003.1");
  self.infoLabe.text = Localized(@"T003.2");
  if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
    [APPSTYLE applyTitle:Localized(@"T003.4") toButton:self.inAppButton];
    self.infoLabe.text = [NSString stringWithFormat:Localized(@"T010.b"), [USER_MANAGER takeNativeDateStringFromDate]];
  }
  ///////
  if ([self youHaveRestoreData]) {

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"youHaveRestoreDataChek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.restoreButton setHidden:NO];
    [self.restoreButton setEnabled:YES];
    [self.inAppButton setHidden:YES];
    [self.inAppButton setEnabled:NO];

    self.guestLoginTitleLabel.text = Localized(@"T003.5");
    self.infoLabe.text = Localized(@"T003.6");
    [APPSTYLE applyTitle:Localized(@"T003.7") toButton:self.restoreButton];
  } else {
    BOOL firsTimeCloudChek = [[NSUserDefaults standardUserDefaults] boolForKey:@"firsTimeCloudChek"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firsTimeCloudChek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (!firsTimeCloudChek) {
      [self addActivityIndicator];
      [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(chekCloudData) userInfo:nil repeats:NO];
    }
  }
}
- (void)chekCloudData
{
  [self removeActivityIndicator];
  NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:kMonthlyPurchaseId];
  NSLog(@"chekCloudData>>> cloud_uuid %@", cloud_uuid);
  NSLog(@"chekCloudData>>> objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  NSLog(@"chekCloudData>>>kMonthlyPurchaseId %@ ", [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]]);
  if ([self youHaveRestoreData]) {
    [self.restoreButton setHidden:NO];
    [self.restoreButton setEnabled:YES];
    [self.inAppButton setHidden:YES];
    [self.inAppButton setEnabled:NO];

    self.guestLoginTitleLabel.text = Localized(@"T003.5");
    self.infoLabe.text = Localized(@"T003.6");
    [APPSTYLE applyTitle:Localized(@"T003.7") toButton:self.restoreButton];
  }
}
- (BOOL)youHaveRestoreData
{
  if ([USER_MANAGER isGuest]) {
    BOOL youHaveRestoreDataBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"youHaveRestoreDataChek"];
    return youHaveRestoreDataBefore;
  }
  if (![USER_MANAGER getUserStatus]) {
    NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
    NSLog(@" youHaveRestoreData uid_icloud:%@ ", uid_icloud);
    if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
      return YES;
    }
  } else {
    if (![USER_MANAGER isNative]) {
      BOOL youMakeNativeUserBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"makeNativeUser"];
      if(youMakeNativeUserBefore)
      {
        return NO;
      }
      NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
      NSLog(@"#2 youHaveRestoreData uid_icloud:%@ ", uid_icloud);
      if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
        return YES;
      }
    }
  }
  return NO;
}
- (void)configureObservers
{
  [super configureObservers];
}

- (void)configureNavigation
{
  [super configureNavigation];
  // self.navigationController.navigationBarHidden = YES;
}

- (void)configureData
{
  [super configureData];
}

- (void)loadData
{
  [super loadData];
}

- (void)layout
{
  [super layout];
}

- (void)dismissObservers
{
  [super dismissObservers];
}

#pragma mark - validation of phone number
- (BOOL)chekUserTextFields
{
  return ([self chekUserPhonePrefixField] && [self chekUserPhoneNumberField]);
}

- (BOOL)chekUserPhonePrefixField
{
  if ([self.userPhonePrefixField.text length] < 2) {
    [UIAlertView alertWithCause:kAlertPhoneNumberNotValid];
    return NO;
  }
  return YES;
}

- (BOOL)chekUserPhoneNumberField
{
  if ([self.userPhoneNumberField.text length] < 7) {
    [UIAlertView alertWithCause:kAlertPhoneNumberShorterThan8Digit];
    return NO;
  }
  return YES;
}

#pragma mark - Interface Builder Actions

- (IBAction)userLoginTapped:(id)sender
{
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnection];
    return;
  }

  if ([self chekUserTextFields]) {
    NSString *msisdn = [NSString stringWithFormat:@"%@%@%@", [USER_MANAGER defaultPrefix], self.userPhonePrefixField.text, self.userPhoneNumberField.text];
    [self addActivityIndicator];
    [self.userPhoneNumberField resignFirstResponder];

    if ([USER_MANAGER takeLoginTokenWithMSISDN:msisdn]) {
      [self removeActivityIndicator];
      LMLoginCodeViewController *controler = [LMLoginCodeViewController new];
      controler.msisdn = msisdn;
      [self.navigationController pushViewController:controler animated:YES];
    } else {
      [self removeActivityIndicator];
      // LMBuyInAppViewController *controler = [LMBuyInAppViewController new];
      //[self.navigationController pushViewController:controler animated:YES];
    }
  }
}

- (IBAction)guestLoginTapped:(id)sender
{
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnection];
  }
  [self addActivityIndicator];
  [USER_MANAGER initializeWithGuest];
  [USER_MANAGER loadUserValuesWithCompletitionBlock:^(NSError *error) {
    [self removeActivityIndicator];
    [USER_MANAGER setGuestStatus];
    [APP_DELEGATE buildMainStack];
  }];
}

- (IBAction)inAppButtonTapped:(id)sender
{
  [self subscribe:nil];
}
- (IBAction)restoreButtonTapped:(id)sender
{
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }
  [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"youHaveRestoreDataChek"]; // this is only for guestUser chek restor data
  [[NSUserDefaults standardUserDefaults] synchronize];
  [self addActivityIndicator];
  [USER_MANAGER initializeWithNativeUser];
  [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
  [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
    if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
      [self removeActivityIndicator];
      [APP_DELEGATE buildMainStack];
    } else {
      [self.restoreButton setHidden:YES];
      [self.restoreButton setEnabled:NO];
      [self.inAppButton setHidden:NO];
      [self.inAppButton setEnabled:YES];
      [self removeActivityIndicator];
      [APPSTYLE applyTitle:Localized(@"T003.4") toButton:self.inAppButton];
      self.infoLabe.text = [NSString stringWithFormat:Localized(@"T010.b"), [USER_MANAGER takeNativeDateStringFromDate]];
    }

  }];
}
#pragma mark in app
- (IBAction)subscribe:(id)sender
{
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }
  [self addActivityIndicator];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:0];

  if (![PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
    [PURCHASE_MANAGER requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
      if (success) {
        NSArray *filtered = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier BEGINSWITH[cd] %@", kMonthlyPurchaseId]];
        if (filtered && [filtered count] != 0) {
          SKProduct *product = filtered[0];
          [PURCHASE_MANAGER buyProduct:product];
          NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:@"No userID", @"userID", nil];
          [Flurry logEvent:kOnAppleSubscriptionTerms withParameters:articleParams];
        } else {
          [[NSNotificationCenter defaultCenter] removeObserver:self];
          [self removeActivityIndicator];
        }
      } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self removeActivityIndicator];
      }
    }];
  } else {
    [self removeActivityIndicator];
  }
}
#pragma mark - Purchased
- (void)productPurchased:(NSString *)productId
{
  NSLog(@"productPurchased %@", productId);
  [self removeActivityIndicator];
  //[[NSNotificationCenter defaultCenter] removeObserver:self];
  if ([PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
    NSLog(@"YES ");
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // dali samo za yes da go trgam?
    //[self addActivityIndicator];
    NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
    if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
      [USER_MANAGER initializeWithNativeUser];
      [USER_MANAGER makeBillingForNativeUserWithCompletitionBlock:^(NSError *error) {
        [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
          [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
          [[NSUserDefaults standardUserDefaults] synchronize];
          [self removeActivityIndicator];
          [APP_DELEGATE buildMainStack];
        }];
      }];

      return;
    }
    //[self addActivityIndicator];
    [USER_MANAGER initializeWithNativeUser];
    [USER_MANAGER registerNativeUserValuesWithCompletitionBlock:^(NSError *error) {
      [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
        [self removeActivityIndicator];
        [APP_DELEGATE buildMainStack];
      }];
    }];
  } else {
    NSLog(@"NO ");
  }
  //[_submitButton setEnabled:YES];
  //[self releaseLoadingWindow];
}

#pragma mark - TextField functionsF
//
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  if ((textField == self.userPhonePrefixField) || (textField == self.userPhoneNumberField)) {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
    if (numberOfMatches == 0)
      return NO;


    if (textField == self.userPhonePrefixField) {
      if (self.userPhonePrefixField.text.length + [string length] >= 2 && [string length] > 0) {
        if (self.userPhonePrefixField.text.length < 2)
          self.userPhonePrefixField.text = [self.userPhonePrefixField.text stringByAppendingString:string];
        [self.userPhoneNumberField becomeFirstResponder];
        return NO;
      }
      return YES;
    }
    if (textField == self.userPhoneNumberField) {
      if (self.userPhoneNumberField.text.length >= 9 && [string length] > 0)
        return NO;
      return YES;
    }
    return YES;
  }

  return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if (textField == self.userPhonePrefixField) {
    [self.userPhoneNumberField becomeFirstResponder];
  }
  return YES;
}
#pragma mark subscriptionPopUpView

- (IBAction)showSubscribePopUp:(id)sender
{

  subscriptionBlackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  subscriptionBlackView.backgroundColor = [UIColor blackColor];
  subscriptionPopUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
  [subscriptionBlackView setAlpha:0];
  [self.view addSubview:subscriptionBlackView];

  [self.view addSubview:subscriptionPopUpView];

  /* loginButton.userInteractionEnabled = NO;
   guestLoginButoon.userInteractionEnabled = NO;
   subscribeButton.userInteractionEnabled = NO;

   [prefixPhoneLabel resignFirstResponder];
   [phoneLabel resignFirstResponder];*/
  int upSpace = 41;
  if (IS_IPHONE4) {
    upSpace = 31;
  }
  [self.userPhonePrefixField resignFirstResponder];
  [self.userPhoneNumberField resignFirstResponder];
  [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, self.view.frame.size.height,
                                             subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
  subscriptionPopUpView.alpha = 0;
  [UIView animateWithDuration:0.4
    delay:0
    options:0
    animations:^{
      [subscriptionBlackView setAlpha:0.8];
      [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, upSpace,
                                                 subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
      subscriptionPopUpView.alpha = 1;
      /*tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];

       [self.view addGestureRecognizer:tap];*/
    }

    completion:^(BOOL finished){

    }];
}

- (IBAction)cancelSubscribePopUp:(id)sender
{
  [self removeSubscriptionPopUpView];
}

- (void)removeSubscriptionPopUpView
{
  [UIView animateWithDuration:0.4
    delay:0
    options:0
    animations:^{
      [subscriptionPopUpView setFrame:CGRectMake((self.view.frame.size.width - subscriptionPopUpView.frame.size.width) / 2, self.view.frame.size.height,
                                                 subscriptionPopUpView.frame.size.width, subscriptionPopUpView.frame.size.height)];
      subscriptionPopUpView.alpha = 0;
      [subscriptionBlackView setAlpha:0];
      [self.view removeGestureRecognizer:tap];
    }

    completion:^(BOOL finished) {
      /* loginButton.userInteractionEnabled = YES;
       guestLoginButoon.userInteractionEnabled = YES;
       subscribeButton.userInteractionEnabled = YES;*/
      [subscriptionBlackView removeFromSuperview];
    }];
}


@end
