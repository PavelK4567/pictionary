//  LMSearchCell.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


typedef enum {
	kSearchCellInactive,
	kSearchCellFocus,
	kSearchCellResults,
	kSearchCellResultsFromApi,
} ESearchCellStatus;


@protocol LMSearchCellDelegate <NSObject>

- (void)expandButtonTapped:(UIButton *)expandButton;
- (void)searchCellChangedState:(ESearchCellStatus)state;
- (void)updateSearchString:(NSString *)updateString;
- (void)changeSearchString:(NSString *)updateString;
@end


@interface LMSearchCell : UICollectionViewCell

@property (assign, nonatomic) id <LMSearchCellDelegate> delegate;

- (void)setInfoText:(NSString *)text;
- (void)setSearchLabelText:(NSString *)text;
- (void)setFirstResponder;

@end
