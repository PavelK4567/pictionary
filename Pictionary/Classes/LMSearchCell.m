//  LMSearchCell.m
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMSearchCell.h"


@interface LMSearchCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *expandButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end


@implementation LMSearchCell

- (IBAction)expandButtonTapped:(UIButton *)sender {
	[self.delegate expandButtonTapped:sender];
}

- (void)awakeFromNib {
	[super awakeFromNib];
	[self configUI];
	[self configAppearance];
	[self.searchField setDelegate:self];
	[self.searchField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
  self.searchField .placeholder = Localized(@"T070");
  UILabel *magnifyingGlass = [[UILabel alloc] init];
  [magnifyingGlass setText:[[NSString alloc] initWithUTF8String:"  \xF0\x9F\x94\x8D"]];
  [magnifyingGlass sizeToFit];
  
  [self.searchField setLeftView:magnifyingGlass];
  [self.searchField setLeftViewMode:UITextFieldViewModeAlways];
  
}

- (void)configUI {
	[self.searchField setCornerRadius:2.0];
	[self.searchField setBorder:1.0 color:[APPSTYLE colorForType:@"Field_Normal"]];
}

- (void)configAppearance {
	[APPSTYLE applyStyle:@"Field_Normal" toTextField:self.searchField];
}

#pragma mark - number of words
- (BOOL)moreThanTwoWords:(NSString *)word {
	return [[word componentsSeparatedByString:@" "] count] > 2;
}
- (BOOL)moreThanThreeWords:(NSString *)word {
  return [[word componentsSeparatedByString:@" "] count] > 3;
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[self.delegate searchCellChangedState:kSearchCellFocus];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	if ([string isEqualToString:@""])
		return YES;
	return ![self moreThanThreeWords:textField.text];
}

- (void)textFieldDidChange {
	[self.delegate changeSearchString:self.searchField.text];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	[textField performSelector:@selector(resignFirstResponder) withObject:0 afterDelay:0.1];
	[self.delegate searchCellChangedState:kSearchCellInactive];
	[self.searchField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if([self.searchField.text length] == 1){
    [UIAlertView alertWithCause:kAlertOneLetterToSearch];
  }else{
    [self.delegate updateSearchString:self.searchField.text];
    [self.delegate searchCellChangedState:kSearchCellResults];
    [self.searchField resignFirstResponder];
  }
  return YES;
}

- (void)setInfoText:(NSString *)text {
	[self.expandButton setHidden:(!([text isEqualToString:@""]))];
	[self.infoLabel setHidden:(([text isEqualToString:@""]))];
	self.infoLabel.text = text;
}

- (void)setSearchLabelText:(NSString *)text {
	self.searchField.text = text;
}

- (void)setFirstResponder {
		[self.searchField becomeFirstResponder];
}

@end
