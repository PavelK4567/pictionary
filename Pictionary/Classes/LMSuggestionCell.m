//  LMSuggestionCell.m
//  Created by Dimitar Tasev on 24.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMSuggestionCell.h"

@interface LMSuggestionCell ()

@property (weak, nonatomic) IBOutlet UILabel *itemTitle;

@end

@implementation LMSuggestionCell


- (void)configureWithString:(NSString *)string andLength:(NSInteger)length {
	NSUInteger characterCount1 = length;
	NSUInteger characterCount2 = [string length] - length;
	NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:string];
	[string2 addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, characterCount1)];
	//[string2 addAttribute:NSForegroundColorAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0,1)];
	if (characterCount2 > 0)
		[string2 addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(characterCount1, characterCount2)];
  
	self.itemTitle.attributedText = string2;
}
- (void)configureWithString:(NSString *)string andSubString:(NSString*)subString {
	
	 NSMutableAttributedString *mutable = [[NSMutableAttributedString alloc] initWithString:string];
  [mutable addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, [string length])];
  [mutable addAttribute: NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:[string rangeOfString:subString]];
  
  [mutable addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:[string rangeOfString:subString]];
  
	self.itemTitle.attributedText = mutable;
}

- (NSString *)takeSuggestionText{
  return [self.itemTitle.attributedText string];
}
@end
