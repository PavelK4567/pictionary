//  LMDetailViewController.h
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMBaseViewController.h"


@interface LMDetailViewController : LMBaseViewController

- (void)configureWithItem:(LMItem *)item;
- (void)addSaveButton;

@end
