//  LMPhotoButtonCell.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.

@protocol LMPhotoButtonCellDelegate <NSObject>

- (void)photoButtonTapped:(UIButton *)photoButton;

@end

@interface LMPhotoButtonCell : UICollectionViewCell

@property (assign, nonatomic) id <LMPhotoButtonCellDelegate> delegate;

-(void)setInfoText:(NSString*)text;

@end
