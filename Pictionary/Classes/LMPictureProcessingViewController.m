//
//  LMPictureProcessingViewController.m
//  Pictionary
//
//  Created by Kiril Kiroski on 7/16/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMPictureProcessingViewController.h"
#import "LMDetailViewController.h"


@interface LMPictureProcessingViewController ()

@property IBOutlet UIImageView *photoImageView;
@property IBOutlet UIButton *confirmButton, *refusedButton;
@end

@implementation LMPictureProcessingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)configureAppearance {
	[super configureAppearance];
    
    NSURL *imageURL = [NSURL URLWithString:self.photoPath];

    UIImage *image = [LMHelper loadImageForImageName:[imageURL lastPathComponent]];
    
	[image resizedImageWithMinimumSize:CGSizeMake(self.photoImageView.frame.size.width, self.photoImageView.frame.size.height)];
  //[[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:self.photoPath] error:nil];

  [self.photoImageView setImage:image];
  image = nil;
	if (![ReachabilityHelper reachable]) {
		[UIAlertView alertWithCause:kAlertRefusedPhotoMessage];
		[self.confirmButton setHidden:NO];
	}
}

- (void)configureUI {
	[super configureUI];
}

- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
  self.navigationItem.title = Localized(@"T069");
}

- (void)configureData {
	[super configureData];
}

- (void)loadData {
	[super loadData];
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
  //[self removeTestForLockDevice];
}

#pragma mark - Interface Builder Actions
- (IBAction)confirmButtonTapped:(UIButton *)sender {
	if (![ReachabilityHelper reachable]) {
		[UIAlertView alertWithCause:kAlertNoInternetForCamFind];
	}
	else {
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
		//[self.navigationController popToRootViewControllerAnimated:YES];
		[self processingPhoto];
		[self performSelector:@selector(confirmImage) withObject:0 afterDelay:0.250];
		//[self confirmImage];
	}
}

#pragma mark - Interface Builder Actions
- (IBAction)refusedButtonTapped:(UIButton *)sender {
	//[UIAlertView alertWithCause:kAlertRefusedPhotoMessage];
	//[self.navigationController popToRootViewControllerAnimated:YES];
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Confirm image
- (void)confirmImage {
	NSDictionary *headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER deviceIdentifier] };
	if ([USER_MANAGER isGuest]) {
		headers = @{ kUserMSISDN : @"guest", kUserDeviceId : [USER_MANAGER deviceIdentifier] };
	}
  if ([USER_MANAGER isNative]) {
    headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER userMsisdn] };
  }
	ASIHTTPRequest *confirmImageRequest = [REQUEST_MANAGER post:kRequestPicSeeResultsAPICall headers:headers];
	[confirmImageRequest setPostBody:[UIImageJPEGRepresentation([self.photoImageView.image resizedImageWithMinimumSize:CGSizeMake(640, 480)], 1.0) mutableCopy]];
	__weak ASIHTTPRequest *request = confirmImageRequest;
  [request setShouldContinueWhenAppEntersBackground:NO];
  //request.timeOutSeconds = 10;
  //[request setTimeOutSeconds:10];
  //[self testForLockDevice];
  
	[confirmImageRequest setCompletionBlock: ^{
    //[self removeTestForLockDevice];
    [self removeActivityIndicator];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      NSMutableArray *tagsArray = [NSMutableArray new];
      NSArray *responseArray = [responseDictionary objectForKey:kPreferencesFileTags];
      for (int i = 0; i < [responseArray count]; i++) {
        NSDictionary *object = [responseArray objectAtIndex:i];
        NSDictionary *tepmDictionary =
        @{ kParserTagTextLocalized : [object objectForKey:kParserTagTextNative], kParserTagTextNative : [object objectForKey:kParserTagTextTranslate] };
        [tagsArray addObject:tepmDictionary];
			}
      if ([tagsArray count] > 0) {
        [USER_MANAGER takeCredit];
          UIImage *image = [LMHelper loadImageForImageName:[self.photoPath lastPathComponent]];
        [image resizedImageWithMinimumSize:CGSizeMake(840, 840)];
        //[image croppedImageWithRect:CGRectMake(200, 200, 640, 640)];
        
        LMItem *item = [ITEM_MANAGER makeItemWithImage:image withTags:tagsArray isLandscapeImage:self.isLandscapeImage];
        
        
        LMDetailViewController *controler = [LMDetailViewController new];
        [controler configureWithItem:item];
        [controler addSaveButton];
        [self.navigationController pushViewController:controler animated:YES];
       // [controler makeDoneBtn];
			}
      else {
        [UIAlertView alertWithCause:kAlertNoResultFromCamFindMessage];
        [self.navigationController popViewControllerAnimated:YES];
      }
      //[self removeActivityIndicator];
      return;
      // [UIImage]
    }else{
      //black image ili losh respons
      [UIAlertView alertWithCause:kAlertNoResultFromCamFindMessage];
      [self.navigationController popViewControllerAnimated:YES];
    }
	}];
	[confirmImageRequest setFailedBlock: ^{
    DLog(@"Failed Block ");
    [UIAlertView alertWithCause:kAlertCamFindAPITechnicalError];
    [self removeActivityIndicator];
	}];
	[confirmImageRequest startAsynchronous];
}
-(void)testForLockDevice{
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationGoToBackground:) name:kApplicationDidEnterBackground object:nil];
}
-(void)removeTestForLockDevice{
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:kApplicationDidEnterBackground
                                                object:nil];
}
-(void) applicationGoToBackground:(NSNotification*) notification
{
  NSLog(@"applicationGoToBackground");
  //[APP_DELEGATE buildMainStack];
  for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
  {
    [req cancel];
    [req setDelegate:nil];
  }
  [self removeActivityIndicator];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self.photoImageView setImage:nil];
    if ([self.navigationController.viewControllers indexOfObject:self] != NSNotFound)
    {
        NSMutableArray *listOfViewControllers = [self.navigationController.viewControllers mutableCopy];
        NSInteger viewIndex = self.navigationController.viewControllers.count - 2;
        [listOfViewControllers removeObjectAtIndex:viewIndex];
        self.navigationController.viewControllers = listOfViewControllers;
    }
}

@end
