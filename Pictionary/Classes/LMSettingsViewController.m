//  LMSettingsViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMSettingsViewController.h"


@interface LMSettingsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *guestUserInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *soundEfectLabel;
@property (weak, nonatomic) IBOutlet UISwitch *soundEfectSwich;

@end


@implementation LMSettingsViewController

- (void)configureAppearance {
	[super configureAppearance];
	self.view.backgroundColor = [APPSTYLE otherViewColorForKey:@"View_Background"];
  
	[APPSTYLE applyStyle:@"Settings_Text_Field" toLabel:self.numberLabel];
  [APPSTYLE applyStyle:@"Settings_Text_Field" toLabel:self.numberInfoLabel];
	[APPSTYLE applyStyle:@"Settings_Text_Field" toLabel:self.soundEfectLabel];
  [APPSTYLE applyStyle:@"Settings_Text_Field" toLabel:self.guestUserInfoLabel];
	[APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.loginButton];
	self.loginButton.layer.cornerRadius = 4.0;
	self.loginButton.layer.borderWidth = 1;
  
}

- (void)configureUI {
	[super configureUI];
  
	[self.numberLabel setCornerRadius:4.0];
	[self.numberLabel setBorder:1.0 color:[UIColor blackColor]];
  [self.guestUserInfoLabel setCornerRadius:4.0];
  [self.guestUserInfoLabel setBorder:1.0 color:[UIColor blackColor]];
	[self.soundEfectLabel setCornerRadius:4.0];
	[self.soundEfectLabel setBorder:1.0 color:[UIColor blackColor]];
  self.soundEfectSwich.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"SoundEffectsEnabled"];
	self.numberLabel.text = [NSString stringWithFormat:@"  %@", [USER_MANAGER userPhone]];
  self.numberInfoLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T147")];
	[APPSTYLE applyTitle:Localized(@"T151") toButton:self.loginButton];
  
	if ([USER_MANAGER isGuest]) {
		self.numberLabel.text = [NSString stringWithFormat:@"  "];
		//self.numberLabel.enabled = NO;
    //[self.numberLabel setHidden:YES];
    //[self.numberInfoLabel setHidden:YES];
		[APPSTYLE applyTitle:Localized(@"T002") toButton:self.loginButton];
		self.statusLabel.text = [NSString stringWithFormat:@"  %@%@", Localized(@"T146"), Localized(@"T150")];
	}
	else if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
		self.statusLabel.text = [NSString stringWithFormat:@"  %@%@", Localized(@"T146"), Localized(@"T148")];
	}
	else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
		self.statusLabel.text = [NSString stringWithFormat:@"  %@%@", Localized(@"T146"), Localized(@"T149")];
	}
	else {
		self.statusLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T146")];
	}
  
  
	self.soundEfectLabel.text = [NSString stringWithFormat:@"  %@", Localized(@"T145")];
  if([USER_MANAGER isNative]){
    [self.loginButton setHidden:YES];
    [self.loginButton setEnabled:NO];
    self.numberInfoLabel.text =@"";
    [self.numberLabel  setHidden:YES];
    self.numberLabel.text = [NSString stringWithFormat:@"  "];
    [self.guestUserInfoLabel setHidden:NO];

    self.guestUserInfoLabel.text = [NSString stringWithFormat:Localized(@"T147.1"),[USER_MANAGER takeNativeDateStringForSettings],[NSString stringWithFormat:@"%ld",(long)[USER_MANAGER numberOfCredits]]];
  }
}

- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
  self.navigationItem.title = Localized(@"T141");
}

- (void)configureData {
	[super configureData];
}

- (void)loadData {
	[super loadData];
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
}

#pragma mark - Interface Builder Actions

- (IBAction)pressLogOut:(id)sender {
  if ([USER_MANAGER isGuest]) {
    [APP_DELEGATE buildLoginStack];
  }else{
    [self showLogOutAlert];
  }
}
- (IBAction)soundEffectsSwichPress:(id)sender {
	[[NSUserDefaults standardUserDefaults] setBool:self.soundEfectSwich.on forKey:@"SoundEffectsEnabled"];
  [AUDIO_MANAGER setEnabled:self.soundEfectSwich.on];
  [[NSUserDefaults standardUserDefaults] synchronize];
}
#pragma mark - log out for active user
-(void)showLogOutAlert{
  UIAlertView *tempAlert = [UIAlertView alertViewWithTitle:@""
                                                   message:Localized(@"T142")
                                         cancelButtonTitle:Localized(@"T143")
                                         otherButtonTitles:@[Localized(@"T144")]
                                                 onDismiss: ^(int buttonIndex) {
                                                   if (buttonIndex == -1){
                                                     
                                                   }else if (buttonIndex == 0){
                                                     [USER_MANAGER logOutUser];
                                                     [APP_DELEGATE buildLoginStack];
                                                   }
                                                 }
                                                  onCancel: ^{
                                                    
                                                  }];
  [tempAlert show];
}

@end
