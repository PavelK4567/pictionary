//  LMCaptureViewController.h
//  Created by Kiril Kiroski on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMBaseViewController.h"
#import "DIYCam.h"

@interface LMCaptureViewController : LMBaseViewController <DIYCamDelegate, UIGestureRecognizerDelegate>

@end
