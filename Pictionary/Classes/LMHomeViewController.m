//  LMHomeViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//  Edited by Darko Trpevski

#import "LMHomeViewController.h"
#import "LMPhotoButtonCell.h"
#import "LMSearchCell.h"
#import "LMItemCell.h"
#import "LMResultCell.h"
#import "LMSuggestionCell.h"
#import "LMSettingsViewController.h"
#import "LMCaptureViewController.h"
#import "LMDetailViewController.h"
#import "LMItem.h"
#import "LMTag.h"
#import "CaptureViewController.h"


static NSString *takeImageButtonCellIdentifier = @"TakeImageButtonCellIdentifier";
static NSString *searchCellIdentifier = @"SearchCellIdentifier";
static NSString *itemCellIdentifier = @"ItemCellIdentifier";
static NSString *resultCellIdentifier = @"ResultCellIdentifier";
static NSString *suggestionCellIdentifier = @"SuggestionCellIdentifier";


@interface LMHomeViewController () <LMSearchCellDelegate, LMPhotoButtonCellDelegate, LMItemCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *contentView;
@property (strong, nonatomic) IBOutlet UIView *garbageView;

@property (strong, nonatomic) NSString *searchString;
@property (assign, nonatomic) ESearchCellStatus searchStatus;
@property (assign, nonatomic) BOOL expandedStatus;
@property (assign, nonatomic) BOOL clickExpand;
@property (strong, nonatomic) NSMutableArray *apiResultsArray;
@property (strong, nonatomic) NSMutableArray *apiResultsTagsArray;
@property (strong, nonatomic) NSMutableArray *itemsNames;
@property (strong, nonatomic) NSMutableArray *itemsNamesForSearch;
@property (strong, nonatomic) NSMutableArray *resultItemsArray;
@property (strong, nonatomic) LMItem *selectItem;
@property (strong, nonatomic) UICollectionViewCell *selectItemCell;
@property (strong, nonatomic) LMSearchCell *tempSearchCell;
@end


@implementation LMHomeViewController

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[self unselectCellsForRemove];
}

- (void)configureAppearance {
	[super configureAppearance];
	[self takeNamesForSearch];
	[self.contentView reloadData];
	[self.contentView setScrollEnabled:self.expandedStatus];
}

- (void)configureUI {
	[super configureUI];
  
	[self.contentView registerClass:[LMPhotoButtonCell class] forCellWithReuseIdentifier:takeImageButtonCellIdentifier];
	[self.contentView registerClass:[LMSearchCell class] forCellWithReuseIdentifier:searchCellIdentifier];
	[self.contentView registerClass:[LMItemCell class] forCellWithReuseIdentifier:itemCellIdentifier];
	[self.contentView registerClass:[LMSuggestionCell class] forCellWithReuseIdentifier:suggestionCellIdentifier];
	[self.contentView registerClass:[LMResultCell class] forCellWithReuseIdentifier:resultCellIdentifier];
  
	IS_WIDESCREEN
	? [self.contentView registerNib:[UINib nibWithNibName:@"LMPhotoButtonCell-568h" bundle:0] forCellWithReuseIdentifier:takeImageButtonCellIdentifier]
	: [self.contentView registerNib:[UINib nibWithNibName:@"LMPhotoButtonCell" bundle:0] forCellWithReuseIdentifier:takeImageButtonCellIdentifier];
  
	[self.contentView registerNib:[UINib nibWithNibName:@"LMSearchCell" bundle:0] forCellWithReuseIdentifier:searchCellIdentifier];
	[self.contentView registerNib:[UINib nibWithNibName:@"LMItemCell" bundle:0] forCellWithReuseIdentifier:itemCellIdentifier];
	[self.contentView registerNib:[UINib nibWithNibName:@"LMSuggestionCell" bundle:0] forCellWithReuseIdentifier:suggestionCellIdentifier];
	[self.contentView registerNib:[UINib nibWithNibName:@"LMResultCell" bundle:0] forCellWithReuseIdentifier:resultCellIdentifier];
}

- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
}

- (void)configureData {
	[super configureData];
}

- (void)loadData {
	[super loadData];
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
}

- (void)refreshContentAnimation {
	static NSMutableIndexSet *refreshableSet = 0;
  
  
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
    refreshableSet = [NSMutableIndexSet indexSet];
    [refreshableSet addIndex:0];
    [refreshableSet addIndex:2];
    [refreshableSet addIndex:3];
    [refreshableSet addIndex:4];
	});
  
	[UIView setAnimationsEnabled:NO];
  
	[self.contentView performBatchUpdates: ^{ [self.contentView reloadSections:refreshableSet]; }
	                           completion: ^(BOOL finished) { [UIView setAnimationsEnabled:YES]; }];
}

#pragma mark - reload Sections

- (void)refreshContent {
	static NSMutableIndexSet *refreshableSet = 0;
  
  
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
    refreshableSet = [NSMutableIndexSet indexSet];
    [refreshableSet addIndex:0];
    [refreshableSet addIndex:2];
    [refreshableSet addIndex:3];
    [refreshableSet addIndex:4];
	});
	[self unselectCellsForRemove];
  
	switch (self.searchStatus) {
		case kSearchCellInactive:
			[self setSearchString:0];
			if (self.clickExpand) {
				self.clickExpand = NO;
				if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
					[self.contentView reloadSections:refreshableSet];
				}
				else {
					[self.contentView reloadData];
				}
			}
			[self.tempSearchCell setSearchLabelText:@""];
			[self.tempSearchCell setInfoText:@""];
			self.itemsNamesForSearch = [NSMutableArray new];
			self.resultItemsArray = [NSMutableArray new];
			[self.contentView reloadData];
			[self removeSearchBackBtn];
			break;
      
		case kSearchCellFocus:
      
			if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
				[self.contentView reloadSections:refreshableSet]; // ios6 bug
			}
			else {
				//[self.contentView reloadData];
				// [self.tempSearchCell performSelector:@selector(setFirstResponder) withObject:0 afterDelay:0.250];
			}
			//[self refreshContentAnimation];
			//[self.contentView reloadSections:refreshSearchSet];
			//[self.contentView reloadData];
			[self.tempSearchCell setInfoText:Localized(@"T111")];
      [self.contentView setScrollEnabled:YES];
			[self makeSearchBackBtn];
			break;
      
		case kSearchCellResults:
      if (![ReachabilityHelper reachable]) {
        [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
       // [UIAlertView alertWithCause:kAlertNoInternetForGoogleImage];
        break;
      }
			//if ([self.itemsNamesForSearch count] == 0) {
      if([self youCanSearchOnGoogle]){
				if ([USER_MANAGER canGoogle]) {
          self.resultItemsArray = [NSMutableArray new];
					[self.tempSearchCell setInfoText:Localized(@"T122")];
					[self addActivityIndicator];
					//[self performSelector:@selector(takeSearchResults) withObject:0 afterDelay:0.125];//after don't show results :(
          
					[self takeSearchResults];
				}
				else {
					if ([USER_MANAGER getUserStatus] == KAUserGuest) {
						[UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForGuestUser];
					}
					else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
						[UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
            //was kAlertNoMoreGoogleImageCreditsForDisabledUser
            //
           }
					else if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
						[UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForActiveUser];
					}
				}
			}
			else {
				[self.tempSearchCell setInfoText:[NSString stringWithFormat:Localized(@"T114"), [self.resultItemsArray count]]];
			}
			if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
				[self.contentView reloadSections:refreshableSet];
			}
			else {
				[self.contentView reloadData];
			}
			break;
      
		case kSearchCellResultsFromApi:
			[self.tempSearchCell setInfoText:Localized(@"T113")];
			[self.contentView setScrollEnabled:YES];
			break;
	}
  
	if (self.searchStatus == kSearchCellInactive) {
		[self.contentView setScrollEnabled:self.expandedStatus];
	}
}

- (void)expandButtonTapped:(UIButton *)expandButton {
	/*[self.contentView setScrollEnabled:YES];
   [self.contentView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
   return;*/
	self.clickExpand = YES;
	expandButton.selected = !expandButton.selected;
	[self setExpandedStatus:expandButton.selected];
	[self setSearchStatus:kSearchCellInactive];
	[self refreshContent];
}

- (void)searchCellChangedState:(ESearchCellStatus)state {
	[self setSearchStatus:state];
	[self refreshContent];
}

- (void)updateSearchString:(NSString *)updateString {
	self.searchString = updateString;
	[self takeItemsResultsForInputText];
}

- (void)changeSearchString:(NSString *)updateString {
	self.searchString = updateString;
	self.itemsNamesForSearch = [NSMutableArray new];
	for (NSString *tempObjectName in self.itemsNames) {
		if ([tempObjectName rangeOfString:updateString options:NSCaseInsensitiveSearch].location != NSNotFound)
			[self.itemsNamesForSearch addObject:tempObjectName];
	}
	//DLog(@"self.itemsNamesForSearch %d", [self.itemsNamesForSearch count]);
	// self.itemsNamesForSearch = (NSMutableArray*)[self.itemsNamesForSearch sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	//
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"length" ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	[self.itemsNamesForSearch sortUsingDescriptors:sortDescriptors];
	//
	[self refreshContent];
}
- (BOOL)youCanSearchOnGoogle{
  if ([self.itemsNamesForSearch count] == 0) {
    return YES;
  }
  self.itemsNamesForSearch = [NSMutableArray new];
  for (NSString *tempObjectName in self.itemsNames) {
    if ([tempObjectName isEqualToString:self.searchString ])
      return NO;
  }
  return YES;
}
#pragma mark - LMPhotoButtonCellDelegate

- (void)photoButtonTapped:(UIButton *)photoButton {
	[self showCapture];
}

#pragma mark - LMItemCellDelegate

- (void)selectItemCellIsSelectedWithItem:(LMItem *)selectItem {
	if (self.searchStatus == kSearchCellInactive) {
		if (self.selectItem) {
			[self changeSelectedCell];
		}
		self.selectItem = selectItem;
		[self showDeleteBtn];
	}
}

- (void)selectItemCell:(UICollectionViewCell *)selectCell {
	self.selectItemCell = selectCell;
}
- (BOOL)canSelectItem{
  return self.expandedStatus;
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return [self numberForPhotoCel]; //(self.searchStatus != kSearchCellInactive) ? 0 : 1;;
			break;
      
		case 1:
			return 1;
			break;
      
		case 2:
			return [self numberForSearchCel];
			break;
      
		case 3:
			return (self.searchStatus == kSearchCellResults) ? [self.resultItemsArray count] : 0;
			break;
      
		default:
			return [self numberForItemCell];
			break;
	}
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	UICollectionViewCell *cell = 0;
	switch (indexPath.section) {
		case 0:
			cell = [collectionView dequeueReusableCellWithReuseIdentifier:takeImageButtonCellIdentifier forIndexPath:indexPath];
			if (!cell) {
				cell = [LMPhotoButtonCell getFromNib];
			}
			[(LMPhotoButtonCell *)cell setInfoText : Localized(@"T071")];
			[(LMPhotoButtonCell *)cell setDelegate : self];
			break;
      
		case 1:
			cell = [collectionView dequeueReusableCellWithReuseIdentifier:searchCellIdentifier forIndexPath:indexPath];
			if (!cell) {
				cell = [LMSearchCell getFromNib];
			}
			self.tempSearchCell = (LMSearchCell *)cell;
			[(LMSearchCell *)cell setDelegate : self];
			/* if (self.searchStatus == kSearchCellFocus){
       [self.tempSearchCell setFirstResponder];
       }*/
			break;
      
		case 2:
			cell = [collectionView dequeueReusableCellWithReuseIdentifier:suggestionCellIdentifier forIndexPath:indexPath];
			if (!cell) {
				cell = [LMSuggestionCell getFromNib];
			}
			[(LMSuggestionCell *)cell configureWithString :[self.itemsNamesForSearch objectAtIndex:indexPath.item] andSubString : self.searchString];
			break;
      
		case 3:
			cell = [collectionView dequeueReusableCellWithReuseIdentifier:resultCellIdentifier forIndexPath:indexPath];
			if (!cell) {
				cell = [LMResultCell getFromNib];
			}
			[(LMResultCell *)cell configureWithModel :[self.resultItemsArray objectAtIndex:indexPath.item]];
      
			break;
      
		default:
			cell = [collectionView dequeueReusableCellWithReuseIdentifier:itemCellIdentifier forIndexPath:indexPath];
			if (!cell) {
				cell = [LMItemCell getFromNib];
			}
			if (self.searchStatus == kSearchCellInactive) {
                

				[(LMItemCell *)cell configureWithModel :[ITEM_MANAGER itemAtIndex:indexPath.item]];
				[(LMItemCell *)cell setDelegate : self];
			}
			else {
				[(LMItemCell *)cell configureWithDictionary :[self.apiResultsArray objectAtIndex:indexPath.item]];
			}
			break;
	}
	return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return 5;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
//           viewForSupplementaryElementOfKind:(NSString *)kind
//                                 atIndexPath:(NSIndexPath *)indexPath {
//  return 0;
//}

- (NSInteger)numberForPhotoCel {
	return (self.searchStatus != kSearchCellInactive) ? 0 : (self.expandedStatus) ? 0 : 1;
}

- (NSInteger)numberForSearchCel {
	switch (self.searchStatus) {
		case kSearchCellFocus:
			// return ([self.itemsNamesForSearch count] > 4) ? 4 : [self.itemsNamesForSearch count];
			return [self.itemsNamesForSearch count];
			break;
      
		default:
			return 0;
			break;
	}
}

- (NSInteger)numberForItemCell {
	switch (self.searchStatus) {
		case kSearchCellInactive:
			return [ITEM_MANAGER itemCount];
			break;
      
		case kSearchCellResultsFromApi:
			return [self.apiResultsArray count];
			break;
      
		default:
			return 0;
			break;
	}
}

#pragma mark - UICollectionViewDelegateFlowLayout
/*- (CGSize)           collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout *)collectionViewLayout
 referenceSizeForHeaderInSection:(NSInteger)section {
 return kHeaderSize;
 }
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
 referenceSizeForFooterInSection:(NSInteger)section{
 return kHeaderSize;
 }*/

- (CGSize)  collectionView:(UICollectionView *)collectionView
                    layout:(UICollectionViewLayout *)collectionViewLayout
    sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	switch (indexPath.section) {
		case 0:
			return IS_WIDESCREEN ? kHomePhotoCellSize : kHomePhoto480CellSize;
			break;
      
		case 1:
      return (self.searchStatus == kSearchCellInactive)? kHomeSearchCellSize : kHomeSearchActiveStateCellSize;
			//return kHomeSearchCellSize;
			break;
      
		case 2:
			return kHomeSuggestionCellSize;
			break;
      
		case 3:
			return kHomeResultCellSize;
			break;
      
		default:
			return kHomeItemCellSize;
			break;
	}
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
//                        layout:(UICollectionViewLayout *)collectionViewLayout
//        insetForSectionAtIndex:(NSInteger)section {
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView
//                               layout:(UICollectionViewLayout *)collectionViewLayout
//  minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView
//                                    layout:(UICollectionViewLayout *)collectionViewLayout
//  minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView
//                           layout:(UICollectionViewLayout *)collectionViewLayout
//  referenceSizeForHeaderInSection:(NSInteger)section {
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView
//                           layout:(UICollectionViewLayout *)collectionViewLayout
//  referenceSizeForFooterInSection:(NSInteger)section {
//}

#pragma mark - UICollectionViewDelegate

//- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath;
//- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath;
//- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath;
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	//[self unselectCellsForRemove];
  if (self.selectItem) {
    [self unselectCellsForRemove];
    return;
  }
	if (indexPath.section == 4) {
    
		if (self.searchStatus == kSearchCellInactive) {
			LMDetailViewController *controler = [LMDetailViewController new];
			[controler configureWithItem:[ITEM_MANAGER itemAtIndex:indexPath.item]];
			[self.navigationController pushViewController:controler animated:YES];
		}
		else {
			//[self processingPhoto];
			NSDictionary *object = [self.apiResultsArray objectAtIndex:indexPath.item];
			NSString *result = [object objectForKey:kParserTagOriginalImage];
			NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:result]];
			UIImage *image = [UIImage imageWithData:imageData];
			[image resizedImageWithMinimumSize:CGSizeMake(640, 640)];
			LMItem *item = [ITEM_MANAGER makeItemWithImage:image withTags:self.apiResultsTagsArray isLandscapeImage:NO];
			// if ([ITEM_MANAGER addImageFromGoogle:image withTags:self.apiResultsTagsArray]) {
			//[self searchCellChangedState:kSearchCellInactive];
			//[self removeActivityIndicator];
			//[UIAlertView alertWithCause:kItemSavedSuccessfullyConfirmation];
			LMDetailViewController *controler = [LMDetailViewController new];
			[controler configureWithItem:item];
			[controler addSaveButton];
			[self.navigationController pushViewController:controler animated:YES];
			//}
			// else {
			//[UIAlertView alertWithCause:kItemFromGoogleSavedFailConfirmation];
			//}
		}
	}
	else if (indexPath.section == 2) {
		self.searchString = [self.itemsNamesForSearch objectAtIndex:indexPath.item];
		[self takeItemsResultsForInputText];
		[self searchCellChangedState:kSearchCellResults];
		[[self view] endEditing:YES];
	}
	else if (indexPath.section == 3) {
		LMDetailViewController *controler = [LMDetailViewController new];
		[controler configureWithItem:[self.resultItemsArray objectAtIndex:indexPath.item]];
		[self.navigationController pushViewController:controler animated:YES];
	}
}

//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath;
//
//- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath;
//- (void)collectionView:(UICollectionView *)collectionView
//  didEndDisplayingSupplementaryView:(UICollectionReusableView *)view
//                   forElementOfKind:(NSString *)elementKind
//                        atIndexPath:(NSIndexPath *)indexPath;
//
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath;
//- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender;
//- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender;
//
//- (UICollectionViewTransitionLayout *)collectionView:(UICollectionView *)collectionView
//                        transitionLayoutForOldLayout:(UICollectionViewLayout *)fromLayout
//                                           newLayout:(UICollectionViewLayout *)toLayout;

#pragma mark - show Settings

- (void)showSettings {
	LMSettingsViewController *settingsControler = [LMSettingsViewController new];
	[self.navigationController pushViewController:settingsControler animated:YES];
}

#pragma mark - show Capture Controler for take photo

- (void)showCapture {
	CaptureViewController *captureController = [CaptureViewController new];
	[self.navigationController pushViewController:captureController animated:YES];
}

#pragma mark - press search Back Button

- (void)searchBackAction {
	[self searchCellChangedState:kSearchCellInactive];
}

#pragma mark - api call for Search Results
- (void)takeSearchResults {
	if (![ReachabilityHelper reachable]) {
		[UIAlertView alertWithCause:kAlertNoInternetForGoogleImage];
		[self searchCellChangedState:kSearchCellInactive];
		return;
	}
  
  
	NSDictionary *headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER deviceIdentifier], kSearchWords : self.searchString };
	if ([USER_MANAGER isGuest]) {
		headers = @{ kUserMSISDN : @"guest", kUserDeviceId : [USER_MANAGER deviceIdentifier], kSearchWords : self.searchString };
	}
  if ([USER_MANAGER isNative]) {
    headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER userMsisdn] , kSearchWords : self.searchString};
  }
	ASIHTTPRequest *confirmImageRequest = [REQUEST_MANAGER post:kRequestGoogleResultsAPICall headers:headers];
  
  //build an info object and convert to json
  NSDictionary* info = [NSDictionary dictionaryWithObjectsAndKeys:
                        self.searchString ,@"words",
                        nil];
  
  //convert object to data
  NSError* error = nil;
  NSData* jsonData = [NSJSONSerialization dataWithJSONObject:info 
                                                     options:NSJSONWritingPrettyPrinted error:&error];
  NSMutableData *body = [jsonData mutableCopy];
  [confirmImageRequest setPostBody:body];
  
	__weak ASIHTTPRequest *request = confirmImageRequest;
	[confirmImageRequest setCompletionBlock: ^{
    [self removeActivityIndicator];
    if (request.responseStatusCode == 478 && !request.error) {
      [UIAlertView alertWithCause:kAlertErrorTranslateWord];
      [self.tempSearchCell setInfoText:Localized(@"T111")];
		}
    else if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      //NSString *jsonString = [[NSString alloc] initWithData:request.responseData encoding:NSISOLatin1StringEncoding];
      NSString *jsonString = [[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding];
     // NSLog(@"Output: %@", jsonString);
      NSDictionary *responseDictionary =
      [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
      // NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      
      self.apiResultsTagsArray = [NSMutableArray new];
      self.apiResultsArray = [NSMutableArray new];
      NSArray *tagsResponseArray = [responseDictionary objectForKey:kPreferencesFileTags];
      NSArray *urlResponseArray = [responseDictionary objectForKey:kPreferencesFileUrls];
      for (int i = 0; i < [urlResponseArray count]; i++) {
        NSDictionary *object = [urlResponseArray objectAtIndex:i];
        NSDictionary *resultsDictionary = @{
                                            kParserTagImage : [object objectForKey:kParserTagImage],
                                            kParserTagThumbnail : [object objectForKey:kParserTagThumbnail],
                                            kParserTagOriginalImage : [object objectForKey:kParserTagOriginalImage]
                                            };
        [self.apiResultsArray addObject:resultsDictionary];
			}
      for (int i = 0; i < [tagsResponseArray count]; i++) {
        NSDictionary *object = [tagsResponseArray objectAtIndex:i];
        NSDictionary *tepmDictionary =
        @{ kParserTagTextLocalized : [object objectForKey:kParserTagTextNative], kParserTagTextNative : [object objectForKey:kParserTagTextTranslate] };
        [self.apiResultsTagsArray addObject:tepmDictionary];
			}
      if ([self.apiResultsArray count] == 0) {
        [UIAlertView alertWithCause:kAlertGoogleImageAPINoResults];
        [self.tempSearchCell setInfoText:Localized(@"T111")];
        return;
			}
      else {
        [USER_MANAGER takeGoogleCredits];
        [AUDIO_MANAGER playSound:kSoundResults];
        [self searchCellChangedState:kSearchCellResultsFromApi];
        
        return;
			}
		}
    else {
      [UIAlertView alertWithCause:kAlertGoogleImageAPITechnicalError];
      [self searchCellChangedState:kSearchCellInactive];
		}
	}];
	[confirmImageRequest setFailedBlock: ^{
    //DLog(@"Failed Block ");
    [self removeActivityIndicator];
    [UIAlertView alertWithCause:kAlertGoogleImageAPITechnicalError];
    [self searchCellChangedState:kSearchCellInactive];
	}];
	[confirmImageRequest startSynchronous];
}

#pragma mark Names for searching

- (void)takeNamesForSearch {
	self.itemsNames = [NSMutableArray new];
	self.itemsNamesForSearch = [NSMutableArray new];
	// self.resultItemsArray = [NSMutableArray new];
	for (NSInteger i = 0; i < [ITEM_MANAGER itemCount]; i++) {
		LMItem *item = [ITEM_MANAGER itemAtIndex:i];
		[self.itemsNames addObject:item.localizedString];
		[self.itemsNames addObject:item.nativeString];
		for (NSString *tagUUID in item.items) {
			if ([self isNotDuplicate:[[ITEM_MANAGER tagWithId:tagUUID] native]])
				[self.itemsNames addObject:[[ITEM_MANAGER tagWithId:tagUUID] native]];
			if ([self isNotDuplicate:[[ITEM_MANAGER tagWithId:tagUUID] localized]])
				[self.itemsNames addObject:[[ITEM_MANAGER tagWithId:tagUUID] localized]];
		}
	}
	//DLog(@"self.itemsNamesForSearch %lu", (unsigned long)[self.itemsNames count]);
}

- (BOOL)isNotDuplicate:(NSString *)chekString {
	for (NSString *aa in self.itemsNames) {
		if ([aa isEqualToString:chekString]) {
			return NO;
		}
	}
	return YES;
}

#pragma mark Items Results For input or selected Text
- (void)takeItemsResultsForInputText {
	self.resultItemsArray = [NSMutableArray new];
	for (NSInteger i = 0; i < [ITEM_MANAGER itemCount]; i++) {
		LMItem *item = [ITEM_MANAGER itemAtIndex:i];
		if ([item.nativeString rangeOfString:self.searchString].location != NSNotFound) {
			[self.resultItemsArray addObject:item];
		}
		else if ([item.localizedString rangeOfString:self.searchString].location != NSNotFound) {
			[self.resultItemsArray addObject:item];
		}
		else {
			for (NSString *tagUUID in item.items) {
				if ([[[ITEM_MANAGER tagWithId:tagUUID] native] rangeOfString:self.searchString].location != NSNotFound) {
					[self.resultItemsArray addObject:item];
				}
				else if ([[[ITEM_MANAGER tagWithId:tagUUID] localized] rangeOfString:self.searchString].location != NSNotFound) {
					[self.resultItemsArray addObject:item];
				}
			}
		}
	}
	//DLog(@"resultItemsArray %lu", (unsigned long)[self.resultItemsArray count]);
}

#pragma mark - Interface Builder Actions

- (IBAction)garbageButtonTapped:(id)sender {
  if (self.selectItem.defaultImage){
    [UIAlertView alertWithCause:kAlertPredefinedPictureCanBeDeleted];
    [self unselectCellsForRemove];
    return;
  }
  
    UIAlertView *confirmationAlert = [UIAlertView alertViewWithTitle:@""
                                                     message:Localized(@"T102")
                                           cancelButtonTitle:Localized(@"T104")
                                           otherButtonTitles:@[Localized(@"T103")]
                                                   onDismiss: ^(int buttonIndex)
                                                   {
                                                     [self deleteObject];
                                                   }
                                                    onCancel: ^{
                                                        return;
                                                    }];
    [confirmationAlert show];

}

#pragma mark - Delete Actions

- (void)showDeleteBtn {
	if (self.selectItem && (![self.garbageView superview])) {
		[self.view addSubview:self.garbageView];
		[self.garbageView setFrame:CGRectMake((self.view.frame.size.width - self.garbageView.frame.size.width) / 2, -66, self.garbageView.frame.size.width,
		                                      self.garbageView.frame.size.height)];
		[UIView animateWithDuration:0.4
		                      delay:0
		                    options:0
		                 animations: ^{
                       [self.garbageView setFrame:CGRectMake((self.view.frame.size.width - self.garbageView.frame.size.width) / 2, 0, self.garbageView.frame.size.width,
                                                             self.garbageView.frame.size.height)];
                     }
     
		                 completion: ^(BOOL finished) {}];
	}
}

- (void)deleteObject {
	[self.garbageView removeFromSuperview];
	[ITEM_MANAGER removeItem:self.selectItem];
	//[AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"ButtonClickFX.mp3" ofType:@""]]];
	[AUDIO_MANAGER playSound:kSoundDeletePicture];
	//[UIAlertView alertWithCause:kAlertDeletedConfirmation];
	[self takeNamesForSearch];
	[self refreshContent];
	[self.contentView setScrollEnabled:self.expandedStatus];
	self.selectItem = nil;
}

- (void)unselectCellsForRemove {
	[self.garbageView removeFromSuperview];
	[self changeSelectedCell];
	self.selectItem = nil;
}

- (void)changeSelectedCell {
	[(LMItemCell *)self.selectItemCell removeSelection];
	self.selectItemCell = nil;
}

@end
