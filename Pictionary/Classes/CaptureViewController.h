//
//  CaptureViewController.h
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 Darko Trpevski. All rights reserved.
//


#import "DTAVCaptureSessionController.h"
#import "LMBaseViewController.h"

@interface CaptureViewController : LMBaseViewController
{
    __weak IBOutlet UIImageView *_imageViewMask;
    __weak IBOutlet UILabel *_labelSubtitle;
    __weak IBOutlet UIButton *_buttonCaptureImage;
    __weak IBOutlet UIView *_buttomView;
	
    __weak IBOutlet UIView *_buttomLineView;
    DTAVCaptureSessionController *_avCaptureSessionController;
}

@property (nonatomic, strong) void (^completionBlock)(UIImage *image);
@property (nonatomic, weak) IBOutlet UIView *viewCapture;
@property (nonatomic) BOOL isLandscapeImage;

- (void)loadGUI;

@end
