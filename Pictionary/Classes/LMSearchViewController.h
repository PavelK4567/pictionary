//  LMSearchViewController.h
//  Created by Dimitar Tasev on 09.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMBaseViewController.h"
#import "LMSearchDatasource.h"
#import "LMResultCell.h"
#import "LMItemCell.h"
#import "LMItem.h"

typedef enum
{
    ImagesFromPhone,
    ImagesFromGoogle
    
} ImageSearchType;

@interface LMSearchViewController : LMBaseViewController <UITextFieldDelegate>
{
    __weak IBOutlet UICollectionView *_collectionView;
    IBOutlet LMSearchDatasource *searchDatasource;
    UITextField *_searchTextField;
    UILabel *_messageLabel;
}

@property (nonatomic,strong) NSArray *items;
@property (nonatomic,strong) NSArray *tagItems;
@property (nonatomic) ImageSearchType searchType;
@property (nonatomic,strong) NSString *searchString;

@end
