//
//  LMBuyInAppViewController.m
//  Pictionary
//
//  Created by Kiril Kiroski on 7/15/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMBuyInAppViewController.h"
//#import "RageKAInAppPurchasesHelper.h"

@interface LMBuyInAppViewController ()
@property (weak, nonatomic) IBOutlet KeyboardAvoidingScrollView *contentScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@end

@implementation LMBuyInAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)configureAppearance {
	[super configureAppearance];
  
	[APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.buyButton];
	[APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.buyButton];
}

- (void)configureUI {
	[super configureUI];
	[self.contentScroll addSubview:self.contentView];
	[self.contentScroll setContentSize:self.contentView.frame.size];
	[self.contentScroll contentSizeToFit];
  
	[APPSTYLE applyTitle:Localized(@"T036") toButton:self.buyButton];
}

- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
}

- (void)configureData {
	[super configureData];
}

- (void)loadData {
	[super loadData];
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
}

- (IBAction)buyButtonTapped:(id)sender {
	[USER_MANAGER buyPurchases];
}

@end
