//  LMSuggestionCell.h
//  Created by Dimitar Tasev on 24.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMSuggestionCell : UICollectionViewCell

- (void)configureWithString:(NSString *)string andLength:(NSInteger)length;
- (void)configureWithString:(NSString *)string andSubString:(NSString *)subString;
- (NSString *)takeSuggestionText;

@end
