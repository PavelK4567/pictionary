//  LMResultCell.h
//  Created by Dimitar Tasev on 24.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.

@class LMItem;

@interface LMResultCell : UICollectionViewCell

- (void)configureWithModel:(LMItem *)item;

@end
