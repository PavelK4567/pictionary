//  LMPhotoButtonCell.m
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMPhotoButtonCell.h"

@interface LMPhotoButtonCell ()

@property(weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation LMPhotoButtonCell

- (void)awakeFromNib
{
  [super awakeFromNib];
}


- (IBAction)photoButtonTapped:(UIButton *)sender
{

  if (![ReachabilityHelper reachable]) {

    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    //[UIAlertView alertWithCause:kAlertNoInternetForCamFind];
    return;
  }
  if ([USER_MANAGER canRecognize]) {
    if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
    } else {
      [self.delegate photoButtonTapped:sender];
    }
  } else {
    if ([USER_MANAGER getUserStatus] == KAUserGuest) {
      //[UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForGuestUser];
      [self showPopUpForGuest];
    } else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
    } else if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForActiveUser];
    }
  }
}
- (void)showPopUpForGuest
{
  UIAlertView *confirmationAlert = [UIAlertView alertViewWithTitle:@""
    message:Localized(@"T074")
    cancelButtonTitle:Localized(@"T074.2")
    otherButtonTitles:@[ Localized(@"T074.1") ]
    onDismiss:^(int buttonIndex) { [APP_DELEGATE buildLoginStack]; }
    onCancel:^{ return; }];
  [confirmationAlert show];
}
- (void)setInfoText:(NSString *)text
{
  self.infoLabel.text = text;
}
@end
