//  LMCaptureViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMCaptureViewController.h"
#import "LMPictureProcessingViewController.h"

@interface LMCaptureViewController ()
@property IBOutlet DIYCam *cam;
@property IBOutlet UIButton *cameraButton;
@property IBOutlet UIButton *flash;
@end


@implementation LMCaptureViewController

- (void)configureAppearance {
	[super configureAppearance];
  if(!self.cam){
    self.cam = [[DIYCam alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view insertSubview:self.cam atIndex:0];
  }
  [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(configureCamera) userInfo:nil repeats:NO];
	//[self.cam startSession];//remove for beter performance
	[self.cameraButton setEnabled:YES];
	//[APPSTYLE applyStyle:@"Button_Login_Page" toButton:self.flash];
  //  [self.flash setHidden:YES];
	//[APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Button_Login_Frame"] toButton:self.flash];
  
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)configureUI {
	[super configureUI];

	// Setup cam
	//self.cam.delegate = self;
	//[self.cam setupWithOptions:@{ DIYAVSettingCameraPosition : [NSNumber numberWithInt:AVCaptureDevicePositionBack] }];
	//[self.cam setCamMode:DIYAVModePhoto];
	///
}
- (void)configureCamera {
  // Setup cam
  /*if(!self.cam){
    self.cam = [[DIYCam alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.cam];
  }*/
  self.cam.delegate = self;
  [self.cam setupWithOptions:@{ DIYAVSettingCameraPosition : [NSNumber numberWithInt:AVCaptureDevicePositionBack] }];
  [self.cam setCamMode:DIYAVModePhoto];
  [self.cam startSession];
  ///
}
- (void)configureObservers {
	[super configureObservers];
}

- (void)configureNavigation {
	[super configureNavigation];
  self.navigationItem.title = Localized(@"T069");
}

- (void)configureData {
  
	[super configureData];
}

- (void)loadData {
	[super loadData];
  //[self configureCamera];//no wait for screen to come
}

- (void)layout {
	[super layout];
}

- (void)dismissObservers {
	[super dismissObservers];
  [self.cam removeFromSuperview];
   self.cam.delegate = nil;
  self.cam = nil;
}

#pragma mark - Interface Builder Actions
- (IBAction)photoButtonTapped:(UIButton *)sender {
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetForCamFind];
    return;
  }
  //[AUDIO_MANAGER playSound:kSoundTakePicture];
	[self.cameraButton setEnabled:NO];
	[self.cam capturePhoto];
}

- (IBAction)toggleFlash:(id)sender {
	if ([self.cam getFlash]) {
		[self.cam setFlash:false];
		[self.flash setTitle:@"Flash Off" forState:UIControlStateNormal];
	}
	else {
		[self.cam setFlash:true];
		[self.flash setTitle:@"Flash On" forState:UIControlStateNormal];
	}
}

#pragma mark - DIYCamDelegate

- (void)camReady:(DIYCam *)cam {
	NSLog(@"Ready");
}

- (void)camDidFail:(DIYCam *)cam withError:(NSError *)error {
	NSLog(@"Fail");
}

- (void)camModeWillChange:(DIYCam *)cam mode:(DIYAVMode)mode {
	NSLog(@"Mode will change");
}

- (void)camModeDidChange:(DIYCam *)cam mode:(DIYAVMode)mode {
	NSLog(@"Mode did change");
}

- (void)camCaptureStarted:(DIYCam *)cam {
	NSLog(@"Capture started");
}

- (void)camCaptureStopped:(DIYCam *)cam {
	NSLog(@"Capture stopped");
}

- (void)camCaptureProcessing:(DIYCam *)cam {
	[self.cam stopSession];
	NSLog(@"Capture processing");
}

- (void)camCaptureComplete:(DIYCam *)cam withAsset:(NSDictionary *)asset {
	NSLog(@"Capture complete. Asset: %@", asset);
	//[self performSelector:@selector(showPicture) withObject:0 afterDelay:1];
	[self.cam stopSession];
  
	NSURL *pathUrl = [asset valueForKey:kPathForCameraPhoto];
	[self showPictureProcessing:[pathUrl absoluteString]];
	//[NSTimer scheduledTimerWithTimeInterval:0.75 target:self selector:@selector(showPicture:) userInfo:[pathUrl absoluteString] repeats:NO];
	return;
}

- (void)showPicture:(NSString *)path {
    NSString *imagePath = path;
	[self.cam stopSession];
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }
	if ([USER_MANAGER canRecognize]) {
    /*if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
      return;
    }*/
		LMPictureProcessingViewController *controler = [LMPictureProcessingViewController new];
		controler.photoPath = imagePath;
        
        __weak LMCaptureViewController *_weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_weakSelf.navigationController pushViewController:controler animated:YES];
        });
	}
	else {
		if ([USER_MANAGER getUserStatus] == KAUserGuest) {
			[UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForGuestUser];
		}
		else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled) {
			[UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
		}
		else if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
			[UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForActiveUser];
		}
	}
}

- (void)camCaptureLibraryOperationComplete:(DIYCam *)cam {
	NSLog(@"Library save complete");
}

#pragma mark - UIGesture

- (void)focusAtTap:(UIGestureRecognizer *)gestureRecognizer {
	// self.focusImageView.center = [gestureRecognizer locationInView:self.cam];
	//[self animateFocusImage];
}

- (void)showPictureProcessing:(NSString *)photoPath {
    
    [self showPicture:photoPath];
}

@end
