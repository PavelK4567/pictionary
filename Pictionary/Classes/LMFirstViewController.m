//
//  LMFirstViewController.m
//  Pictionary
//
//  Created by Darko Trpevski on 11/6/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMFirstViewController.h"


@implementation LMFirstViewController

static NSString *itemCellIdentifier = @"ItemCellIdentifier";


#pragma mark - UIView LifeCicle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadCameraButtonView];
    [self loadSearchButtonView];
    [self loadExpandButton];
    [self loadCollectionView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [_searchTextField resignFirstResponder];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_searchTextField resignFirstResponder];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_deleteView removeFromSuperview];
    [_searchTextField setText:@""];
    
    [self unselectCellsForRemove];
    [self setExpandedStatus:YES];
    [self expandViewsAnimated:NO];

}


#pragma mark - ConfigureAppearance

- (void)configureAppearance
{
	[self.containerView reloadData];
    
    self.view.backgroundColor = _containerView.backgroundColor;

}


#pragma mark - ConfigureNavigation

- (void)configureNavigation
{
    [super configureNavigation];
}


#pragma mark - ConfigureData

- (void)configureData
{
    [super configureData];
}


#pragma mark - LoadData

- (void)loadData
{
    [super loadData];
}


#pragma mark - ConfigureUI

- (void)configureUI
{
   [super configureUI];
    
   [_searchTextField setCornerRadius:2.0];
   [_searchTextField setBorder:1.0 color:[APPSTYLE colorForType:@"Field_Normal"]];
}


#pragma mark - LoadViews

- (void)loadCameraButtonView
{
    cameraButtonView = [[[NSBundle mainBundle]
                         loadNibNamed:(IS_WIDESCREEN) ? @"LMPhotoButtonCell-568h" : @"LMPhotoButtonCell"
                         owner:self
                         options:nil] objectAtIndex:0];
    
    cameraButtonView.delegate = self;

    [cameraButtonView setInfoText:Localized(@"T071")];
    [self.view addSubview:cameraButtonView];
}


- (void)loadSearchButtonView
{
    _searchTextField.delegate = self;
    _searchTextField.autoCompleteDelegate = self;
    _searchTextField.autoCompleteDataSource = searchDataSource;
    _searchTextField.backgroundColor = [UIColor whiteColor];
    _searchTextField.returnKeyType = UIReturnKeyGo;
    _searchTextField.placeholder = Localized(@"T070");
    _searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _searchTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;

    
    [_searchTextField setLeftView:[self searchIconLabel]];
    [_searchTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_searchTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [_searchTextField setAutoCompleteTableCornerRadius:0.0];
    [_searchTextField setAutoCompleteTableBorderColor:[UIColor grayColor]];
    [_searchTextField setAutoCompleteTableCellTextColor:[UIColor grayColor]];
    [_searchTextField setAutoCompleteFontSize:17.0];

}


- (void)loadCollectionView
{
    [self.containerView registerClass:[LMItemCell class] forCellWithReuseIdentifier:itemCellIdentifier];
    [self.containerView registerNib:[UINib nibWithNibName:@"LMItemCell" bundle:0] forCellWithReuseIdentifier:itemCellIdentifier];
}

- (void)loadExpandButton
{
    expandButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [expandButton addTarget:self
               action:@selector(expandButtonAction)
     forControlEvents:UIControlEventTouchUpInside];
    
    [expandButton setImage:[UIImage imageNamed:@"ic_arrow_up.png"] forState:UIControlStateNormal];
    expandButton.frame = CGRectMake(80.0, 210.0, 160.0, 30.0);
    
    [self.view addSubview:expandButton];
}

- (void)loadDeleteView
{
    if (self.selectItem && (![_deleteView superview]))
    {
        [self.view addSubview:_deleteView];
        
        [_deleteView setFrame:CGRectMake((self.view.width - _deleteView.width) / 2, -66, _deleteView.width, _deleteView.height)];
        [UIView animateWithDuration:0.4
                              delay:0
                            options:0
                         animations: ^{
                             [_deleteView setFrame:CGRectMake((self.view.width - _deleteView.width) / 2, 0,_deleteView.width, _deleteView.height)];
                         }
         
                         completion: ^(BOOL finished) {}];
    }

}


#pragma mark - LayoutViews

- (void)layout
{
    [super layout];
    
    cameraButtonView.alpha = 1.0;
    cameraButtonView.y = kViewsPadding * 1.5;
    _searchTextField.x = kViewsPadding;
    _searchTextField.width = (self.view.width - kViewsPadding * 2);
   
    self.expandedStatus = NO;

    [_searchTextField placeBelowView:cameraButtonView withPadding:(kViewsPadding * 10)];
    [expandButton placeBelowView:_searchTextField withPadding:1.0];
    [_containerView placeBelowView:expandButton withPadding:kViewsPadding ];
    
    _containerView.height = (self.view.height - (expandButton.height + expandButton.y + kViewsPadding * 2));

}


#pragma mark - ExpandViews

- (void)expandViewsAnimated:(BOOL)animated
{
    const double animationTime = (animated) ? 0.4 : 0.0;
    
    __weak typeof(self) weakSelf = self;

    if(!self.expandedStatus)
    {
        [UIView animateWithDuration:animationTime animations:^{
            cameraButtonView.alpha = 0;
            _searchTextField.y = 10;
            [expandButton placeBelowView:_searchTextField withPadding:1.0];
            _containerView.height = (weakSelf.view.height - (expandButton.height + expandButton.y + kViewsPadding * 2));
            [_containerView placeBelowView:expandButton withPadding:kViewsPadding];
            [expandButton setImage:[UIImage imageNamed:@"ic_arrow_down.png"] forState:UIControlStateNormal];
           
            weakSelf.expandedStatus = YES;
            [_deleteView removeFromSuperview];
        }];
    }
    else
    {
        [_searchTextField resignFirstResponder];
        
        [UIView animateWithDuration:animationTime animations:^{
        
            cameraButtonView.alpha = 1;
            [weakSelf layout];
            [expandButton setImage:[UIImage imageNamed:@"ic_arrow_up.png"] forState:UIControlStateNormal];
            [_deleteView removeFromSuperview];
            weakSelf.expandedStatus = NO;
            
        }];
    }
}


#pragma mark - UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [ITEM_MANAGER itemCount];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:itemCellIdentifier forIndexPath:indexPath];

    if (!cell)
    {
      
        cell = [LMItemCell getFromNib];
    }
    
    [(LMItemCell *)cell configureWithModel :[ITEM_MANAGER itemAtIndex:indexPath.item]];
    [((LMItemCell *)cell) setDelegate:self];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selectItem)
    {
        [self unselectCellsForRemove];
    }
    else
    {
        LMDetailViewController *detailsVc = [LMDetailViewController new];
        [detailsVc configureWithItem:[ITEM_MANAGER itemAtIndex:indexPath.item]];
    
        [self.navigationController pushViewController:detailsVc animated:YES];
    }
}


#pragma mark - LMPhotoButtonCell delegate

- (void)photoButtonTapped:(UIButton *)photoButton
{
    CaptureViewController *captureViewController = [CaptureViewController new];
    [self.navigationController pushViewController:captureViewController animated:YES];
}


#pragma mark - ButtonActions

-(void)expandButtonAction
{
    [self expandViewsAnimated:YES];
}


#pragma mark - Autocomplete text delegate 

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableArray *items = [NSMutableArray new];
    LMSearchViewController *searchViewController = [LMSearchViewController new];
    
    if([searchDataSource hasSearchResultsForString:textField.text])
    {

        items = [searchDataSource resultItemsForString:textField.text];

        [searchViewController setItems:items];
        [searchViewController setSearchString:textField.text];
        
        if([items count] == 1)
            [self showDetails:[items firstObject]];
        else
            [self.navigationController pushViewController:searchViewController animated:YES];
    }
}

#pragma LMItemCell delegate

- (void)selectItemCellIsSelectedWithItem:(LMItem *)selectItem
{
    self.selectItem = selectItem;
    [self loadDeleteView];
}


- (void)selectItemCell:(UICollectionViewCell *)selectCell
{
    selectedCell = selectCell;
}


- (BOOL)canSelectItem
{
    return self.expandedStatus;
}

#pragma mark - Delete Builder Actions

- (IBAction)garbageButtonTapped:(id)sender
{
    if (self.selectItem.defaultImage)
    {
        [UIAlertView alertWithCause:kAlertPredefinedPictureCanBeDeleted];
        [self unselectCellsForRemove];
        return;
    }
    
    UIAlertView *confirmationAlert = [UIAlertView alertViewWithTitle:@""
                                                             message:Localized(@"T102")
                                                   cancelButtonTitle:Localized(@"T104")
                                                   otherButtonTitles:@[Localized(@"T103")]
                                                           onDismiss: ^(int buttonIndex)
                                      {
                                          [self deleteObject];
                                      }
                                                            onCancel: ^{
                                                                return;
                                                            }];
    [confirmationAlert show];
    
}

- (void)deleteObject
{
    [_deleteView removeFromSuperview];
    
    [ITEM_MANAGER removeItem:self.selectItem];
    [AUDIO_MANAGER playSound:kSoundDeletePicture];
    [_containerView reloadData];
    [self unselectCellsForRemove];
    
    self.selectItem = nil;
}

- (void)unselectCellsForRemove
{
    [_deleteView removeFromSuperview];
    
    [self changeSelectedCell];
    self.selectItem = nil;
}

- (void)changeSelectedCell
{
    [(LMItemCell *)selectedCell removeSelection];
    selectedCell = nil;
    
}


#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!self.expandedStatus)
        [self expandViewsAnimated:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_searchTextField resignFirstResponder];
    
    if(!self.expandedStatus)
        [self expandViewsAnimated:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if([textField.text length] == 1)
  {
    [UIAlertView alertWithCause:kAlertOneLetterToSearch];
    return YES;
  }else{
    
    [textField resignFirstResponder];
    
    LMSearchViewController *searchViewController = [LMSearchViewController new];
    
    if([searchDataSource hasSearchResultsForString:textField.text])
    {
        NSMutableArray *items = [NSMutableArray new];
        
        items = [searchDataSource resultItemsForString:textField.text];
        [searchViewController setItems:items];
        [searchViewController setSearchString:textField.text];
        
        if ([items count] == 1)
            [self showDetails:[items firstObject]];
        else
            [self.navigationController pushViewController:searchViewController animated:YES];
    }
    else
    {
      
      if (![ReachabilityHelper reachable])
      {
        [UIAlertView alertWithCause:kAlertNoInternetForGoogleImage];
        return YES;
      }
      if ([USER_MANAGER canGoogle])
      {
        [self addActivityIndicator];
        [searchDataSource getGoogleImagesForString:textField.text withCompletitionBlock:^(NSArray *result, NSError *error) {
          
          if(!error && [result count] > 0)
          {
            [searchViewController setItems:result];
            [searchViewController setTagItems:searchDataSource.apiResultsTagsArray];
            [searchViewController setSearchType:ImagesFromGoogle];
            [searchViewController setSearchString:textField.text];
            [self.navigationController pushViewController:searchViewController animated:YES];
          }
          
          __weak LMFirstViewController *weakSelf = self;
          [weakSelf removeActivityIndicator];
          
        }];
      }
      else
      {
        if ([USER_MANAGER getUserStatus] == KAUserGuest)
        {
          [UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForGuestUser];
        }
        else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
        {
          [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];

        }
        else if ([USER_MANAGER getUserStatus] == KAUserStatusActive)
        {
          [UIAlertView alertWithCause:kAlertNoMoreGoogleImageCreditsForActiveUser];
        }
      }
    

    }
    return YES;
  }
}


#pragma mark - Show details

- (void)showDetails:(LMItem *)item
{
    LMDetailViewController *detailViewController = [LMDetailViewController new];
    [detailViewController configureWithItem:item];
    [self.navigationController pushViewController:detailViewController animated:YES];

}


#pragma mark - SearchIconLabel

- (UILabel *)searchIconLabel
{
    UILabel *searchIcon = [UILabel new];
    
    [searchIcon setText:[[NSString alloc] initWithUTF8String:"  \xF0\x9F\x94\x8D"]];
    [searchIcon sizeToFit];
    
    return searchIcon;
}


#pragma mark - Show Settings

- (void)showSettings
{
    LMSettingsViewController *settingsControler = [LMSettingsViewController new];
    [self.navigationController pushViewController:settingsControler animated:YES];
}

#pragma mark - Observers

- (void)configureObservers
{
    [super configureObservers];
}


- (void)dismissObservers
{
    [super dismissObservers];
}


#pragma mark -
@end
