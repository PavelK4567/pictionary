//
//  CaptureViewController.m
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 Darko Trpevski. All rights reserved.
//


#import "CaptureViewController.h"
#import "UIImage+ImageResizing.h"
#import "LMPictureProcessingViewController.h"

@interface CaptureViewController ()

- (IBAction)captureButtonAction:(id)sender;

@end


@implementation CaptureViewController


#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  [self loadGUI];
  [self layoutGUI];

  [_avCaptureSessionController startRunning];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

  [_avCaptureSessionController stopRunning];
}


- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
  self.view = nil;
}


#pragma mark - LoadGUI

- (void)loadGUI
{
  [self loadCapture];
}


#pragma mark - LoadCapture

- (void)loadCapture
{
  _viewCapture.backgroundColor = [UIColor blackColor];

  _avCaptureSessionController = [[DTAVCaptureSessionController alloc] init];

  double delayInSeconds = 0.5;
  dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds));
  dispatch_after(delayTime, dispatch_get_main_queue(), ^(void) {
    [_avCaptureSessionController loadWithCaptureDevicePosition:AVCaptureDevicePositionBack];
    _viewCapture.backgroundColor = [UIColor clearColor];
  });
}


#pragma mark - LayoutGUI

- (void)layoutGUI
{
  [_avCaptureSessionController loadForView:self.view];

  [self.view bringSubviewToFront:_imageViewMask];
  [self.view bringSubviewToFront:_viewCapture];
  [self.view bringSubviewToFront:_labelSubtitle];
  [self.view bringSubviewToFront:_buttonCaptureImage];

  _buttomView.y = self.view.height - _buttomView.height;
  _buttonCaptureImage.y = self.view.height - _buttonCaptureImage.height;
  _buttomLineView.y = _buttomView.y - kButtomLinePadding;
}


#pragma mark - CaptureButtonAction

- (IBAction)captureButtonAction:(id)sender
{

  __weak CaptureViewController *weakSelf = self;

  [_avCaptureSessionController captureImageWithCompletionBlock:^(UIImage *image, NSError *error) {

    if (!error && image) {
      [self addActivityIndicator];
      [UIImage adjustImage:image
                 rootFrame:weakSelf.view.frame
                   toFrame:weakSelf.viewCapture.frame
           completionBlock:^(UIImage *image) {

             if (weakSelf.completionBlock)
               weakSelf.completionBlock(image);

             [self removeActivityIndicator];
             [self setIsLandscapeImage:isLandscape];
             [self showPicture:[LMHelper saveImage:image]];
             image = nil;

           }];
    }
  }];
}


#pragma mark - ShowPicture

- (void)showPicture:(NSString *)path
{
  NSURL *urlPath = [[NSURL URLWithString:path] absoluteURL];
  [ urlPath setResourceValue: [ NSNumber numberWithBool: YES ] forKey: NSURLIsExcludedFromBackupKey error: nil ];
  if (![ReachabilityHelper reachable])
  {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
    return;
  }

  if ([USER_MANAGER canRecognize])
  {
    if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
    {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
      return;
    }

    LMPictureProcessingViewController *controler = [LMPictureProcessingViewController new];
    controler.photoPath = urlPath.absoluteString;
    controler.isLandscapeImage = self.isLandscapeImage;
      
    __weak CaptureViewController *_weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{ [_weakSelf.navigationController pushViewController:controler animated:YES]; });
  }
  else
  {
    if ([USER_MANAGER getUserStatus] == KAUserGuest)
    {
      [self showPopUpForGuest];
    }
    else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
    {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
    }
    else if ([USER_MANAGER getUserStatus] == KAUserStatusActive)
    {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForActiveUser];
    }
  }
}


- (void)showPopUpForGuest
{
  UIAlertView *confirmationAlert = [UIAlertView alertViewWithTitle:@""
    message:Localized(@"T074")
    cancelButtonTitle:Localized(@"T074.2")
    otherButtonTitles:@[ Localized(@"T074.1") ]
    onDismiss:^(int buttonIndex) { [APP_DELEGATE buildLoginStack]; }
    onCancel:^{ return; }];
  [confirmationAlert show];
}


#pragma mark - ShouldAutorotate

- (BOOL)shouldAutorotate
{
  return NO;
}


- (NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Dealloc

- (void)dealloc
{
  _imageViewMask = nil;
  _labelSubtitle = nil;
  _buttonCaptureImage = nil;
  _buttomView = nil;
  _buttomLineView = nil;
  _avCaptureSessionController = nil;
}

#pragma mark -
@end
