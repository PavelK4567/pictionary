//  LMResultCell.m
//  Created by Dimitar Tasev on 24.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMResultCell.h"
#import "LMItem.h"
#import "LMTag.h"
#import "UIImageView+WebCache.h"

@interface LMResultCell ()

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemTitle;

@end

@implementation LMResultCell

- (void)configureWithModel:(LMItem *)item {
	NSString *title = 0;
	for (NSString *tagUUID in item.items) {
		if (title) {
			title = [title stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] native]];
		}
		else {
			title = [[ITEM_MANAGER tagWithId:tagUUID] native];
		}
	}
	[self.itemTitle setText:title];
	[self.itemImage setImage:[UIImage imageWithContentsOfFile:[[ITEM_MANAGER destinationPath] stringByAppendingFormat:@"/%@.png", item.imageThumb]]];
	DLog(@"ITEM\n\tLoaded title: %@\n\tLoaded image: %@", title, [[ITEM_MANAGER destinationPath] stringByAppendingFormat:@"/%@.png", item.imageThumb]);
}

@end
