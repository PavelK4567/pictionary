//  ReachabilityHelper.h
//  Created by Dimitar Tasev on 20140401.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.



@interface ReachabilityHelper : NSObject

+ (void)setupReachability;

+ (BOOL)hasCellular;
+ (BOOL)hasCellularData;
+ (BOOL)reachable;
+ (BOOL)reachableOverWifi;

@end
