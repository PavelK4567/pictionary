//  NSArray+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface NSArray (Extras)

+ (NSArray *)randomisedArray:(NSArray *)input;
+ (id)randomObject:(NSArray *)input;
+ (void)logItems:(NSArray *)input;

@end
