//  UIImage+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UIImage+Extras.h"
#include <objc/runtime.h>


@implementation UIImage (Extras)

+ (void)load
{
  if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && ([UIScreen mainScreen].bounds.size.height > 480.0f)) {

    // Exchange XIB loading implementation
    Method m1 = class_getInstanceMethod(NSClassFromString(@"UIImageNibPlaceholder"), @selector(initWithCoder:));
    Method m2 = class_getInstanceMethod(self, @selector(initWithCoderH568:));
    method_exchangeImplementations(m1, m2);

    // Exchange imageNamed: implementation
    method_exchangeImplementations(class_getClassMethod(self, @selector(imageNamed:)), class_getClassMethod(self, @selector(imageNamedH568:)));
  }
}

+ (UIImage *)imageNamedH568:(NSString *)imageName
{
  return [UIImage imageNamedH568:[self renameImageNameForH568:imageName]];
}

- (id)initWithCoderH568:(NSCoder *)aDecoder
{
  NSString *resourceName = [aDecoder decodeObjectForKey:@"UIResourceName"];
  NSString *resourceH568 = [UIImage renameImageNameForH568:resourceName];

  // If no 568h version, load as default
  if ([resourceName isEqualToString:resourceH568]) {
    return [self initWithCoderH568:aDecoder];
  }
  // If 568h exists, load with [UIImage imageNamed:]
  else {
    return [UIImage imageNamedH568:resourceH568];
  }
}

+ (NSString *)renameImageNameForH568:(NSString *)imageName
{

  NSMutableString *imageNameMutable = [imageName mutableCopy];

  // Delete png extension
  NSRange extension = [imageName rangeOfString:@".png" options:NSBackwardsSearch | NSAnchoredSearch];
  if (extension.location != NSNotFound) {
    [imageNameMutable deleteCharactersInRange:extension];
  }

  // Look for @2x to introduce -568h string
  NSRange retinaAtSymbol = [imageName rangeOfString:@"@2x"];
  if (retinaAtSymbol.location != NSNotFound) {
    [imageNameMutable insertString:@"-568h" atIndex:retinaAtSymbol.location];
  } else {
    [imageNameMutable appendString:@"-568h@2x"];
  }

  // Check if the image exists and load the new 568 if so or the original name if not
  NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageNameMutable ofType:kPreferencesFilePng];
  if (imagePath) {
    // Remove the @2x to load with the correct scale 2.0
    [imageNameMutable replaceOccurrencesOfString:@"@2x" withString:@"" options:NSBackwardsSearch range:NSMakeRange(0, [imageNameMutable length])];
    return imageNameMutable;
  } else {
    return imageName;
  }
}

#pragma mark - Stretching

+ (UIImage *)stretchedImage:(UIImage *)image insets:(UIEdgeInsets)insets
{
  if (![image respondsToSelector:@selector(resizableImageWithCapInsets:resizingMode:)]) {
    return [image resizableImageWithCapInsets:insets];
  }

  return [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeTile];
}

+ (UIImage *)buttonStretchedImage:(UIImage *)image insets:(UIEdgeInsets)insets
{
  if (![image respondsToSelector:@selector(resizableImageWithCapInsets:resizingMode:)]) {
    return [image stretchableImageWithLeftCapWidth:insets.left topCapHeight:insets.top];
  }

  return [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
}

#pragma mark -

+ (UIImage *)imageWithColor:(UIColor *)color
{
  CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);

  UIGraphicsBeginImageContext(rect.size);

  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, color.CGColor);
  CGContextFillRect(context, rect);

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();

  UIGraphicsEndImageContext();
  return image;
}

- (UIImage *)imageWithFixedOrientation
{
  UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
  [self drawInRect:(CGRect) { 0, 0, self.size }];
  UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return normalizedImage;
}

- (CGImageRef)cgimageWithFixedOrientation
{
  
  if (self.imageOrientation == UIImageOrientationDown) {
    // retaining because caller expects to own the reference
    CGImageRetain([self CGImage]);
    return [self CGImage];
  }
  UIGraphicsBeginImageContext(self.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  if (self.imageOrientation == UIImageOrientationRight) {
    CGContextRotateCTM(context, 90 * M_PI / 180);
  } else if (self.imageOrientation == UIImageOrientationLeft) {
    CGContextRotateCTM(context, -90 * M_PI / 180);
  } else if (self.imageOrientation == UIImageOrientationUp) {
    CGContextRotateCTM(context, 180 * M_PI / 180);
  }
  [self drawAtPoint:CGPointMake(0, 0)];
  CGImageRef cgImage = CGBitmapContextCreateImage(context);
  UIGraphicsEndImageContext();

  return cgImage;
}

//- (UIImage *)resizedImageByWidth:(NSUInteger)width
//{
//  CGImageRef imgRef = [self cgimageWithFixedOrientation];
//  CGFloat original_width = CGImageGetWidth(imgRef);
//  CGFloat original_height = CGImageGetHeight(imgRef);
//  CGFloat ratio = width / original_width;
//  CGImageRelease(imgRef);
//  return [self drawImageInBounds:CGRectMake(0, 0, width, round(original_height * ratio))];
//}

- (UIImage *)resizedImageByHeight:(NSUInteger)height
{
  CGImageRef imgRef = [self cgimageWithFixedOrientation];
  CGFloat original_width = CGImageGetWidth(imgRef);
  CGFloat original_height = CGImageGetHeight(imgRef);
  CGFloat ratio = height / original_height;
  CGImageRelease(imgRef);
  return [self drawImageInBounds:CGRectMake(0, 0, round(original_width * ratio), height)];
}

- (UIImage *)resizedImageWithMinimumSize:(CGSize)size
{
  CGImageRef imgRef = [self cgimageWithFixedOrientation];
  CGFloat original_width = CGImageGetWidth(imgRef);
  CGFloat original_height = CGImageGetHeight(imgRef);
  CGFloat width_ratio = size.width / original_width;
  CGFloat height_ratio = size.height / original_height;
  CGFloat scale_ratio = width_ratio > height_ratio ? width_ratio : height_ratio;
  CGImageRelease(imgRef);
  return [self drawImageInBounds:CGRectMake(0, 0, round(original_width * scale_ratio), round(original_height * scale_ratio))];
}

- (UIImage *)resizedImageWithMaximumSize:(CGSize)size
{
  CGImageRef imgRef = [self cgimageWithFixedOrientation];
  CGFloat original_width = CGImageGetWidth(imgRef);
  CGFloat original_height = CGImageGetHeight(imgRef);
  CGFloat width_ratio = size.width / original_width;
  CGFloat height_ratio = size.height / original_height;
  CGFloat scale_ratio = width_ratio < height_ratio ? width_ratio : height_ratio;
  CGImageRelease(imgRef);
  return [self drawImageInBounds:CGRectMake(0, 0, round(original_width * scale_ratio), round(original_height * scale_ratio))];
}

- (UIImage *)drawImageInBounds:(CGRect)bounds
{
  UIGraphicsBeginImageContext(bounds.size);
  [self drawInRect:bounds];
  UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return resizedImage;
}

- (UIImage *)croppedImageWithRect:(CGRect)rect
{
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, self.size.width, self.size.height);
  CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
  [self drawInRect:drawRect];
  UIImage *subImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return subImage;
}

- (UIImage *)cropAndResizeto:(CGSize)size
{
  UIImage *image = [self resizedImageWithMinimumSize:size];
  CGSize imageSize = [image size];
  CGPoint imageOrigin = CGPointMake((imageSize.width - size.width) / 2, (imageSize.height - size.height) / 2);
  image = [image croppedImageWithRect:(CGRect) { imageOrigin, size }];
  return image;

  //  CGSize imageSize = [self size];
  //  CGFloat ratio = MAX(size.height / imageSize.height, size.width / imageSize.width);
  //
  //  CGSize bounds = CGSizeMake(imageSize.width / ratio, imageSize.height / ratio);
  //  UIGraphicsBeginImageContext(bounds);
  //  CGContextRef context = UIGraphicsGetCurrentContext();
  //  CGContextClipToRect(context, CGRectMake((bounds.width - size.width) / 2, (bounds.height - size.height) / 2, size.width, size.height));
  //  [self drawInRect:(CGRect) { CGPointZero, bounds }];
  //  UIImage *tempImage = UIGraphicsGetImageFromCurrentImageContext();
  //  UIGraphicsEndImageContext();
  //
  //  return tempImage;
}

@end
