//  UIAlertView+Extras.h
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


typedef enum {
	kAlertError,
	kAlertException,
	kAlertConfigMissing,
	kAlertNoConnection,
	kAlertNoInternetConnection,
	kAlertNoInternetConnectionWhenClickingCameraButton,
	kAlertServerUnreachable,
	kAlertNoLocation,
	kAlertLocationBatteryUse,
	kAlertNoCallsPossible,
	kAlertNoSMSPossible,
	kAlertNoEmailPossible,
	kAlertWrongCredentials,
	kAlertMissingfields,
	kAlertMissingUser,
	kAlertUploadSucess,
	kAlertNoName,
	kAlertPhoneNumberShorterThan8Digit,
	kAlertNotRegisteredUser,
	kAlertCanceledUser,
  kAlertCanceledNativeUser,
	kAlertDisabledUser,
	kAlertCodeNotValid,
  kAlertDeletedConfirmation,
	kAlertPredefinedPictureCanBeDeleted,
  kAlertPictureCanBeDeletedInClosedGallery,
	kAlertNoMoreCamFindCreditsForGuestUser,
	kAlertNoMoreCamFindCreditsForDisabledUser,
	kAlertNoMoreCamFindCreditsForActiveUser,
	kAlertRefusedPhotoMessage,
	kAlertErrorTranslateWord,
	kAlertNoMoreGoogleImageCreditsForGuestUser,
	kAlertNoMoreGoogleImageCreditsForDisabledUser,
	kAlertNoMoreGoogleImageCreditsForActiveUser,
  kAlertOneLetterToSearch,
	kAlertGoogleImageAPINoResults,
	kAlertGoogleImageAPITechnicalError,
	kItemSavedSuccessfullyConfirmation,
	kItemSavedFailConfirmation,
	kItemFromGoogleSavedFailConfirmation,
	kAlertNoInternetForGoogleImage,
	kAlertNoInternetForCamFind,
	kAlertCamFindAPITechnicalError,
	kAlertNoResultFromCamFindMessage,
	kAlertCodeNotMatchMSISDN,
	kAlertPhoneNumberNotValid
} EAlertViewCause;


typedef enum { kAlertInformation, kAlertApprove, kAlertViewWarning, kAlertViewError } EAlertViewType;

@interface UIAlertView (Extras) <UIAlertViewDelegate>

+ (void)alertWithCause:(EAlertViewCause)cause;
+ (void)alertWithCause:(EAlertViewCause)cause success:(void (^)(int))success failure:(void (^)(void))failure;
+ (void)createAlertViewTitle:(NSString *)title
                     message:(NSString *)message
                        type:(EAlertViewType)type
                     success:(void (^)(int))success
                     failure:(void (^)(void))failure;

#pragma mark - MKBlockAdditions

@property (nonatomic, copy) DismissBlock dismissBlock;
@property (nonatomic, copy) CancelBlock cancelBlock;

+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle;
+ (UIAlertView *)alertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                  cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitles:(NSArray *)otherButtons
                          onDismiss:(DismissBlock)dismissed
                           onCancel:(CancelBlock)cancelled;

@end
