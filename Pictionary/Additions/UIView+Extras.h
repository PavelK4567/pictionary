//  UIView+Extras.h
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface UIView (Extras)

- (void)setBorder:(CGFloat)borderWidth color:(UIColor *)color;
- (void)setCornerRadius:(CGFloat)cornerRadius;

- (CGPoint)origin;
- (CGSize)size;
- (void)setOrigin:(CGPoint)origin;
- (void)setSize:(CGSize)size;

- (CGFloat)x;
- (CGFloat)y;
- (CGFloat)width;
- (CGFloat)height;
- (void)setX:(CGFloat)x;
- (void)setY:(CGFloat)y;
- (void)setWidth:(CGFloat)width;
- (void)setHeight:(CGFloat)height;

- (CGFloat)left;
- (CGFloat)top;
- (CGFloat)right;
- (CGFloat)bottom;
- (void)setLeft:(CGFloat)left;
- (void)setTop:(CGFloat)top;
- (void)setRight:(CGFloat)right;
- (void)setBottom:(CGFloat)bottom;

- (CGPoint)topLeft;
- (CGPoint)bottomLeft;
- (CGPoint)topRight;
- (CGPoint)bottomRight;
- (void)setTopLeft:(CGPoint)topLeft;
- (void)setBottomLeft:(CGPoint)bottomLeft;
- (void)setTopRight:(CGPoint)topRight;
- (void)setBottomRight:(CGPoint)bottomRight;

- (CGFloat)midX;
- (CGFloat)maxX;
- (CGFloat)midY;
- (CGFloat)maxY;
- (void)setMidX:(CGFloat)midX;
- (void)setMaxX:(CGFloat)maxX;
- (void)setMidY:(CGFloat)midY;
- (void)setMaxY:(CGFloat)maxY;

+ (void)placeView:(UIView *)addView belowView:(UIView *)fixedView withPadding:(CGFloat)padding;
+ (void)placeView:(UIView *)addView belowAndCenterView:(UIView *)fixedView withPadding:(CGFloat)padding;
+ (void)placeView:(UIView *)addView rightFromView:(UIView *)fixedView withPadding:(CGFloat)padding;
+ (void)placeView:(UIView *)addView rightAndCenterFromView:(UIView *)fixedView withPadding:(CGFloat)padding;
+ (void)setOrigin:(CGPoint)origin forView:(UIView *)view;
+ (void)setSize:(CGSize)size forView:(UIView *)view;
+ (void)setX:(CGFloat)x andY:(CGFloat)y forView:(UIView *)view;
+ (void)setWidth:(CGFloat)width andHeight:(CGFloat)height forView:(UIView *)view;

- (void)ceilFrame;
- (UIImage *)getImage;
- (UIViewController *)viewController;
- (void)placeBelowView:(UIView *)fixedView withPadding:(CGFloat)padding;
- (void)placeCenterAndBelowView:(UIView *)fixedView withPadding:(CGFloat)padding;
- (void)placeRightFromView:(UIView *)fixedView withPadding:(CGFloat)padding;
- (void)placeBelowViewAndSizeToFit:(UIView *)fixedView withPadding:(CGFloat)padding;
- (void)placeCenterAndRightFromView:(UIView *)fixedView withPadding:(CGFloat)padding;
- (void)setWidth:(CGFloat)width andHeight:(CGFloat)height;
- (void)setX:(CGFloat)x andY:(CGFloat)y;
- (void)placeAboveView:(UIView *)fixedView withPadding:(CGFloat)padding;

+ (UIView *)getViewWithClass:(Class)viewClass fromNibNamed:(NSString *)nibName;

@end
