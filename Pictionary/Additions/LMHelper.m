//
//  LMHelper.m
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMHelper.h"

@implementation LMHelper

#pragma mark - Load image from assets

+ (UIImage*)loadImageForImageName:(NSString *)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:
                      imageName ];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    return image;
}

#pragma mark - Save image to assets

+ (NSString *)saveImage:(UIImage *)image
{
    NSString *path;
    if (image != nil)
    {
        
        NSString *uniqueName = [NSString stringWithFormat:@"%@%lu", @"pic_", (unsigned long)([[NSDate date] timeIntervalSince1970]*10.0)];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        path = [documentsDirectory stringByAppendingPathComponent:
                uniqueName ];
        path = [path stringByAppendingString:kPreferencesFileJpg];
        NSData *data = UIImageJPEGRepresentation(image, 50.0);
        [data writeToFile:path atomically:YES];
        
    }
    
    return path;
}

+ (void)removeImagesFromDocumentsFolder
{
    NSString *jpgFile = @"jpg";
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: documentsDirectory error:&error];
    NSEnumerator *enumerator = [contents objectEnumerator];
    NSString *filename;
    
    while ((filename = [enumerator nextObject])) {
        if ([[filename pathExtension] isEqualToString:jpgFile]) {
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
}

@end
