//  NSArray+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "NSArray+Extras.h"


@implementation NSArray (Extras)

+ (NSArray *)randomisedArray:(NSArray *)input {
  NSMutableArray *randomised = [NSMutableArray arrayWithCapacity:[input count]];
  for (id object in self) {
    NSUInteger index = randomInRange(0, [randomised count]);
    [randomised insertObject:object atIndex:index];
  }
  return randomised;
}

+ (id)randomObject:(NSArray *)input {
  return input[randomInRange(0, [input count] - 1)];
}

+ (void)logItems:(NSArray *)input {
  for (id item in input)
    NSLog(@"%@", item);
}

@end
