//  UIAlertView+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UIAlertView+Extras.h"
#import <objc/runtime.h>


#define ALERT_ERROR @"Alert.ErrorTitle"
#define ALERT_ERROR_MSG @"Alert.ErrorMessage"
#define ALERT_EXCEPTION @"Alert.ExceptionTitle"
#define ALERT_EXCEPTION_MSG @"Alert.ExceptionMessage"
#define ALERT_CONFIGMISSING @"Alert.ConfigMissingTitle"
#define ALERT_CONFIGMISSING_MSG @"Alert.ConfigMissingMessage"
#define ALERT_NOCONNECTION @"Alert.NoConnectionTitle"
#define ALERT_NOCONNECTION_MSG @"Alert.NoConnectionMessage"
#define ALERT_SERVERUNREACHABLE @"Alert.ServerUnreachableTitle"
#define ALERT_SERVERUNREACHABLE_MSG @"Alert.ServerUnreachableMessage"
#define ALERT_NOLOCATION @"Alert.NoLocationTitle"
#define ALERT_NOLOCATION_MSG @"Alert.NoLocationMessage"
#define ALERT_LOCATIONBATTERY @"Alert.LocationBatteryTitle"
#define ALERT_LOCATIONBATTERY_MSG @"Alert.LocationBatteryMessage"
#define ALERT_NOCALLS @"Alert.CantCallPhoneTitle"
#define ALERT_NOCALLS_MSG @"Alert.CantCallPhoneMessage"
#define ALERT_NOSMS @"Alert.CantSendSMSTitle"
#define ALERT_NOSMS_MSG @"Alert.CantSendSMSMessage"
#define ALERT_NOEMAIL @"Alert.CantSendMailTitle"
#define ALERT_NOEMAIL_MSG @"Alert.CantSendMailMessage"
#define ALERT_WRONGCREDENTIALS @"Alert.WrongCredentialsTitle"
#define ALERT_WRONGCREDENTIALS_MSG @"Alert.WrongCredentialsMessage"
#define ALERT_MISSINGFIELDS @"Alert.MissingFieldsTitle"
#define ALERT_MISSINGFIELDS_MSG @"Alert.MissingFieldsMessage"
#define ALERT_MISSINGUSER @"Alert.MissingUserTitle"
#define ALERT_MISSINGUSER_MSG @"Alert.MissingUserMessage"

#define ALERT_TYPE_SUCCESS @"Alert.Success"
#define ALERT_TYPE_SUCCESS_MSG @"Alert.Success.Msg"

#define ALERT_TYPE_NO_TITLE @"Alert.NoTitle"
#define ALERT_TYPE_NO_TITLE_MSG @"Alert.NoTitle.Msg"


static char DISMISS_IDENTIFER;
static char CANCEL_IDENTIFER;


@implementation UIAlertView (Extras)

+ (void)alertWithCause:(EAlertViewCause)cause {
	[UIAlertView alertWithCause:cause success:0 failure:0];
}

+ (void)alertWithCause:(EAlertViewCause)cause success:(void (^)(int))success failure:(void (^)(void))failure {
	NSString *__title = 0;
	NSString *__message = 0;
	EAlertViewType __type = kAlertInformation;
  
	switch (cause) {
		case kAlertException:
			__title = Localized(ALERT_EXCEPTION);
			__message = Localized(ALERT_EXCEPTION_MSG);
			break;
      
		case kAlertNoConnection:
			__title = Localized(ALERT_NOCONNECTION);
			__message = Localized(ALERT_NOCONNECTION_MSG);
			break;
      
		case kAlertConfigMissing:
			__title = Localized(ALERT_CONFIGMISSING);
			__message = Localized(ALERT_CONFIGMISSING_MSG);
			break;
      
		case kAlertServerUnreachable:
			__title = Localized(ALERT_SERVERUNREACHABLE);
			__message = Localized(ALERT_SERVERUNREACHABLE_MSG);
			break;
      
		case kAlertNoLocation:
			__title = Localized(ALERT_NOLOCATION);
			__message = Localized(ALERT_NOLOCATION_MSG);
			__type = kAlertApprove;
			break;
      
		case kAlertLocationBatteryUse:
			__title = Localized(ALERT_LOCATIONBATTERY);
			__message = Localized(ALERT_LOCATIONBATTERY_MSG);
			break;
      
		case kAlertNoCallsPossible:
			__title = Localized(ALERT_NOCALLS);
			__message = Localized(ALERT_NOCALLS_MSG);
			break;
      
		case kAlertNoSMSPossible:
			__title = Localized(ALERT_NOSMS);
			__message = Localized(ALERT_NOSMS_MSG);
			break;
      
		case kAlertNoEmailPossible:
			__title = Localized(ALERT_NOEMAIL);
			__message = Localized(ALERT_NOEMAIL_MSG);
			break;
      
		case kAlertWrongCredentials:
			__title = Localized(ALERT_WRONGCREDENTIALS);
			__message = Localized(ALERT_WRONGCREDENTIALS_MSG);
			break;
      
		case kAlertMissingfields:
			__title = Localized(ALERT_MISSINGFIELDS);
			__message = Localized(ALERT_MISSINGFIELDS_MSG);
			break;
      
		case kAlertMissingUser:
			__title = Localized(ALERT_MISSINGUSER);
			__message = Localized(ALERT_MISSINGUSER_MSG);
			break;
      
		case kAlertUploadSucess:
			__title = Localized(ALERT_TYPE_SUCCESS);
			__message = Localized(ALERT_TYPE_SUCCESS_MSG);
			break;
      
		case kAlertNoName:
			__title = Localized(ALERT_TYPE_NO_TITLE);
			__message = Localized(ALERT_TYPE_NO_TITLE_MSG);
			break;
      
		case kAlertPhoneNumberNotValid:
			__title = @"";
			__message = Localized(@"T006");
			break;
      
		case kAlertPhoneNumberShorterThan8Digit:
			__title = @"";
			__message = Localized(@"T006");
			break;
      
		case kAlertNotRegisteredUser:
			__title = @"";
			__message = Localized(@"T008");
			break;
      
		case kAlertCanceledUser:
			__title = @"";
			__message = Localized(@"T010");
			break;
    case kAlertCanceledNativeUser:
      __title = @"";
      __message = [NSString stringWithFormat:Localized(@"T010.b"),[USER_MANAGER takeStringFromDate]];
      break;
      
		case kAlertDisabledUser:
			__title = @"";
			__message = Localized(@"T011");
			break;
      
		case kAlertCodeNotValid:
			__title = @"";
			__message = Localized(@"T021");
			break;
      
		case kAlertCodeNotMatchMSISDN:
			__title = @"";
			__message = Localized(@"T022");
			break;
      
      
		case kAlertDeletedConfirmation:
			__title = @"";
			__message = Localized(@"T106");
			break;
      
		case kAlertNoInternetConnection:
			__title = @"";
			__message = Localized(@"T135");
			break;
      
		case kItemSavedFailConfirmation:
			__title = @"";
			__message = Localized(@"T072");
			break;
      
		case kAlertNoInternetConnectionWhenClickingCameraButton:
			__title = @"";
			__message = Localized(@"T073");
			break;
      
		case kAlertNoMoreCamFindCreditsForGuestUser:
			__title = @"";
			__message = Localized(@"T074");
			break;
      
		case kAlertNoMoreCamFindCreditsForDisabledUser:
			__title = @"";
			__message = Localized(@"T075");
			break;
      
		case kAlertNoMoreCamFindCreditsForActiveUser:
			__title = @"";
      if([USER_MANAGER isNative]){
        __message = [NSString stringWithFormat:Localized(@"T076.a")];
      }else{
        __message = [NSString stringWithFormat:Localized(@"T076"),[USER_MANAGER nextBillingDateString]];
      }
			break;
      
		case kAlertNoMoreGoogleImageCreditsForGuestUser:
			__title = @"";
			__message = Localized(@"T129");
			break;
      
		case kAlertNoMoreGoogleImageCreditsForActiveUser:
			__title = @"";
			__message = Localized(@"T129");;
			break;
      
		case kAlertNoMoreGoogleImageCreditsForDisabledUser:
			__title = @"";
			__message = Localized(@"T075");;
			break;
      
		case kAlertErrorTranslateWord:
			__title = @"";
			__message = Localized(@"T123");
            break;
		case kAlertGoogleImageAPITechnicalError:
			__title = @"";
			__message = Localized(@"T124");
			break;
      
		case kAlertNoInternetForGoogleImage:
			__title = @"";
			__message = Localized(@"T125");
			break;
      
		case kAlertGoogleImageAPINoResults:
			__title = @"";
			__message = Localized(@"T126");
			break;
    case kAlertOneLetterToSearch:
      __title = @"";
      __message = Localized(@"T126.1");
      break;
		case kItemSavedSuccessfullyConfirmation:
			__title = @"";
			__message = Localized(@"T127");
			break;
      
		case kItemFromGoogleSavedFailConfirmation:
			__title = @"";
			__message = Localized(@"T128");
			break;
    case kAlertPredefinedPictureCanBeDeleted:
      __title = @"";
      __message = Localized(@"T115");
      break;
    case kAlertPictureCanBeDeletedInClosedGallery:
      __title = @"";
      __message = Localized(@"T116");
      break;
     
		case kAlertNoResultFromCamFindMessage:
			__title = @"";
			__message = Localized(@"T082");
			break;
      
		case kAlertRefusedPhotoMessage:
			__title = @"";
			__message = Localized(@"T084");
			break;
      
		case kAlertNoInternetForCamFind:
			__title = @"";
			__message = Localized(@"T083");
			break;
      
      
		case kAlertCamFindAPITechnicalError:
			__title = @"";
			__message = Localized(@"T094");
			break;
      
		case kAlertError:
			break;
      
		default:
			__title = Localized(ALERT_ERROR);
			__message = Localized(ALERT_ERROR_MSG);
			break;
	}
  
	[UIAlertView createAlertViewTitle:__title message:__message type:__type success:success failure:failure];
}

+ (void)createAlertViewTitle:(NSString *)title
                     message:(NSString *)message
                        type:(EAlertViewType)type
                     success:(void (^)(int))success
                     failure:(void (^)(void))failure {
	UIAlertView *alert = 0;
	if (type == kAlertApprove) {
		alert = [UIAlertView alertViewWithTitle:title
		                                message:message
		                      cancelButtonTitle:Localized(@"Alert.Cancel")
		                      otherButtonTitles:@[Localized(@"Alert.OK")]
		                              onDismiss: ^(int buttonIndex) {
                                    if (buttonIndex == -1 && success)
                                      success(buttonIndex);
                                    else if (buttonIndex == 0 && failure)
                                      failure();
                                  }
             
		                               onCancel: ^{ failure(); }];
	}
	else {
		alert = [UIAlertView alertViewWithTitle:title message:message cancelButtonTitle:Localized(@"T053")];
	}
  
	[alert show];
}

#pragma mark - MKBlockAdditions

@dynamic cancelBlock;
@dynamic dismissBlock;

- (void)setDismissBlock:(DismissBlock)dismissBlock {
	objc_setAssociatedObject(self, &DISMISS_IDENTIFER, dismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (DismissBlock)dismissBlock {
	return objc_getAssociatedObject(self, &DISMISS_IDENTIFER);
}

- (void)setCancelBlock:(CancelBlock)cancelBlock {
	objc_setAssociatedObject(self, &CANCEL_IDENTIFER, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CancelBlock)cancelBlock {
	return objc_getAssociatedObject(self, &CANCEL_IDENTIFER);
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                  cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitles:(NSArray *)otherButtons
                          onDismiss:(DismissBlock)dismissed
                           onCancel:(CancelBlock)cancelled {
	UIAlertView *alert =
  [[UIAlertView alloc] initWithTitle:title message:message delegate:[self class] cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
  
	[alert setDismissBlock:dismissed];
	[alert setCancelBlock:cancelled];
  
	for (NSString *buttonTitle in otherButtons) {
		[alert addButtonWithTitle:buttonTitle];
	}
	return alert;
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message {
	return [UIAlertView alertViewWithTitle:title message:message cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")];
}

+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
	return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == [alertView cancelButtonIndex]) {
		if (alertView.cancelBlock) {
			alertView.cancelBlock();
		}
	}
	else {
		if (alertView.dismissBlock) {
			alertView.dismissBlock(buttonIndex - 1); // cancel button is button 0
		}
	}
}

@end
