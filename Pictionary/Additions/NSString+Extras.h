//  NSString+Extras.h
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface NSString (Extras)

// unicode
+ (BOOL)containsUnicode:(NSString *)string;

// url schemes
+ (NSString *)phoneURL:(NSString *)number;
+ (NSString *)emailURL:(NSString *)path;
+ (NSString *)linkURL:(NSString *)path;

+ (BOOL)validatePassword:(NSString *)pass;
+ (BOOL)validateEmail:(NSString *)email;

+ (NSString *)documentsDir;
+ (NSString *)libraryDir;
+ (NSString *)cachesDir;

+ (NSString *)getUUID;
+ (NSString *)md5:(NSString *)input;
+ (NSString *)getSHA1ForString:(NSString *)input;
+ (NSData *)dataFromBase64String:(NSString *)input;
+ (NSString *)AES256EncryptedBase64String:(NSString *)input key:(NSString *)key;
+ (NSString *)AES256DecryptedBase64String:(NSString *)input key:(NSString *)key;

@end

#define Localized(exp) NSLocalizedString(exp, 0)
