//  UIColor+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface UIColor (Extras)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
