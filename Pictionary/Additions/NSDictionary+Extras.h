//  NSDictionary+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface NSDictionary (Extras)

- (NSString *)queryString;
- (id)parseField:(NSString *)fieldKey withClass:(Class)fieldClass;
@end
