//  UIView+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UIView+Extras.h"


@implementation UIView (Extras)

- (void)setBorder:(CGFloat)borderWidth color:(UIColor *)color {
  self.layer.borderColor = color.CGColor;
  self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
  self.layer.cornerRadius = cornerRadius;
  self.layer.masksToBounds = YES;
  self.clipsToBounds = YES;
}

- (void)setX:(CGFloat)x {
  CGRect rect = self.frame;

  self.frame = CGRectMake(x, rect.origin.y, rect.size.width, rect.size.height);
}

- (void)setY:(CGFloat)y {
  CGRect rect = self.frame;

  self.frame = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
}

- (void)setWidth:(CGFloat)width {
  CGRect rect = self.frame;

  self.frame = CGRectMake(rect.origin.x, rect.origin.y, width, rect.size.height);
}

- (void)setHeight:(CGFloat)height {
  CGRect rect = self.frame;

  self.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);
}

- (CGFloat)x {
  return self.frame.origin.x;
}

- (CGFloat)y {
  return self.frame.origin.y;
}

- (CGFloat)width {
  return self.frame.size.width;
}

- (CGFloat)height {
  return self.frame.size.height;
}

#pragma mark -

+ (void)placeView:(UIView *)addView belowView:(UIView *)fixedView withPadding:(CGFloat)padding {
  CGRect addViewFrame = addView.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) + padding;
  addView.frame = addViewFrame;
}

+ (void)placeView:(UIView *)addView belowAndCenterView:(UIView *)fixedView withPadding:(CGFloat)padding {
  addView.center = fixedView.center;

  CGRect addViewFrame = addView.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) + padding;
  addView.frame = addViewFrame;
}

+ (void)placeView:(UIView *)addView rightFromView:(UIView *)fixedView withPadding:(CGFloat)padding {
  CGRect addViewFrame = addView.frame;
  addViewFrame.origin.x = CGRectGetMaxX(fixedView.frame) + padding;
  addView.frame = addViewFrame;
}

+ (void)placeView:(UIView *)addView rightAndCenterFromView:(UIView *)fixedView withPadding:(CGFloat)padding {
  addView.center = fixedView.center;

  CGRect addViewFrame = addView.frame;
  addViewFrame.origin.x = CGRectGetMaxX(fixedView.frame) + padding;
  addView.frame = addViewFrame;
}

+ (void)setOrigin:(CGPoint)origin forView:(UIView *)view {
  CGRect frame = view.frame;
  frame.origin = origin;
  view.frame = frame;
}

+ (void)setSize:(CGSize)size forView:(UIView *)view {
  CGRect frame = view.frame;
  frame.size = size;
  view.frame = frame;
}

+ (void)setX:(CGFloat)x andY:(CGFloat)y forView:(UIView *)view {
  CGRect frame = view.frame;
  frame.origin = CGPointMake(x, y);
  view.frame = frame;
}

+ (void)setWidth:(CGFloat)width andHeight:(CGFloat)height forView:(UIView *)view {
  CGRect frame = view.frame;
  frame.size = CGSizeMake(width, height);
  view.frame = frame;
}

#pragma mark -

- (CGFloat)left {
  return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
  CGRect frame = self.frame;
  frame.origin.x = x;
  self.frame = frame;
}

- (CGFloat)top {
  return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
  CGRect frame = self.frame;
  frame.origin.y = y;
  self.frame = frame;
}

- (CGFloat)right {
  return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
  CGRect frame = self.frame;
  frame.origin.x = right - frame.size.width;
  self.frame = frame;
}

- (CGFloat)bottom {
  return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
  CGRect frame = self.frame;
  frame.origin.y = bottom - frame.size.height;
  self.frame = frame;
}

- (CGFloat)midX {
  return CGRectGetMidX(self.frame);
}

- (void)setMidX:(CGFloat)midX {
  CGRect frame = self.frame;
  frame.origin.x = midX - frame.size.width / 2;
  self.frame = frame;
}

- (CGFloat)maxX {
  return CGRectGetMaxX(self.frame);
}

- (void)setMaxX:(CGFloat)maxX {
  CGRect frame = self.frame;
  frame.origin.x = maxX - frame.size.width;
  self.frame = frame;
}

- (CGFloat)midY {
  return CGRectGetMidY(self.frame);
}

- (void)setMidY:(CGFloat)midY {
  CGRect frame = self.frame;
  frame.origin.y = midY - frame.size.height / 2;
  self.frame = frame;
}

- (CGFloat)maxY {
  return CGRectGetMaxY(self.frame);
}

- (void)setMaxY:(CGFloat)maxY {
  CGRect frame = self.frame;
  frame.origin.y = maxY - frame.size.height;
  self.frame = frame;
}

- (CGPoint)origin {
  return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
  CGRect frame = self.frame;
  frame.origin = origin;
  self.frame = frame;
}

- (CGSize)size {
  return self.frame.size;
}

- (void)setSize:(CGSize)size {
  CGRect frame = self.frame;
  frame.size = size;
  self.frame = frame;
}

- (CGPoint)topLeft {
  return CGPointMake(self.left, self.top);
}

- (void)setTopLeft:(CGPoint)topLeft {
  self.top = topLeft.y;
  self.left = topLeft.x;
}

- (CGPoint)bottomLeft {
  return CGPointMake(self.left, self.bottom);
}

- (void)setBottomLeft:(CGPoint)bottomLeft {
  self.bottom = bottomLeft.y;
  self.left = bottomLeft.x;
}

- (CGPoint)topRight {
  return CGPointMake(self.right, self.top);
}

- (void)setTopRight:(CGPoint)topRight {
  self.top = topRight.y;
  self.right = topRight.x;
}

- (CGPoint)bottomRight {
  return CGPointMake(self.right, self.bottom);
}

- (void)setBottomRight:(CGPoint)bottomRight {
  self.bottom = bottomRight.y;
  self.right = bottomRight.x;
}

#pragma mark -

- (void)ceilFrame {
  self.frame = CGRectMake(ceil(self.frame.origin.x), ceil(self.frame.origin.y), ceil(self.frame.size.width), ceil(self.frame.size.height));
}

- (UIImage *)getImage {
  UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
  [self.layer renderInContext:UIGraphicsGetCurrentContext()];

  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();

  UIGraphicsEndImageContext();

  return img;
}

- (UIViewController *)viewController {
  Class vcc = [UIViewController class];

  UIResponder *responder = self;

  while (responder) {
    responder = [responder nextResponder];
    if ([responder isKindOfClass:vcc])
      return (UIViewController *)responder;
  }

  return nil;
}

- (void)placeAboveView:(UIView *)fixedView withPadding:(CGFloat)padding {
  CGRect addViewFrame = self.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) - padding;
  self.frame = addViewFrame;
}

- (void)placeBelowView:(UIView *)fixedView withPadding:(CGFloat)padding {
  CGRect addViewFrame = self.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) + padding;
  self.frame = addViewFrame;
}

- (void)placeBelowViewAndSizeToFit:(UIView *)fixedView withPadding:(CGFloat)padding {
  if ([fixedView isKindOfClass:[UILabel class]]) {
    [(UILabel *)fixedView sizeToFit];
  }

  if ([self isKindOfClass:[UILabel class]]) {
    [(UILabel *)self sizeToFit];
  }

  CGRect addViewFrame = self.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) + padding;
  self.frame = addViewFrame;
}

- (void)placeCenterAndBelowView:(UIView *)fixedView withPadding:(CGFloat)padding {
  self.center = fixedView.center;

  CGRect addViewFrame = self.frame;
  addViewFrame.origin.y = CGRectGetMaxY(fixedView.frame) + padding;
  self.frame = addViewFrame;
}

- (void)placeRightFromView:(UIView *)fixedView withPadding:(CGFloat)padding {
  CGRect addViewFrame = self.frame;
  addViewFrame.origin.x = CGRectGetMaxX(fixedView.frame) + padding;
  self.frame = addViewFrame;
}

- (void)placeCenterAndRightFromView:(UIView *)fixedView withPadding:(CGFloat)padding {
  self.center = fixedView.center;

  CGRect addViewFrame = self.frame;
  addViewFrame.origin.x = CGRectGetMaxX(fixedView.frame) + padding;
  self.frame = addViewFrame;
}

- (void)setWidth:(CGFloat)width andHeight:(CGFloat)height {
  CGRect frame = self.frame;
  frame.size = CGSizeMake(width, height);
  self.frame = frame;
}

- (void)setX:(CGFloat)x andY:(CGFloat)y {
  CGRect frame = self.frame;
  frame.origin = CGPointMake(x, y);
  self.frame = frame;
}

#pragma mark -

+ (UIView *)getViewWithClass:(Class)viewClass fromNibNamed:(NSString *)nibName {
  NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];

  for (UIView *view in topLevelObjects) {
    if ([view isKindOfClass:viewClass]) {
      return view;
    }
  }

  return nil;
}

@end
