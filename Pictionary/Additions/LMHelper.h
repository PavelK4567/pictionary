//
//  LMHelper.h
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LMHelper : NSObject

+ (UIImage*)loadImageForImageName:(NSString *)imageName;
+ (NSString *)saveImage:(UIImage *)image;
+ (void)removeImagesFromDocumentsFolder;

@end
