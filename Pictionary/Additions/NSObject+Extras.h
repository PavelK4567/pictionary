//  NSObject+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


typedef void (^VoidBlock)();
typedef void (^DismissBlock)(int buttonIndex);
typedef void (^CancelBlock)();
typedef void (^PhotoPickedBlock)(UIImage *chosenImage);


@interface NSObject (Extras)

+ (id)getFromNibNamed:(NSString *)nibName;
+ (id)getFromNib;

- (void)performBlock:(VoidBlock)block;
- (void)performBlock:(VoidBlock)block afterDelay:(NSTimeInterval)delay;

@end
