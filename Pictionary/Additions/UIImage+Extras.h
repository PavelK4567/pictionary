//  UIImage+Extras.h
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface UIImage (Extras)

+ (UIImage *)stretchedImage:(UIImage *)image insets:(UIEdgeInsets)insets;
+ (UIImage *)buttonStretchedImage:(UIImage *)image insets:(UIEdgeInsets)insets;

+ (UIImage *)imageWithColor:(UIColor *)color;
- (UIImage *)imageWithFixedOrientation;
- (CGImageRef)cgimageWithFixedOrientation;

//- (UIImage *)resizedImageByWidth:(NSUInteger)width;
- (UIImage *)resizedImageByHeight:(NSUInteger)height;
- (UIImage *)resizedImageWithMinimumSize:(CGSize)size;
- (UIImage *)resizedImageWithMaximumSize:(CGSize)size;
- (UIImage *)drawImageInBounds:(CGRect)bounds;
- (UIImage *)croppedImageWithRect:(CGRect)rect;
- (UIImage *)cropAndResizeto:(CGSize)size;

@end
