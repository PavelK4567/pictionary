//
//  NSDate-Expanded.m
//  YES
//
//  Created by action item on 1/14/10.
//  Copyright 2010 RosTelecom. All rights reserved.
//

#import "NSDate-Expanded.h"

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]
#define NUMBER_OF_SECONDS_IN_A_DAY 86400

@implementation NSDate (Expanded)

static NSDateFormatter *gDateFormatter = NULL;





- (BOOL)isBefore:(NSDate*)other
{
	BOOL retVal = [self timeIntervalSinceDate:other] < 0;
	return retVal;
}

- (BOOL)isAfter:(NSDate*)other
{
	return [self timeIntervalSinceDate:other] > 0;
}

- (BOOL)isSameDay:(NSDate*)other
{
    if (other == nil) return NO; // or it will crash

    NSCalendar* calendar = [NSCalendar currentCalendar];

    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:other];

    return [comp1 day] == [comp2 day] &&
	[comp1 month] == [comp2 month] &&
	[comp1 year]  == [comp2 year];
}

- (int)daysFrom1970
{
	NSTimeInterval since = [self timeIntervalSince1970];
	return since/(60*60*24);
}

- (int)daysFromDate:(NSDate *)date
{
	NSTimeInterval since = [self timeIntervalSinceDate:date];
	return since/(60*60*24);
}

- (BOOL)is7DaysFromNow
{
	NSDate *seven = [[NSDate date] dateByAddingTimeInterval:86400*7];
	return [seven isSameDay:self];
}

- (BOOL)isTomorrow
{
	NSDate *tomorrow = [[NSDate date] dateByAddingTimeInterval:86400];
	return [tomorrow isSameDay:self];
}

- (BOOL)isYesterday
{
    NSDate *yesterday = [[NSDate date] dateByAddingTimeInterval:-86400];
	return [yesterday isSameDay:self];
}

- (NSDate *)dateAtEndOfDay
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	[components setHour:23];
	[components setMinute:59];
	[components setSecond:59];
	return [CURRENT_CALENDAR dateFromComponents:components];
}


@end
