//  LMAppDelegate.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMAppDelegate.h"
#import "LMHomeViewController.h"
#import "LMLoginViewController.h"
#import "LMItemManager.h"
#import "LMFirstViewController.h"

@interface LMAppDelegate ()

- (void)configureReachability;
- (void)configureNotifications;

@end


@implementation LMAppDelegate

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  /*
   // clean save data
   OSStatus errorCode = noErr;

   static NSString *const UIApplication_UIID_Key = @"uniqueInstallationIdentifier";
   static NSString *const UIApplication_UserUIID_Key = @"uniqueUserIdentifier";

   NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)kSecClassGenericPassword, (__bridge id)kSecClass, UIApplication_UIID_Key,
   (__bridge id)kSecAttrGeneric, UIApplication_UIID_Key, (__bridge id)kSecAttrAccount,
   [[NSBundle mainBundle] bundleIdentifier], (__bridge id)kSecAttrService, nil];
   errorCode = SecItemDelete((__bridge CFDictionaryRef)query);

   NSMutableDictionary *queryDelete = [NSMutableDictionary dictionary];
   [queryDelete setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
   [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrGeneric];
   [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrAccount];
   [queryDelete setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:(__bridge id)kSecAttrService];

   errorCode = SecItemDelete((__bridge CFDictionaryRef)queryDelete);

   [SDCloudUserDefaults removeObjectForKey:@"uid"];
   [SDCloudUserDefaults removeObjectForKey:@"uuid"];
   [SDCloudUserDefaults removeObjectForKey:UIApplication_UIID_Key];
   [SDCloudUserDefaults removeObjectForKey:UIApplication_UserUIID_Key];
   [SDCloudUserDefaults removeObjectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
   [SDCloudUserDefaults removeObjectForKey:kMonthlyPurchaseId];
   [SDCloudUserDefaults synchronize];
   return YES;
   // end clean save data
   /*
   */

  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

  NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:kMonthlyPurchaseId];
  NSLog(@"cloud_uuid %@", cloud_uuid);
  NSLog(@" objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  NSLog(@"kMonthlyPurchaseId %@ ", [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]]);

  // ASIHTTP Policies
  [ASIHTTPRequest setDefaultCache:[ASIDownloadCache sharedCache]];
  [ASIHTTPRequest setDefaultTimeOutSeconds:60];
  // Flurry
  [Flurry startSession:kFlurryKey];
  [Flurry setEventLoggingEnabled:YES];
  //[Flurry setDebugLogEnabled:YES];
  [Flurry setBackgroundSessionEnabled:NO];
  //[Flurry logEvent:kOnGetLoginToken];
  //[Flurry logEvent:@"Article_Read"];
  [Flurry setCrashReportingEnabled:YES];
  //[Flurry startSession:kFlurryKey];
  [Flurry setAppVersion:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
  [Flurry logEvent:@"app_started"];

  // Initialization
  [self configureAplicationSettings];
  [self configureReachability];
  [self configureRootController];
  //[self buildMainStack];
  [self configureNotifications];


  // System
  self.window.backgroundColor = [APPSTYLE otherViewColorForKey:@"View_Background"];
  [self.window makeKeyAndVisible];
  return YES;
}
- (void)chekCloudData
{
  NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:kMonthlyPurchaseId];
  BOOL firsTimeCloudChek = [[NSUserDefaults standardUserDefaults] boolForKey:@"firsTimeCloudChek"];
  NSLog(@"chekCloudData>>> cloud_uuid %@", cloud_uuid);
  NSLog(@"chekCloudData>>> objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  NSLog(@"chekCloudData>>>kMonthlyPurchaseId %@ ", [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]]);
}
- (void)removeASIHTTPRequest
{
  for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations) {
    [req cancel];
    [req setDelegate:nil];
  }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  [ITEM_MANAGER synchronize];
  [Flurry logEvent:@"app_go_to_background"];
  NSLog(@"applicationDidEnterBackground");
  //[[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidEnterBackground object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  NSLog(@"applicationWillEnterForeground");
  //[[NSNotificationCenter defaultCenter] postNotificationName:kApplicationWillEnterForeground object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  NSLog(@"applicationDidBecomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  NSLog(@"applicationWillTerminate");
  [Flurry logEvent:@"app_terminate"];
}

#pragma mark - Imlpementation
- (void)configureAplicationSettings
{
  [LMItemManager sharedInstance];
  [AUDIO_MANAGER setEnabled:[[NSUserDefaults standardUserDefaults] boolForKey:@"SoundEffectsEnabled"]];
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  if (![[[defaults dictionaryRepresentation] allKeys] containsObject:@"SoundEffectsEnabled"]) {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SoundEffectsEnabled"];
  }
}

- (void)configureRootController
{
  if ([USER_MANAGER processAutologin]) {
    [self buildMainStack];
  } else {
    [self buildLoginStack];
  }
}

- (void)buildLoginStack
{
  LMLoginViewController *loginController = [LMLoginViewController new];
  self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginController];
  self.baseController = loginController;
  self.window.rootViewController = self.navigationController;
}

- (void)buildMainStack
{
  // LMHomeViewController *homeController = [LMHomeViewController new];
  LMFirstViewController *firstController = [LMFirstViewController new];

  self.navigationController = [[UINavigationController alloc] initWithRootViewController:firstController];
  self.baseController = firstController;
  self.window.rootViewController = self.navigationController;
}

#pragma mark - Private

- (void)configureReachability
{
  [ReachabilityHelper setupReachability];
}


- (void)configureNotifications
{
}

@end
