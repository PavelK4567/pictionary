//  AppConstants.h
//  Created by Dimitar Tasev in May 2014.
//  Copyright (c) 2012 Dimitar Tasev. All rights reserved.

//#ifdef __OBJC__

/* General */
#define kSuffixSelected @"_Selected"
#define kSuffixDisabled @"_Disabled"
#define SERVER_DATETIME_FORMAT @"yyyy/MM/dd" /*2014\/04\/29*/
#define STAGING 1

/* Flurry */
#define kFlurryKey @"J4WNQQSRDHSZGG82KQSR"
#define kOnGetLoginToken @"onGetLoginToken"
#define kOnAppleSuccessfulSubscribe @"onAppleSuccessfulSubscribe"
#define kOnAppleUnsuccessfulSubscribe @"onAppleUnsuccessfulSubscribe"
#define kOnAppleSubscriptionTerms @"onAppleSubscriptionTerms"

/* Singletons */

#define AUDIO_SINGLETON 1
#define COUNTRY_SINGLETON 1
#define ITEM_SINGLETON 1
#define REQUEST_SINGLETON 1
#define USER_SINGLETON 1

/* Paths */

#define kCountriesPath @"Countries"
#define kDemoPath @"demo"

#define kPreferencesFolderPictionaryItems @"Items"
#define kPreferencesFileMapping @"mapping"
#define kPreferencesFileItems @"items"
#define kPreferencesFileTags @"tags"
#define kPreferencesFileUrls @"urls"
#define kPreferencesFileTypePlist @"plist"
#define kPreferencesFilePng @"png"
#define kPreferencesFileJpg @".jpg"


/* Parser */

#define kParserTagUUID @"uuid"
#define kParserItems @"items"
#define kParserTagImageThumb @"thumb"
#define kParserTagImage @"image"
#define kParserTagTextNative @"native"
#define kParserTagTextLocalized @"localized"
#define kParserTagTextTranslate @"translate"
#define kParserTagUserGenerated @"user"
#define kParserTagCanShare @"share"
#define kParserTagMSISDN @"msisdn"
#define kParserTagDefaultImage @"default"
#define kParserTagDateTaken @"date"
#define kParserTagMSISDN @"msisdn"
#define kParserTagImageOrientation @"msisdn"

#define kParserTagImage @"image"
#define kParserTagThumbnail @"thumbnail"
#define kParserTagOriginalImage @"originalImage"


/* Constants */

#define kCamfindSearchesForGuest 5
#define kCamfindSearchesForUser 25
#define kCamfindSearchesForOther 0

#define kUserStatusGuestString @"Guest"
#define kUserStatusNotRegistered @"Not Registered"
#define kUserStatusActiveString @"Active"
#define kUserStatusToBeDeactivatedString @"Pending Cancellation"
#define kUserStatusDeactivatedString @"Cancelled"
#define kUserStatusDisabledString @"Disabled"


/* Preferences */

#define kUserMSISDN @"msisdn"
#define kUserCode @"code"
#define kUserDeviceId @"device_id"
#define kSearchWords @"words"


#define kUserManagerInitialized @"user_manager_initialized"
#define kUserManagerDefault @"000000000"
#define kLastValidMSISDNPath @"last_msisdn"
#define kLastValidLoginUser @"last_valid_login_user"
#define kLastValidMSISDN @"last_valid_msisdn"
#define kLastValidUniqueID @"last_valid_unique_ID"
#define kLastValidUserStatus @"last_valid_user_status"
#define kLastValidBillingDate @"last_valid_user_billing_date"


#define kUserPath @"last_user"
#define kGuestPath @"last_guest"
#define kUserTags @"last_tags"
#define kGuestTags @"guest_tags"

/* UI Constants */

#define __SELECTED__ @"_Selected"

/* Sizes */

#define kCornerRadius 2.
#define kImageNormalSize ((CGSize){ 640, 640 })
#define kImageThumbSize ((CGSize){ 200, 200 })

#define kHomeExpandedInset ((UIEdgeInsets){ 0, 0, 0, 0 })
#define kHomeCollapesedInset ((UIEdgeInsets){ -210, 0, 0, 0 })
#define kHomePhotoCellSize ((CGSize){ 310, 200 })
#define kHomePhoto480CellSize ((CGSize){ 310, 140 })
#define kHomeSearchCellSize ((CGSize){ 310, 52 })
#define kHomeSearchActiveStateCellSize ((CGSize){ 310, 72 })
#define kHomeSuggestionCellSize ((CGSize){ 310, 32 })
#define kHomeResultCellSize ((CGSize){ 310, 100 })
#define kHomeItemCellSize ((CGSize){ 100, 100 })
#define kHeaderSize ((CGSize){ 310, 1 })

#define kNavbarButtonSize ((CGSize){ 32, 32 })
#define kShortButtonSize ((CGSize){ 50, 32 })
#define kLongButtonSize ((CGSize){ 280, 32 })

/* Sounds */

#define kSoundTypeBackground @"SFX_Background"
#define kSoundTypeButtonTap @"SFX_ButtonTap"
#define kSoundTypeSwipe @"SFX_Swipe"
#define kSoundTypeError @"SFX_Error"
#define kSoundTypeSuccess @"SFX_Success"

#define kSoundTypeResults @"GotResultsFX"
#define kSoundTypeDeletePicture @"PicDeletedFX"
#define kSoundTypeTakePicture @"TakePicFX"

#define kSoundFileType @"mp3"

/* URL */
#define kSubscriptionString @"false"
//#define kApiBaseUrl @"http://la-mark-il.com:8081/LMWeb/pictionary"
//#define kApiBaseBillingUrl @"http://la-mark-il.com:8081/LMWeb/Kantoo1000NativeApp"
//#define kApiBaseUrl @"http://kantoo.com/LMWeb/pictionary"
//#define kApiBaseBillingUrl @"http://kantoo.com/LMWeb/Kantoo1000NativeApp"
#define kApiPriceCode @"com.kfoto.weekly" //@"com.completo.montly"

//#define kApiCamFindUrl @"http://54.226.173.227:8080/PitionaryProxy/DoCamFindRequest"
//#define kApiCamFindUrl @"http://192.168.9.168:8080/PitionaryProxy/DoCamFindRequest"
//#define kApiGoogleResultsUrl @"http://54.226.173.227:8080/PitionaryProxy/DoImageSearchRequest"

#if STAGING == 0
#define kApiBaseUrl @"http://la-mark-il.com:8081/LMWeb/pictionary"
#define kApiBaseBillingUrl @"http://la-mark-il.com:8081/LMWeb/Kantoo1000NativeApp"

#define kApiCamFindUrl @"http://pictionary-13.la-mark-il.com:8080/PitionaryProxy/DoCamFindRequest"
#define kApiGoogleResultsUrl @"http://pictionary-13.la-mark-il.com:8080/PitionaryProxy/DoImageSearchRequest"
#endif

#if STAGING == 1
#define kApiBaseUrl @"http://kantoo.com/LMWeb/pictionary"
#define kApiBaseBillingUrl @"http://kantoo.com/LMWeb/Kantoo1000NativeApp"
//////
#define kApiCamFindUrl @"http://pi-1.kantoo.com:8001/PitionaryProxy/DoCamFindRequest"
#define kApiGoogleResultsUrl @"http://pi-1.kantoo.com:8001/PitionaryProxy/DoImageSearchRequest"
#endif

/* Purchases */

#define kMonthlyPurchaseId @"com.lamark.kfoto.weekly"
#define kMonthlyPurchaseIdShort @"com.lamark.kfoto.weekly"


/* API */
#define kApiStatus @"status"
#define kApiUserStatus @"userStatus"
#define kApiNextBillingDate @"validUntil"

#define kApiToken @"token"
#define kApiUserUniqueID @"userUniqueID"

#define kApiUserNumberOfCredits @"numberOfCredits"
#define kApiUserNumberOfCreditsGoogle @"numberOfCreditsGoogle"


#define kUserProductPurchased @"com.razeware.inapprage.updogsadness"
/*  Photos */
#define kPathForCameraPhoto @"path"

/*  Background Notification */
#define kApplicationDidEnterBackground @"applicationDidEnterBackgroundNotification"
#define kApplicationWillEnterForeground @"applicationWillEnterForegroundNotification"

/* UI constants */
#define kButtomLinePadding 2.0

/* First View Controller */
#define kViewsPadding 5.0
//#endif
