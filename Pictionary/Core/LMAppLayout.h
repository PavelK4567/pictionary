//  AppLayout.h
//  Created by Dimitar Tasev on 20140219.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface AppLayout : NSObject

+ (void)place:(UIView *)view1 below:(UIView *)view2 padding:(CGFloat)padding;
+ (void)place:(UIView *)view1 above:(UIView *)view2 padding:(CGFloat)padding;
+ (void)place:(UIView *)view1 rightOf:(UIView *)view2 padding:(CGFloat)padding;

+ (void)placeLabel:(UILabel *)label below:(UIView *)view padding:(CGFloat)padding;
+ (void)placeLabel:(UILabel *)label rightOf:(UIView *)view padding:(CGFloat)padding;
+ (void)resize:(UIView *)parent toFit:(UIView *)view padding:(CGFloat)padding;

+ (void)resize:(UILabel *)label;
+ (void)resize:(UILabel *)label maxSize:(CGSize)size;

@end
