//  LMAppDelegate.h
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@class LMBaseViewController;


@interface LMAppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) UINavigationController *navigationController;
@property(weak, nonatomic) LMBaseViewController *baseController;

- (void)configureRootController;
- (void)buildLoginStack;
- (void)buildMainStack;

@end


#define APP_DELEGATE ((LMAppDelegate *)[[UIApplication sharedApplication] delegate])
