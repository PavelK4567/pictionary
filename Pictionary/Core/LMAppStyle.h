//  AppStyle.h


/* Font */
#define FONT_KEY @"font"
#define FONT_SIZE_KEY @"font_size"

/* Text Color */
#define FONT_COLOR_RED_KEY @"font_color_red"
#define FONT_COLOR_BLUE_KEY @"font_color_blue"
#define FONT_COLOR_GREEN_KEY @"font_color_green"
#define FONT_OPACITY_KEY @"font_color_opacity"

/* Text Shadow Color */
#define SHADOW_COLOR_RED_KEY @"shadow_color_red"
#define SHADOW_COLOR_BLUE_KEY @"shadow_color_blue"
#define SHADOW_COLOR_GREEN_KEY @"shadow_color_green"
#define SHADOW_OPACITY_KEY @"shadow_color_opacity"
#define SHADOW_OFFSET_WIDTH_KEY @"shadow_offset_width"
#define SHADOW_OFFSET_HEIGHT_KEY @"shadow_offset_height"

/* Button */
#define BUTTON_BG @"style_bg"
#define BUTTON_SELECTED_SUFFIX @"_Sel"

/* Button Title Inset */
#define BUTTON_TITLE_INSET_TOP_KEY @"title_inset_top"
#define BUTTON_TITLE_INSET_BOTTOM_KEY @"title_inset_bottom"
#define BUTTON_TITLE_INSET_LEFT_KEY @"title_inset_left"
#define BUTTON_TITLE_INSET_RIGHT_KEY @"title_inset_right"

/* Button Image Inset */
#define BUTTON_IMAGE_INSET_TOP_KEY @"image_inset_top"
#define BUTTON_IMAGE_INSET_BOTTOM_KEY @"image_inset_bottom"
#define BUTTON_IMAGE_INSET_LEFT_KEY @"image_inset_left"
#define BUTTON_IMAGE_INSET_RIGHT_KEY @"image_inset_right"

/* Button Content Inset */
#define BUTTON_CONTENT_INSET_TOP_KEY @"content_inset_top"
#define BUTTON_CONTENT_INSET_BOTTOM_KEY @"content_inset_bottom"
#define BUTTON_CONTENT_INSET_LEFT_KEY @"content_inset_left"
#define BUTTON_CONTENT_INSET_RIGHT_KEY @"content_inset_right"


#import <UIKit/UIKit.h>


@interface AppStyle : NSObject

@property(nonatomic, strong) NSMutableDictionary *buttonsDictionary;
@property(nonatomic, strong) NSMutableDictionary *fontsDictionary;
@property(nonatomic, strong) NSMutableDictionary *colorsDictionary;

- (NSMutableDictionary *)buttonStyleDictionaryForType:(NSString *)buttonType;
- (NSMutableDictionary *)fontStyleDictionaryForType:(NSString *)textType;

- (UIFont *)fontForType:(NSString *)textType;
- (UIColor *)colorForType:(NSString *)textType;
- (UIColor *)shadowColorForType:(NSString *)textType;
- (CGSize)shadowOffsetForType:(NSString *)textType;

- (void)applyBorderColor:(UIColor *)color toButton:(UIButton *)button;

- (void)applyStyle:(NSString *)style toLabel:(UILabel *)label;
- (void)applyStyle:(NSString *)style toButton:(UIButton *)button;
- (void)applyStyle:(NSString *)style toTextView:(UITextView *)textView;
- (void)applyStyle:(NSString *)style toTextField:(UITextField *)textField;
- (void)applyBackground:(NSString *)background toButton:(UIButton *)button;
- (void)applyIcon:(NSString *)icon toButton:(UIButton *)button;
- (void)applyTitle:(NSString *)title toButton:(UIButton *)button;
- (UIColor *)otherViewColorForKey:(NSString *)key;
- (NSString *)htmlStyleForKey:(NSString *)key fontSizePlaceholder:(NSString *)fontSizePlaceholder;
+ (AppStyle *)sharedInstance;
@end

#define APPSTYLE ((AppStyle *)[AppStyle sharedInstance])
