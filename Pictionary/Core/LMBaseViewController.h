//  LMBaseViewController.h
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMBaseViewController : UIViewController

+ (id)new;
+ (id)newFromXibNamed:(NSString *)xib;

- (void)configureAppearance;
- (void)configureUI;
- (void)configureObservers;
- (void)configureNavigation;
- (void)configureData;
- (void)loadData;
- (void)layout;
- (void)dismissObservers;


- (void)addActivityIndicator;
- (void)processingPhoto;
- (void)removeActivityIndicator;

- (void)makeDoneBtn;
- (void)makeSearchBackBtn;
- (void)removeSearchBackBtn;
- (void)makeSearchBackBtnForChekPhoto;

@end
