//  LMBaseViewController.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMBaseViewController.h"


@interface LMBaseViewController ()

@property(strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) UIView *activityIndicatorBacground;

@end


@implementation LMBaseViewController

+ (id) new
{
  return [[self class] newFromXibNamed:NSStringFromClass([self class])];
}

+ (id)newFromXibNamed:(NSString *)xib
{
  return [(LMBaseViewController *)[[self class] alloc] initWithNibName:xib bundle:0];
}


- (id)initWithNibName:(NSString *)xib bundle:(NSBundle *)bundle
{
  BOOL use568H = NO;
  if (IS_WIDESCREEN) {
    NSString *path = [[NSBundle mainBundle] pathForResource:[xib stringByAppendingString:@"-568h"] ofType:@"nib"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
      use568H = YES;
    }
  }
  self = [super initWithNibName:use568H ? [xib stringByAppendingString:@"-568h"] : xib bundle:bundle];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self configureUI];
  [self configureData];
  [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self configureAppearance];
  [self configureNavigation];
  [self configureObservers];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self loadData];
  [self layout];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self dismissObservers];
}

- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
}

#pragma mark - Implementation

- (void)configureAppearance
{
  UIImage *bgImage = [UIImage imageNamed:@"bg.png"];
  self.view.backgroundColor = [UIColor colorWithPatternImage:bgImage];
  if ([UIScreen mainScreen].bounds.size.height == 568.0) {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-568h"]];
  }
}

- (void)configureUI
{
}

- (void)configureObservers
{
}

- (void)configureNavigation
{
  self.navigationItem.title = Localized(@"T069");
  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") || SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar-iOS7"]
                                                 forBarPosition:UIBarPositionTopAttached
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.tintColor = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"]; //[UIColor whiteColor];
  } else {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar-iOS6"] forBarMetrics:UIBarMetricsDefault];
  }

  if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  }
  self.navigationController.navigationBar.titleTextAttributes = @{
    UITextAttributeFont : [UIFont fontWithName:@"Noteworthy-Bold" size:22.0],
    UITextAttributeTextColor : [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"],
    UITextAttributeTextShadowColor : [UIColor colorWithWhite:0.000 alpha:0.250],
    UITextAttributeTextShadowOffset : [NSValue valueWithCGSize:CGSizeMake(0, 0)]
  };
  /*if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ) {
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeFont : [UIFont fontWithName:@"Noteworthy-Bold" size:22.0],
                                                           UITextAttributeTextColor : [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"],
                                                           UITextAttributeTextShadowColor : [UIColor colorWithWhite:0.000 alpha:0.250],
                                                           UITextAttributeTextShadowOffset : [NSValue valueWithCGSize:CGSizeMake(0, 0)]
                                                           }];
  }
  else {
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeFont : [UIFont fontWithName:@"Noteworthy-Bold" size:17.0],
                                                           UITextAttributeTextColor : [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"],
                                                           UITextAttributeTextShadowColor : [UIColor colorWithWhite:0.000 alpha:0.250],
                                                           UITextAttributeTextShadowOffset : [NSValue valueWithCGSize:CGSizeMake(0, 0)]
                                                           }];
  }*/
  self.navigationController.navigationBar.topItem.title = Localized(@"T069");
  [self.navigationController setNavigationBarHidden:NO animated:YES];
  [self makeSettingBtn];
  [UIBarButtonItem.appearance setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
}

- (void)makeSettingBtn
{
  if ([self isKindOfClass:NSClassFromString(@"LMFirstViewController")]) {
    UIBarButtonItem *settingsButton =
      [[UIBarButtonItem alloc] initWithTitle:@"\u2699" style:UIBarButtonItemStylePlain target:self action:@selector(showSettings)];

    UIFont *customFont = [UIFont fontWithName:@"Helvetica" size:24.0];
    NSDictionary *fontDictionary = @{ UITextAttributeFont : customFont };
    [settingsButton setTitleTextAttributes:fontDictionary forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = settingsButton;
  } else if ([self isKindOfClass:NSClassFromString(@"LMPictureProcessingViewController")]) {
    [self makeSearchBackBtnForChekPhoto];
  }
}

- (void)makeDoneBtn
{
  UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
  doneButton.style = UIBarButtonItemStyleBordered;
  self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)makeSearchBackBtn
{
  UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(searchBackAction)];
  // [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(searchBackAction)];
  doneButton.style = UIBarButtonItemStyleBordered;
  self.navigationItem.leftBarButtonItem = doneButton;
}
- (void)makeSearchBackBtnForChekPhoto
{
  UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(chekBackAction)];
  doneButton.style = UIBarButtonItemStyleBordered;
  self.navigationItem.leftBarButtonItem = doneButton;
}
- (void)removeSearchBackBtn
{
  self.navigationItem.leftBarButtonItem = nil;
}

- (void)configureData
{
}

- (void)loadData
{
}

- (void)layout
{
}

- (void)dismissObservers
{
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

#pragma mark - done Action
- (void)doneAction
{
}

#pragma mark - show Settings
- (void)showSettings
{
}

#pragma mark - search Back Action
- (void)searchBackAction
{
}
- (void)chekBackAction
{
  NSLog(@"chekBackAction");
  if (!self.activityIndicator) {
    [self.navigationController popViewControllerAnimated:YES];
  }
}

#pragma mark - ActivityIndicator
- (void)addActivityIndicator
{
  if (!self.activityIndicator) {
    self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityIndicatorBacground.userInteractionEnabled = YES;
    self.activityIndicatorBacground.backgroundColor = [UIColor darkGrayColor];
    self.activityIndicatorBacground.alpha = 0.5;
    [self.view addSubview:self.activityIndicatorBacground];


    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.activityIndicator.color = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.activityIndicator startAnimating];
  }
}

- (void)processingPhoto
{
  [self addActivityIndicator];
  UILabel *titleLabel = [UILabel new];
  [titleLabel setFrame:(CGRect){ { 0, 252 }, { 320, 32 } }];
  [APPSTYLE applyStyle:@"Processing_Photo_Label" toLabel:titleLabel];
  [titleLabel setText:Localized(@"T093")];
  titleLabel.textAlignment = NSTextAlignmentCenter;
  //[self.activityIndicatorBacground addSubview:titleLabel];
  self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, 187);
  /*"T095" = "Processando a foto…\nRestam %d fotos até o dia %@";
   "T096" = "Processing photo…\n 1 photo remaining until %@";
   "T097" = "Processing photo…\nNo photos remaining.\nMore photos available on %@.";

   "T095.g" = "Processando a foto…\n %d  fotos restantes";
   "T096.g" = "Processing photo…\n 1 photo remaining";
   "T097.g" = "Processing photo…\nNo photos remaining.\nRegister now to get more photos every week.";*/

  UILabel *titleLabel2 = [UILabel new];
  [titleLabel2 setFrame:(CGRect){ { 0, 190 }, { 320, 162 } }];
  [APPSTYLE applyStyle:@"Processing_Photo_Label" toLabel:titleLabel2];
  titleLabel2.numberOfLines = 0;
  [titleLabel2 setText:Localized(@"T093")];
  titleLabel2.textAlignment = NSTextAlignmentCenter;
  [self.activityIndicatorBacground addSubview:titleLabel2];

  if ([USER_MANAGER isGuest]) {
    if ([USER_MANAGER numberOfCredits] == 1) {
      [titleLabel2
        setText:[NSString stringWithFormat:@"Processando a foto…\nNão há mais fotos restantes.\nInscreva-se agora para obter mais fotos toda semana."]];
    } else if ([USER_MANAGER numberOfCredits] == 2) {
      [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\n1 foto restante"]];
    } else {
      [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\n%ld fotos restantes", [USER_MANAGER numberOfCredits] - 1]];
    }
  } else {
    if ([USER_MANAGER numberOfCredits] == 1) {
      if ([USER_MANAGER isNative]) {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\n\n(Não restam mais fotos. Renove a inscrição para obter mais.)"]];
        //"T097.A" = "Processando a foto…\n\(Não restam mais fotos. Renove a inscrição para obter mais.)";
      } else {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\n Não restam mais fotos. Mais fotos estarão disponíveis no dia %@.",
                                                        [USER_MANAGER takeStringFromDate]]];
      }
    } else if ([USER_MANAGER numberOfCredits] == 2) {
      if ([USER_MANAGER isNative]) {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\nResta 1 foto até o dia %@", [USER_MANAGER takeNativeDateStringForSettings]]];
      } else {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\nResta 1 foto até o dia %@", [USER_MANAGER takeStringFromDate]]];
      }
    } else {
      if ([USER_MANAGER isNative]) {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\nRestam %ld fotos até o dia %@", [USER_MANAGER numberOfCredits] - 1,
                                                        [USER_MANAGER takeNativeDateStringForSettings]]];
      } else {
        [titleLabel2 setText:[NSString stringWithFormat:@"Processando a foto…\nRestam %ld fotos até o dia %@", [USER_MANAGER numberOfCredits] - 1,
                                                        [USER_MANAGER takeStringFromDate]]];
      }
    }
  }
}

- (void)removeActivityIndicator
{
  if (self.activityIndicator) {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    [self.activityIndicatorBacground removeFromSuperview];
    self.activityIndicator = nil;
    self.activityIndicatorBacground = nil;
  }
}

@end
