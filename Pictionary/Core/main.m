//  main.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMAppDelegate.h"


int main(int argc, char *argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([LMAppDelegate class]));
  }
}
