//  LMTag.h
//  Created by Dimitar Tasev on 14.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMTag : NSObject <NSCopying>

+ (id)newWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionary;

@property(strong, nonatomic) NSString *uuid;
@property(strong, nonatomic) NSString *native;
@property(strong, nonatomic) NSString *localized;
@property(strong, nonatomic) NSMutableArray *items;

@end
