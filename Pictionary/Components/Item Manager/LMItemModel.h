//  LMItemModel.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMItemModel : NSObject

@property(strong, nonatomic) NSString *native;
@property(strong, nonatomic) NSString *localized;
@property(strong, nonatomic) NSString *imageThumb;
@property(strong, nonatomic) NSString *image;

@end
