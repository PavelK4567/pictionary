//  LMItemModel.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMItem : NSObject <NSCopying>

+ (id)newWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionary;
- (void)setSummOfStrings;


@property(strong, nonatomic) NSString *uuid;
@property(strong, nonatomic) NSString *imageThumb;
@property(strong, nonatomic) NSString *image;
@property(assign, nonatomic) BOOL isLandscapeImage;
@property(strong, nonatomic) NSString *msisdn;
@property(strong, nonatomic, readonly) NSString *nativeString;
@property(strong, nonatomic, readonly) NSString *localizedString;
@property(strong, nonatomic, readonly) NSString *localizedStringReverse;
@property(strong, nonatomic) NSMutableArray *items;
@property(assign, nonatomic) BOOL userGenerated;
@property(assign, nonatomic) BOOL canShare;
@property(assign, nonatomic) BOOL defaultImage;
@property(nonatomic) double dateTaken;
@end
