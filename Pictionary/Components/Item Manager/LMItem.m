//  LMItemModel.m
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMItem.h"
#import "LMTag.h"

@implementation LMItem

+ (id)newWithDictionary:(NSDictionary *)dict {
	LMItem *item = [LMItem new];
	if (item) {
		item.uuid = dict[kParserTagUUID];
		if (item.uuid) {
			item.imageThumb = dict[kParserTagImageThumb];
            item.msisdn = dict[kParserTagMSISDN] ? dict[kParserTagMSISDN] : @"";
			item.image = dict[kParserTagImage];
			item.items = [dict[kParserItems] mutableCopy];
			item.userGenerated = [dict[kParserTagUserGenerated] boolValue];
			item.canShare = [dict[kParserTagCanShare] boolValue];
			item.defaultImage = [dict[kParserTagDefaultImage] boolValue];
            item.dateTaken = [dict[kParserTagDateTaken] doubleValue];
            item.isLandscapeImage = [dict[kParserTagImageOrientation] boolValue];
			return item;
		}
	}
	return 0;
}

- (id)copyWithZone:(NSZone *)zone {
	LMItem *copy = [LMItem new];
	if (copy) {
		[copy setUuid:self.uuid];
		[copy setImageThumb:self.imageThumb];
		[copy setImage:self.image];
        [copy setMsisdn:self.msisdn ? self.msisdn : @""];
		[copy setUserGenerated:self.userGenerated];
		[copy setCanShare:self.canShare];
		[copy setDefaultImage:self.defaultImage];
        [copy setCanShare:self.canShare];
        [copy setDateTaken:self.dateTaken];
        [copy setIsLandscapeImage:self.isLandscapeImage];
	}
	return copy;
}

- (NSDictionary *)dictionary {
	return @{
           kParserTagUUID : self.uuid,
           kParserTagImage : self.image,
           kParserTagMSISDN : self.msisdn ? self.msisdn : @"",
           kParserTagImageThumb : self.imageThumb,
           kParserItems : self.items,
           kParserTagUserGenerated : [NSNumber numberWithBool:self.userGenerated],
           kParserTagCanShare : [NSNumber numberWithBool:self.canShare],
           kParserTagDateTaken : [NSNumber numberWithDouble:self.dateTaken],
           kParserTagDefaultImage : [NSNumber numberWithBool:self.defaultImage],
           kParserTagImageOrientation : [NSNumber numberWithBool:self.isLandscapeImage]
           };
}

- (void)setSummOfStrings {
}

- (NSString *)nativeString {
	NSString *nativeTempString;
	for (NSString *tagUUID in self.items) {
		if (nativeTempString) {
			nativeTempString = [nativeTempString stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] native]];
		}
		else {
			nativeTempString = [[ITEM_MANAGER tagWithId:tagUUID] native];
		}
	}
	return nativeTempString;
}

- (NSString *)localizedString {
	NSString *localizedTempString;
	for (NSString *tagUUID in self.items) {
		if (localizedTempString) {
			localizedTempString = [localizedTempString stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] localized]];
		}
		else {
			localizedTempString = [[ITEM_MANAGER tagWithId:tagUUID] localized];
		}
	}
	return localizedTempString;
}
- (NSString *)localizedStringReverse {
  NSString *localizedTempString;
  for (NSString *tagUUID in self.items) {
    if (localizedTempString) {
      //localizedTempString = [localizedTempString stringByAppendingFormat:@" %@", [[ITEM_MANAGER tagWithId:tagUUID] localized]];
      localizedTempString = [NSString stringWithFormat:@"%@ %@", [[ITEM_MANAGER tagWithId:tagUUID] localized],localizedTempString];
    }
    else {
      localizedTempString = [[ITEM_MANAGER tagWithId:tagUUID] localized];
    }
  }
  return localizedTempString;
}
@end
