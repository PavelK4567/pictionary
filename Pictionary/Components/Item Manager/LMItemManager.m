//  LMItemManager.m
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMItemManager.h"
#import "LMItem.h"
#import "LMTag.h"


@interface LMItemManager ()

@property (assign, nonatomic) BOOL needsSync;

@property (strong, nonatomic) NSString *destinationPath;
@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) NSMutableArray *items;

@property (strong, nonatomic) LMTag *lastTag;
@property (strong, nonatomic) NSMutableArray *lastTagitems;

@property (strong, nonatomic) NSString *lastKeyword;
@property (strong, nonatomic) NSArray *lastKeywordItems;

@end


@implementation LMItemManager


#ifdef ITEM_SINGLETON
SINGLETON_GCD(LMItemManager)
#endif

- (id)init {
	self = [super init];
	if (self) {
		[self load];
	}
	return self;
}

+ (void)initialize {
	[super initialize];
	[LMItemManager sharedInstance];
	[ITEM_MANAGER setDestinationPath:[[ITEM_MANAGER itemFolderPath] stringByAppendingPathComponent:kPreferencesFolderPictionaryItems]];
}

- (void)load {
	void (^loadStructure)() = ^() {
		NSArray *items = [NSArray arrayWithContentsOfFile:[self itemsPath]];
		self.items = [NSMutableArray array];
		for (NSDictionary *itemDict in items) {
			LMItem *item = [LMItem newWithDictionary:itemDict];
			if (item) {
				[self.items addObject:item];
			}
		}
    
		NSArray *tags = [NSArray arrayWithContentsOfFile:[self tagsPath]];
		self.tags = [NSMutableArray array];
		for (NSDictionary *tagDict in tags) {
			LMTag *tag = [LMTag newWithDictionary:tagDict];
			if (tag) {
				[self.tags addObject:tag];
			}
		}

		self.needsSync = NO;
	};
  
  
	void (^createStructure)(NSFileManager *fm) = ^void (NSFileManager *fm) {
		[fm createDirectoryAtPath:[self itemFolderPath] withIntermediateDirectories:YES attributes:0 error:0];
		NSString *rootPath = [[NSBundle mainBundle] pathForResource:kDemoPath ofType:@"bundle"];
    
		[fm copyItemAtPath:[rootPath stringByAppendingFormat:@"/%@/%@.%@", kPreferencesFileTypePlist, kPreferencesFileTags, kPreferencesFileTypePlist]
		            toPath:[self tagsPath]
		             error:0];
		[fm copyItemAtPath:[rootPath stringByAppendingFormat:@"/%@/%@.%@", kPreferencesFileTypePlist, kPreferencesFileItems, kPreferencesFileTypePlist]
		            toPath:[self itemsPath]
		             error:0];
		[fm copyItemAtPath:[rootPath stringByAppendingFormat:@"/%@/%@.%@", kPreferencesFileTypePlist, kPreferencesFileMapping, kPreferencesFileTypePlist]
		            toPath:[self mappingPath]
		             error:0];
		[fm       createDirectoryAtPath:[[self itemsPath] stringByAppendingPathComponent:kPreferencesFolderPictionaryItems]
		    withIntermediateDirectories:YES
		                     attributes:0
		                          error:0];
    
		loadStructure();
    
		self.destinationPath = [[self itemFolderPath] stringByAppendingPathComponent:kPreferencesFolderPictionaryItems];
		[fm createDirectoryAtPath:self.destinationPath withIntermediateDirectories:YES attributes:0 error:0];
    
		for (LMItem *item in self.items) {
			[fm copyItemAtPath:[rootPath stringByAppendingFormat:@"/%@@2x.%@", item.imageThumb, kPreferencesFilePng]
			            toPath:[self.destinationPath stringByAppendingFormat:@"/%@@2x.%@", item.imageThumb, kPreferencesFilePng]
			             error:0];
			[fm copyItemAtPath:[rootPath stringByAppendingFormat:@"/%@@2x.%@", item.image, kPreferencesFilePng]
			            toPath:[self.destinationPath stringByAppendingFormat:@"/%@@2x.%@", item.image, kPreferencesFilePng]
			             error:0];
		}
	};
  
  
	__block NSFileManager *fm = [NSFileManager defaultManager];
  
	BOOL isDirectory = NO;
	if ([fm fileExistsAtPath:[self itemFolderPath] isDirectory:&isDirectory]) {
		if (!isDirectory) {
			[fm removeItemAtPath:[self itemFolderPath] error:0];
			createStructure(fm);
		}
		else {
			loadStructure();
		}
	}
	else {
		createStructure(fm);
	}

}

- (NSString *)itemFolderPath {
	static NSString *path = 0;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{ path = [[NSString documentsDir] stringByAppendingPathComponent:kPreferencesFolderPictionaryItems]; });
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:path]];
	return path;
}

- (NSString *)mappingPath {
	static NSString *path = 0;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
    path = [[self itemFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", kPreferencesFileMapping, kPreferencesFileTypePlist]];
	});
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:path]];
	return path;
}

- (NSString *)tagsPath {
	static NSString *path = 0;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
    path = [[self itemFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", kPreferencesFileTags, kPreferencesFileTypePlist]];
	});
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:path]];
	return path;
}

- (NSString *)itemsPath {
	static NSString *path = 0;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
    path = [[self itemFolderPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", kPreferencesFileItems, kPreferencesFileTypePlist]];
	});
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:path]];
	return path;
}

- (void)synchronize {
	NSMutableArray *serializableTags = [NSMutableArray array];
	for (LMTag *tag in self.tags) {
		[serializableTags addObject:[tag dictionary]];
	}
	[serializableTags writeToFile:[self tagsPath] atomically:NO];
  
	NSMutableArray *serializableItems = [NSMutableArray array];
	for (LMItem *item in self.items) {
		[serializableItems addObject:[item dictionary]];
	}
	[serializableItems writeToFile:[self itemsPath] atomically:NO];
  
	self.needsSync = NO;
}

#pragma mark - Index

- (LMItem *)itemAtIndex:(NSInteger)index {
	return [self getSortedItems][index];
}

- (NSInteger)itemCount {
	return [[self getSortedItems] count];
}

- (NSString *)itemAtIndex:(NSInteger)index matching:(NSString *)tag {
	if (![self.lastTag.native isEqualToString:tag] && ![self.lastTag.localized isEqualToString:tag]) {
		[self processForTag:tag];
	}
	return self.lastTagitems[index];
}

- (NSInteger)itemCountMatching:(NSString *)tag {
	if (![self.lastTag.native isEqualToString:tag] && ![self.lastTag.localized isEqualToString:tag]) {
		[self processForTag:tag];
	}
	return [self.lastTagitems count];
}

#pragma mark - Tag

- (LMTag *)tagWithId:(NSString *)uuid {
	NSArray *filteredTags = [[self.tags filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"uuid MATCHES[cd] %@", uuid]] mutableCopy];
	return filteredTags ? filteredTags[0] : 0;
}

- (LMTag *)tagAtIndex:(NSInteger)index {
	return self.tags[index];
}

- (NSInteger)tagCount {
	return [self.tags count];
}

- (NSString *)tagAtIndex:(NSInteger)index matching:(NSString *)tag {
	if (![self.lastKeyword isEqualToString:tag]) {
		[self processForKeyword:tag];
	}
	return self.lastKeywordItems[index];
}

- (NSInteger)tagCountMatching:(NSString *)tag {
	if (![self.lastKeyword isEqualToString:tag]) {
		[self processForKeyword:tag];
	}
	return [self.lastKeywordItems count];
}

#pragma mark - Private

- (void)processForKeyword:(NSString *)keyword {
	self.lastKeyword = keyword;
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"native BEGINSWITH[cd] %@ OR localized BEGINSWITH[cd] %@", keyword, keyword];
	self.lastKeywordItems = [self.tags filteredArrayUsingPredicate:pred];
}

- (void)processForTag:(NSString *)tag {
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"native MATCHES[cd] %@ OR localized MATCHES[cd] %@", tag, tag];
	NSArray *filteredTags = [self.tags filteredArrayUsingPredicate:pred];
	if (filteredTags && [filteredTags count]) {
		self.lastTag = filteredTags[0];
		NSArray *lastTagItemUUIDs = self.lastTag.items;
		self.lastTagitems = [NSMutableArray array];
		for (NSString *uuid in lastTagItemUUIDs) {
			LMItem *item = [self.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"uuid MATCHES[cd] %@", uuid]][0];
			if (item) {
				[self.lastTagitems addObject:item];
			}
		}
		return;
	}
  
	self.lastTag = 0;
	self.lastTagitems = 0;
}

- (LMTag *)tagWithNative:(NSString *)native localized:(NSString *)localized {
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"native MATCHES[cd] %@ OR localized MATCHES[cd] %@", native, localized];
	NSArray *filteredTags = [self.tags filteredArrayUsingPredicate:pred];
	if (filteredTags && [filteredTags count]) {
		return filteredTags[0];
	}
	else {
		LMTag *tag = [LMTag new];
		tag.uuid = [NSString getUUID];
		tag.native = native;
		tag.localized = localized;
		tag.items = [NSMutableArray array];
		[self.tags addObject:tag];
		return tag;
	}
	return 0;
}

#pragma mark - Data Maintenance
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
  //[NSFileManager defaultManager]
  //assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
  
  NSError *error = nil;
  BOOL success = [URL setResourceValue:[NSNumber numberWithBool: YES]
                                forKey: NSURLIsExcludedFromBackupKey error: &error];
  if(!success){
    NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
  }
  
  return success;
}

- (LMItem *)makeItemWithImage:(UIImage *)sourceImage
                     withTags:(NSArray *)tags
                  isLandscapeImage:(BOOL)isLandscapeImageOrientation
{
	UIImage *image = [sourceImage resizedImageWithMaximumSize:kImageNormalSize];
	UIImage *imageThumb = [sourceImage cropAndResizeto:kImageThumbSize];
	LMItem *item = [LMItem new];
	item.uuid = [NSString getUUID];
	item.image = item.uuid;
	item.imageThumb = item.uuid;
    item.msisdn = [USER_MANAGER userMsisdn];
	item.items = [NSMutableArray array];
	item.userGenerated = YES;
	item.defaultImage = NO;
	item.canShare = YES;
    item.isLandscapeImage = isLandscapeImageOrientation;
    item.dateTaken = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000] doubleValue];
    
	NSString *thumbPath = [[[[self destinationPath] stringByAppendingPathComponent:item.imageThumb] stringByAppendingFormat:@"_thumb"]
	                       stringByAppendingPathExtension:kPreferencesFilePng];
	NSString *normalPath = [[[self destinationPath] stringByAppendingPathComponent:item.image] stringByAppendingPathExtension:kPreferencesFilePng];
  
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:thumbPath]];
  [self addSkipBackupAttributeToItemAtURL:[ NSURL fileURLWithPath:normalPath]];
    UIImage *imageToDisplay = [UIImage imageWithCGImage:[image CGImage]
                                                  scale:1.0
                                            orientation:item.isLandscapeImage ? UIImageOrientationLeft : UIImageOrientationUp];
    
    UIImage *thumbToDisplay = [UIImage imageWithCGImage:[imageThumb CGImage]
                                                  scale:1.0
                                            orientation:item.isLandscapeImage ? UIImageOrientationLeft : UIImageOrientationUp];

	[UIImageJPEGRepresentation(imageToDisplay, 1.0) writeToFile:normalPath atomically:NO];
	[UIImageJPEGRepresentation(thumbToDisplay, 1.0) writeToFile:thumbPath atomically:NO];
  
	for (NSDictionary *tagDict in tags) {
		LMTag *tag = [self tagWithNative:tagDict[kParserTagTextNative] localized:tagDict[kParserTagTextLocalized]];
		if (tag) {
			[tag.items addObject:item.uuid];
			self.needsSync |= [tag.items containsObject:item.uuid];
			[item.items addObject:tag.uuid];
			self.needsSync |= [item.items containsObject:tag.uuid];
		}
	}
	return item;
}

- (BOOL)addItem:(LMItem *)item {
	[self.items addObject:item];
	self.needsSync |= [self.items containsObject:item];
	if (self.needsSync) {
		[self synchronize];
	}
	return [self.items containsObject:item];
}

- (BOOL)addImage:(UIImage *)sourceImage withTags:(NSArray *)tags {
	UIImage *image = [sourceImage resizedImageWithMaximumSize:kImageNormalSize];
	UIImage *imageThumb = [sourceImage cropAndResizeto:kImageThumbSize];
  
	LMItem *item = [LMItem new];
	item.uuid = [NSString getUUID];
	item.image = item.uuid;
	item.imageThumb = item.uuid;
	item.items = [NSMutableArray array];
	item.userGenerated = YES;
	item.canShare = YES;
	item.defaultImage = NO;
    item.msisdn = [USER_MANAGER userMsisdn];
    item.dateTaken = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000] doubleValue];
    item.isLandscapeImage = item.isLandscapeImage;
	NSString *thumbPath = [[[[self destinationPath] stringByAppendingPathComponent:item.imageThumb] stringByAppendingFormat:@"_thumb"]
	                       stringByAppendingPathExtension:kPreferencesFilePng];
	NSString *normalPath = [[[self destinationPath] stringByAppendingPathComponent:item.image] stringByAppendingPathExtension:kPreferencesFilePng];
  
	[UIImageJPEGRepresentation(image, 1.0) writeToFile:normalPath atomically:NO];
	[UIImageJPEGRepresentation(imageThumb, 1.0) writeToFile:thumbPath atomically:NO];
  
	for (NSDictionary *tagDict in tags) {
		LMTag *tag = [self tagWithNative:tagDict[kParserTagTextNative] localized:tagDict[kParserTagTextLocalized]];
		if (tag) {
			[tag.items addObject:item.uuid];
			self.needsSync |= [tag.items containsObject:item.uuid];
			[item.items addObject:tag.uuid];
			self.needsSync |= [item.items containsObject:tag.uuid];
		}
	}
  
	[self.items addObject:item];
	self.needsSync |= [self.items containsObject:item];
	if (self.needsSync) {
		[self synchronize];
	}
	return [self.items containsObject:item];
}

- (BOOL)addImageFromGoogle:(UIImage *)sourceImage withTags:(NSArray *)tags {
	UIImage *image = [sourceImage resizedImageWithMaximumSize:kImageNormalSize];
	UIImage *imageThumb = [sourceImage cropAndResizeto:kImageThumbSize];
  
	LMItem *item = [LMItem new];
	item.uuid = [NSString getUUID];
	item.image = item.uuid;
	item.imageThumb = item.uuid;
	item.items = [NSMutableArray array];
	item.userGenerated = NO;
	item.canShare = YES;
	item.defaultImage = NO;
    item.msisdn = [USER_MANAGER userMsisdn];
    item.dateTaken = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000] doubleValue];
    item.isLandscapeImage = item.isLandscapeImage;
	NSString *thumbPath = [[[[self destinationPath] stringByAppendingPathComponent:item.imageThumb] stringByAppendingFormat:@"_thumb"]
	                       stringByAppendingPathExtension:kPreferencesFilePng];
	NSString *normalPath = [[[self destinationPath] stringByAppendingPathComponent:item.image] stringByAppendingPathExtension:kPreferencesFilePng];
  
	[UIImageJPEGRepresentation(image, 1.0) writeToFile:normalPath atomically:NO];
	[UIImageJPEGRepresentation(imageThumb, 1.0) writeToFile:thumbPath atomically:NO];
  
	for (NSDictionary *tagDict in tags) {
		LMTag *tag = [self tagWithNative:tagDict[kParserTagTextNative] localized:tagDict[kParserTagTextLocalized]];
		if (tag) {
			[tag.items addObject:item.uuid];
			self.needsSync |= [tag.items containsObject:item.uuid];
			[item.items addObject:tag.uuid];
			self.needsSync |= [item.items containsObject:tag.uuid];
		}
	}
  
	[self.items addObject:item];
	self.needsSync |= [self.items containsObject:item];
	if (self.needsSync) {
		[self synchronize];
	}
	return [self.items containsObject:item];
}

- (BOOL)removeItem:(LMItem *)item {
	if (![self.items containsObject:item]) {
		// the object is not contained
		// nothing to delete
		return YES;
	}
  
	NSArray *tagsUUIDs = [item.items copy];
	item.items = [NSMutableArray array];
	for (NSString *tagUUID in tagsUUIDs) {
		LMTag *tag = [self tagWithId:tagUUID];
		[tag.items removeObject:item.uuid];
		if (![tag.items count]) {
			[self.tags removeObject:tag];
			self.needsSync |= ![self.tags containsObject:tag];
		}
	}
	[self.items removeObject:item];
	self.needsSync |= ![self.items containsObject:item];
	if (self.needsSync) {
		[self synchronize];
	}
	return ![self.items containsObject:item];
}

- (NSMutableArray *)getSortedItems
{
    
    NSPredicate *defailtImagesPredicate = [NSPredicate predicateWithFormat:@"defaultImage = YES"];
    NSPredicate *userImagesPredicate = [NSPredicate predicateWithFormat:@"msisdn = %@",[USER_MANAGER userMsisdn]];
    
    NSArray *defaultImages = [self.items filteredArrayUsingPredicate:defailtImagesPredicate];
    NSArray *userImages = [self.items filteredArrayUsingPredicate:userImagesPredicate];
    NSMutableArray *resultArray = [[defaultImages arrayByAddingObjectsFromArray:userImages] mutableCopy];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTaken" ascending:NO];
    NSMutableArray *filteredImages = [[[resultArray mutableCopy] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] mutableCopy];
    return filteredImages;
}

@end
