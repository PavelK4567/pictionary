//  LMItemManager.h
//  Created by Dimitar Tasev on 07.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@class LMItem;
@class LMTag;


@interface LMItemManager : NSObject

- (LMItem *)makeItemWithImage:(UIImage *)sourceImage
                     withTags:(NSArray *)tags
             isLandscapeImage:(BOOL)isLandscapeImageOrientation;

- (BOOL)addItem:(LMItem *)item;
- (BOOL)addImage:(UIImage *)image withTags:(NSArray *)tags;
- (BOOL)addImageFromGoogle:(UIImage *)sourceImage withTags:(NSArray *)tags;

- (BOOL)removeItem:(LMItem *)item;

- (NSString *)destinationPath;

- (LMItem *)itemAtIndex:(NSInteger)index;
- (NSInteger)itemCount;
- (NSString *)itemAtIndex:(NSInteger)index matching:(NSString *)tag;
- (NSInteger)itemCountMatching:(NSString *)tag;
- (NSMutableArray *)getSortedItems;
- (LMTag *)tagWithId:(NSString *)uuid;
- (LMTag *)tagAtIndex:(NSInteger)index;
- (NSInteger)tagCount;
- (NSString *)tagAtIndex:(NSInteger)index matching:(NSString *)tag;
- (NSInteger)tagCountMatching:(NSString *)tag;

@end


#ifdef ITEM_SINGLETON
@interface LMItemManager ()
+ (id)sharedInstance;
- (void)synchronize;
@end

#define ITEM_MANAGER ((LMItemManager *)[LMItemManager sharedInstance])
#endif
