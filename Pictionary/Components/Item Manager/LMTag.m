//  LMTag.m
//  Created by Dimitar Tasev on 14.07.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMTag.h"


@implementation LMTag


+ (id)newWithDictionary:(NSDictionary *)dict
{
  LMTag *tag = [LMTag new];
  if (tag) {
    tag.uuid = dict[kParserTagUUID];
    if (tag.uuid) {
      tag.native = dict[kParserTagTextNative];
      tag.localized = dict[kParserTagTextLocalized];
      tag.items = [dict[kParserItems] mutableCopy];
      return tag;
    }
  }
  return 0;
}

- (id)copyWithZone:(NSZone *)zone
{
  LMTag *copy = [LMTag new];
  if (copy) {
    [copy setUuid:self.uuid];
    [copy setNative:self.native];
    [copy setLocalized:self.localized];
  }
  return copy;
}

- (NSDictionary *)dictionary
{
  return @{ kParserTagUUID : self.uuid, kParserTagTextNative : self.native, kParserTagTextLocalized : self.localized, kParserItems : self.items };
}

@end
