//  PurchaseManager.m
//  Created by Dimitar Tasev on 10.08.14.
//  Copyright (c) 2014 hAcx. All rights reserved.


#import "PurchaseManager.h"
#import "UIApplication-UIID.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";


@interface PurchaseManager () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end


@implementation PurchaseManager {
  SKProductsRequest *_productsRequest;
  RequestProductsCompletionHandler _completionHandler;
  NSSet *_productIdentifiers;
  NSMutableSet *_purchasedProductIdentifiers;
}


SINGLETON_GCD(PurchaseManager)


- (id)init {
  return [self initWithProductIdentifiers:$(kMonthlyPurchaseId)];
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
  if ((self = [super init])) {
    _productIdentifiers = productIdentifiers;
    _purchasedProductIdentifiers = [NSMutableSet set];
    NSString *uuid = [[UIApplication sharedApplication] uniqueInstallationIdentifier];
    for (NSString *productIdentifier in _productIdentifiers) {
      NSString *productIdValue = [[NSUserDefaults standardUserDefaults] objectForKey:productIdentifier];
      BOOL productPurchased = productIdValue && [productIdValue isEqualToString:uuid];
      if (productPurchased) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:[productIdentifier stringByAppendingString:@"_"]] isAfter:[NSDate date]]) {
          [_purchasedProductIdentifiers addObject:productIdentifier];
          NSLog(@"Previously purchased: %@", productIdentifier);
        }
        else {
          NSLog(@"Previously purchased (expired): %@", productIdentifier);
          [SDCloudUserDefaults removeObjectForKey:[productIdentifier stringByAppendingString:@"_"]];
          [SDCloudUserDefaults removeObjectForKey:productIdentifier];
          [SDCloudUserDefaults synchronize];
        }
      }
      else {
        NSLog(@"Not purchased: %@", productIdentifier);
      }
    }
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
  }
  return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
  _completionHandler = [completionHandler copy];
  _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
  _productsRequest.delegate = self;
  [_productsRequest start];
}

- (BOOL)productPurchased:(NSString *)productIdentifier {
  BOOL productPurchased =
  [[SDCloudUserDefaults objectForKey:productIdentifier] isEqualToString:[[UIApplication sharedApplication] uniqueInstallationIdentifier]];
  if (productPurchased) {
    NSLog(@"productPurchased %@", [[NSUserDefaults standardUserDefaults] objectForKey:[productIdentifier stringByAppendingString:@"_"]]);
    if ([[SDCloudUserDefaults objectForKey:[productIdentifier stringByAppendingString:@"_"]] isAfter:[NSDate date]]) {
      return YES;
    }
    else {
      [SDCloudUserDefaults removeObjectForKey:[productIdentifier stringByAppendingString:@"_"]];
      [SDCloudUserDefaults removeObjectForKey:productIdentifier];
      [SDCloudUserDefaults synchronize];
      [_purchasedProductIdentifiers removeObject:productIdentifier];
    }
  }
  return NO;
}

- (void)buyProduct:(SKProduct *)product {
  SKPayment *payment = [SKPayment paymentWithProduct:product];
  [[SKPaymentQueue defaultQueue] addPayment:payment];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
  _productsRequest = nil;
  
  NSArray *skProducts = response.products;
  for (SKProduct *skProduct in skProducts) {
    NSLog(@"Found product: %@ %@ %0.2f", skProduct.productIdentifier, skProduct.localizedTitle, skProduct.price.floatValue);
  }
  
  _completionHandler(YES, skProducts);
  _completionHandler = nil;
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
  _productsRequest = nil;
  _completionHandler(NO, nil);
  _completionHandler = nil;
}

#pragma mark SKPaymentTransactionOBserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
  // dali treba ova
  for (SKPaymentTransaction *transaction in transactions) {
    switch (transaction.transactionState) {
      case SKPaymentTransactionStatePurchased:
        [self completeTransaction:transaction];
        break;
        
      case SKPaymentTransactionStateFailed:
        [self failedTransaction:transaction];
        break;
        
      case SKPaymentTransactionStateRestored:
        [self restoreTransaction:transaction];
        
      default:
        break;
    }
  }
}

#pragma mark -

// Sent when transactions are removed from the queue (via finishTransaction:).
- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions {
}

// Sent when an error is encountered while adding transactions from the user's purchase history back to the queue.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
}

// Sent when all transactions from the user's purchase history have successfully been added back to the queue.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
  NSLog(@"received restored transactions: %i", queue.transactions.count);
  for (SKPaymentTransaction *transaction in queue.transactions) {
    [self restoreTransaction:transaction];
  }
}

// Sent when the download state has changed.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads {
}

#pragma mark -

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
  NSLog(@"completeTransaction");
  /*if (![userProfileLogic() isNativeUser]) {
   [userProfileLogic()
   registerNativeUser:[[UIApplication sharedApplication] uniqueInstallationIdentifier]
   completion:^{
   [userProfileLogic() retrieveStatusForNativeUser];
   [userProfileLogic() performSelector:@selector(retrieveStatusForNativeUser) withObject:0 afterDelay:1.000];
   [userProfileLogic() performSelector:@selector(silentlyRegisterForPushNotification:) withObject:[NSNumber numberWithBool:YES] afterDelay:1.000];
   [userProfileLogic() performSelector:@selector(retrieveStatusForNativeUser) withObject:0 afterDelay:2.000];
   }];
   } else {
   NSString *uid = [[UIApplication sharedApplication] uniqueUserIdentifier];
   if (!userProfileLogic().user) {
   userProfileLogic().user = [User new];
   userProfileLogic().user.userId = uid;
   }
   if (!userProfileLogic().user.userId) {
   userProfileLogic().user.userId = uid;
   }
   [userProfileLogic() registerBillingForNativeUser:userProfileLogic().user.userId
   priceCode:kMonthlyPurchaseIdShort
   completion:^{
   [userProfileLogic() retrieveStatusForNativeUser];
   [userProfileLogic() performSelector:@selector(retrieveStatusForNativeUser) withObject:0 afterDelay:1.000];
   [userProfileLogic() performSelector:@selector(silentlyRegisterForPushNotification:)
   withObject:[NSNumber numberWithBool:YES]
   afterDelay:1.000];
   [userProfileLogic() performSelector:@selector(retrieveStatusForNativeUser) withObject:0 afterDelay:2.000];
   }];
   }
   */
  if ([[[transaction.transactionDate dateAtEndOfDay] dateByAddingTimeInterval:7 * D_DAY] isAfter:[NSDate date]]) {
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier date:transaction.transactionDate];
    // NSString *uuid = [[UIApplication sharedApplication] uniqueInstallationIdentifier];
    //[Flurry logEvent:@"onAppleSuccessfulSubscribe" withParameters:@{ @"uuid" : uuid }];
  }
  [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
  NSLog(@"NSUserDefaults standardUserDefaults] objectForKey:uid %@ ", [[NSUserDefaults standardUserDefaults] objectForKey:@"uid"]);
  //[userProfileLogic() retrieveStatusForNativeUser];
  if ([[[transaction.originalTransaction.transactionDate dateAtEndOfDay] dateByAddingTimeInterval:30 * D_DAY] isAfter:[NSDate date]]) {
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier date:transaction.originalTransaction.transactionDate];
  }
  [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
  [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:transaction.payment.productIdentifier userInfo:nil];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
  if (transaction.error.code != SKErrorPaymentCancelled) {
    NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
  }
  NSString *uuid = [[UIApplication sharedApplication] uniqueInstallationIdentifier];
  [Flurry logEvent:kOnAppleUnsuccessfulSubscribe withParameters:@{ @"uuid" : uuid }];
  
  [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
  [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:transaction.payment.productIdentifier userInfo:nil];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier date:(NSDate *)purchaseDate {
  /*NSLog(@"Completing purchase for %@ as %@", productIdentifier, [[UIApplication sharedApplication] uniqueInstallationIdentifier]);*/
  [_purchasedProductIdentifiers addObject:productIdentifier];
  NSDate *expirationDate = [[purchaseDate dateAtEndOfDay] dateByAddingTimeInterval:7 * D_DAY];
  [SDCloudUserDefaults setObject:expirationDate forKey:[productIdentifier stringByAppendingString:@"_"]];
  [SDCloudUserDefaults setObject:[[UIApplication sharedApplication] uniqueInstallationIdentifier] forKey:productIdentifier];
  [SDCloudUserDefaults synchronize];
  [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
}

- (void)restoreCompletedTransactions {
  [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}
#pragma mark Login User
- (void)loginUser {
  [USER_MANAGER initializeWithUser];
  [APP_DELEGATE buildMainStack];
}
@end
