//  LMUserManager.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMUserManager.h"


@interface LMUserManager ()

@property(strong, nonatomic) LMUser *user;
@property(strong, nonatomic) NSMutableDictionary *tags;

- (NSString *)lastUsedMSISDN;

- (void)prepareGuestFolders;
- (void)prepareUserFolders:(NSString *)msisdn;
- (void)purgeUserFolders;

@end


@implementation LMUserManager

SINGLETON_GCD(LMUserManager)

- (void)configureAsGuest {
  self.user = 0;
  self.tags = 0;
  self.user = [[LMUser alloc] initWithMSISDN:@""];
  self.tags = [NSMutableDictionary dictionary];
}

- (void)configureWithMSISDN:(NSString *)msisdn {
  LMUser *userToSet = 0;
  NSString *lastUsedMSISDN = [self lastUsedMSISDN];
  if (lastUsedMSISDN && ![lastUsedMSISDN isEqualToString:msisdn]) {
    [self purgeUserFolders];
    userToSet = [[LMUser alloc] initWithMSISDN:msisdn];
  } else {
    // userToSet =
    // restore from preferences
  }
  [self prepareUserFolders:msisdn];
  [[NSUserDefaults standardUserDefaults] setObject:msisdn forKey:kLastValidMSISDN];
  [[NSUserDefaults standardUserDefaults] synchronize];
  self.user = userToSet;
}

- (BOOL)isAuthenticated {
  return 0 != self.user;
}

- (BOOL)isUser {
  return self.user.status == kUserStatusActive || self.user.status == kUserStatusToBeDeactivated;
}

- (BOOL)isGuest {
  return self.user.status == kUserStatusGuest;
}

- (BOOL)canScan {
  return ((self.user.status == kUserStatusGuest && self.user.totalCount < kCamfindSearchesForGuest) ||
          ((self.user.status == kUserStatusActive || self.user.status == kUserStatusToBeDeactivated) && self.user.weeklyCount < kCamfindSearchesForUser) ||
          self.user.totalCount < kCamfindSearchesForOther);
}

- (BOOL)canGoogle {
  // currently no the same conditions apply
  return [self canScan];
}

- (NSString *)lastUsedMSISDN {
  return [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidMSISDN];
}

- (void)prepareGuestFolders {
  NSString *guestPath = [[NSString documentsDir] stringByAppendingPathComponent:kGuestPath];
  BOOL isDirectory = NO;
  if (![[NSFileManager defaultManager] fileExistsAtPath:guestPath isDirectory:&isDirectory]) {
    if ([[NSFileManager defaultManager] fileExistsAtPath:guestPath]) {
      [[NSFileManager defaultManager] removeItemAtPath:guestPath error:0];
    }
    [[NSFileManager defaultManager] createDirectoryAtPath:guestPath withIntermediateDirectories:YES attributes:0 error:0];
  }
}

- (void)prepareUserFolders:(NSString *)msisdn {
}

- (void)purgeUserFolders {
  NSString *lastMSISDN = [self lastUsedMSISDN];
  if (lastMSISDN && ![lastMSISDN isEqualToString:@""]) {
    NSError *error = 0;
    [[NSFileManager defaultManager] removeItemAtPath:[[NSString documentsDir] stringByAppendingPathComponent:lastMSISDN] error:&error];
    if (error) {
      DLog(@"Error.  Couldn't delete the user data for %@", lastMSISDN);
    }
  }
}

- (NSString *)pathForGoogleImages {
  NSString *pathExtension = 0;
  if ([self isGuest]) {
    pathExtension = kGuestPath;
  } else {
    pathExtension = [self lastUsedMSISDN];
  }
  if (!pathExtension || [pathExtension isEqualToString:@""]) {
    return 0;
  }
  return [[[NSString documentsDir] stringByAppendingPathComponent:pathExtension] stringByAppendingPathComponent:@"Google"];
}

- (NSString *)pathForUserImages {
  NSString *pathExtension = 0;
  if ([self isGuest]) {
    pathExtension = kGuestPath;
  } else {
    pathExtension = [self lastUsedMSISDN];
  }
  if (!pathExtension || [pathExtension isEqualToString:@""]) {
    return 0;
  }
  return [[[NSString documentsDir] stringByAppendingPathComponent:pathExtension] stringByAppendingPathComponent:@"Images"];
}

- (NSDictionary *)tags {
  if (!self.tags) {
    NSString *key = [self isGuest] ? kGuestPath : kUserPath;
    NSDictionary *storedTags = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    self.tags = [NSMutableDictionary dictionary];
  }
  return self.tags;
}

@end
