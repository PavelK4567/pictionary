//  LMUserManager.h
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMUser.h"


@interface LMUserManager : NSObject

+ (id)sharedInstance;

- (BOOL)isAuthenticated;
- (BOOL)isUser;
- (BOOL)isGuest;
- (BOOL)canScan;
- (BOOL)canGoogle;

- (LMUser *)user;
- (NSDictionary *)tags;
- (NSString *)pathForGoogleImages;
- (NSString *)pathForUserImages;

- (void)configureAsGuest;
- (void)configureWithMSISDN:(NSString *)msisdn;

@end

#define USER_MANAGER ((LMUserManager *)[LMUserManager sharedInstance])
