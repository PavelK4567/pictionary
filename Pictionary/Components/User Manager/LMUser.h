//  LMUser.h
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMUser : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *msisdn;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *countryCode;
@property (strong, nonatomic) NSString *areaCode;
@property (strong, nonatomic) NSDate *billingDate;
@property (assign, nonatomic) KAUserStatus status;

@property (assign, nonatomic) NSInteger numberOfCredits;
@property (assign, nonatomic) NSInteger numberOfCreditsGoogle;
@property (assign, nonatomic) NSInteger isNative;

@property (assign, nonatomic) NSInteger totalCount;
@property (assign, nonatomic) NSInteger weeklyCount;
@property (assign, nonatomic) NSInteger availableCount;
@property (strong, nonatomic) NSDate *availableStartDate;
@property (strong, nonatomic) NSDate *availableEndDate;

- (id)initWithMSISDN:(NSString *)msisdn;

- (id)initWithContentsOfFile:(NSString *)filePath;

- (void)synchronizeToFile;
- (void)synchronizeNAtiveUserToFile;

- (id)initNativeUser:(NSString *)msisdn;

@end
