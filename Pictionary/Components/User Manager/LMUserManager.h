//  LMUserManager.h
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMUser.h"
#import "PurchaseManager.h"


@interface LMUserManager : NSObject

typedef void (^ManagerResultBlock)(NSError *error);

- (BOOL)processAutologin;
- (BOOL)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code;
- (BOOL)takeLoginTokenWithMSISDN:(NSString *)msisdn;
- (NSNumber *)showMeTokenForMSISDN:(NSString *)msisdn;
- (void)registerNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)buyPurchases;
- (void)logOutUser;


- (void)makeBillingForNativeUserWithCompletitionBlock:(ManagerResultBlock)completitionBlock;

- (NSString *)isNextBillingDateDay;
- (BOOL)isAuthenticated;
- (BOOL)isGuest;
- (BOOL)isInitialized;
- (BOOL)isNative;
- (BOOL)chekUserStatus:(int)currentUserStatus;

- (BOOL)canGoogle;
- (BOOL)canRecognize;
- (void)UserStatus;
- (KAUserStatus)getUserStatus;
- (NSString *)userMsisdn;
- (NSString *)userPhone;

- (void)initializeWithGuest;
- (void)initializeWithUser;
- (void)initializeWithUser:(LMUser *)user;
- (void)initializeWithNativeUser;

- (void)setGuestStatus;

- (NSString *)statusDescription:(KAUserStatus)status;
- (NSString *)loginConfigPath:(LMUser *)user;
- (NSString *)defaultPrefix;
- (NSString *)defaultMSISDN;

- (NSInteger)numberOfCredits;

- (void)takeCredit;
- (void)takeGoogleCredits;

- (NSString *)deviceIdentifier;
- (NSString *)takeStringFromDate;
- (NSString *)takeNativeDateStringFromDate;
- (NSString*)nextBillingDateString;
- (NSString *)takeNativeDateStringForSettings;

@end


#ifdef USER_SINGLETON

@interface LMUserManager ()
+ (id)sharedInstance;
@end

#define USER_MANAGER ((LMUserManager *)[LMUserManager sharedInstance])
#endif
