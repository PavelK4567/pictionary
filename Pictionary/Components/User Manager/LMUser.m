//  LMUser.m
//  Created by Dimitar Tasev on 25.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMUser.h"


@implementation LMUser

- (id)initWithContentsOfFile:(NSString *)filePath {
	self = [self init];
	if (self) {
		// process
	}
	return self;
}

- (id)initWithMSISDN:(NSString *)msisdn {
	self = [self init];
	if (self) {
		self.msisdn = msisdn;
		NSInteger prefixLength = 2; //[[USER_MANAGER defaultPrefix] length];
		self.number = [msisdn substringFromIndex:prefixLength];
		self.areaCode = [self.number substringToIndex:2];
		self.name = @"";
		self.countryCode = [COUNTRY_MANAGER countryCodeFromMSISDN:msisdn];
		self.status = ([self.number isEqualToString:kUserManagerDefault]) ? KAUserGuest : KAUserStatusNotRegistered;
	}
	return self;
}

- (id)initNativeUser:(NSString *)msisdn {
  self = [self init];
  if (self) {
    self.msisdn = msisdn;
    self.number = @"";
    self.areaCode = @"";
    self.name = @"";
    self.countryCode = @"";
    self.status = KAUserStatusNotRegistered;
    self.isNative = 1;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"makeNativeUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
  return self;
}

- (void)synchronizeToFile {
	if ([self.number isEqualToString:@""] || [self.number isEqualToString:kUserManagerDefault]) {
		// guest
		// NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"guest/%@.plist", self.msisdn]];
		/*[[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDNPath];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDN];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidUniqueID];
     [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
     [[NSUserDefaults standardUserDefaults] synchronize];*/
	}
	else {
		// user
		NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
		[[NSUserDefaults standardUserDefaults] setObject:path forKey:kLastValidMSISDNPath];
		[[NSUserDefaults standardUserDefaults] setObject:self.msisdn forKey:kLastValidMSISDN];
		[[NSUserDefaults standardUserDefaults] setInteger:self.status forKey:kLastValidUserStatus];
		[[NSUserDefaults standardUserDefaults] setObject:self.billingDate forKey:kLastValidBillingDate];
		[[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kLastValidLoginUser];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}
- (void)synchronizeNAtiveUserToFile {
    NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
    [[NSUserDefaults standardUserDefaults] setObject:path forKey:kLastValidMSISDNPath];
    [[NSUserDefaults standardUserDefaults] setObject:self.msisdn forKey:kLastValidMSISDN];
    [[NSUserDefaults standardUserDefaults] setInteger:self.status forKey:kLastValidUserStatus];
    [[NSUserDefaults standardUserDefaults] setObject:self.billingDate forKey:kLastValidBillingDate];
    [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
  
}

@end
