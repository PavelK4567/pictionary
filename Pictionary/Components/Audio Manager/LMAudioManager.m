//  GameAudioManager.m
//  Created by Dimitar Tasev in May 2014.
//  Copyright (c) 2012 Dimitar Tasev. All rights reserved.


#import "LMAudioManager.h"
#import <AVFoundation/AVFoundation.h>


@interface LMAudioManager () <AVAudioPlayerDelegate> {
	BOOL _audioEnabled;
}

@property (strong, nonatomic) AVAudioPlayer *backgroundPlayer;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) NSBundle *sounds;

@end


@implementation LMAudioManager


#ifdef AUDIO_SINGLETON
SINGLETON_GCD(LMAudioManager)
#endif


- (id)init {
	self = [super init];
	if (self) {
		_audioEnabled = YES;
		_backgroundPlayer = 0;
		_audioPlayer = 0;
		_sounds = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Sounds" ofType:@"bundle"]];
	}
	return self;
}

- (void)setEnabled:(BOOL)enabled {
	_audioEnabled = enabled;
	/*if (_audioEnabled) {
   [self playBackgroundMusic];
   } else {
   [self pauseBackgroundMusic];
   }*/
}

- (void)playBackgroundMusic {
	if (_audioEnabled) {
		NSError *error = 0;
		NSString *backgroundMusicPath = [self.sounds pathForResource:kSoundTypeBackground ofType:kSoundFileType];
		NSURL *backgroundMusicURL = [NSURL fileURLWithPath:backgroundMusicPath];
    
		self.backgroundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
		[self.backgroundPlayer setNumberOfLoops:-1];
		[self.backgroundPlayer play];
	}
}

- (void)pauseBackgroundMusic {
	if (self.backgroundPlayer) {
		[self.backgroundPlayer stop];
		[self setBackgroundPlayer:0];
	}
}

- (void)playSound:(ESoundType)type {
	if (_audioEnabled) {
		if (![_sounds isLoaded]) {
			[_sounds load];
		}
    
		NSString *effectPath = 0;
		switch (type) {
			case kSoundButtonTap:
				effectPath = [self.sounds pathForResource:kSoundTypeButtonTap ofType:kSoundFileType];
				break;
        
			case kSoundSwipe:
				effectPath = [self.sounds pathForResource:kSoundTypeSwipe ofType:kSoundFileType];
				break;
        
			case kSoundError:
				effectPath = [self.sounds pathForResource:kSoundTypeError ofType:kSoundFileType];
				break;
        
			case kSoundSuccess:
				effectPath = [self.sounds pathForResource:kSoundTypeSuccess ofType:kSoundFileType];
				break;
        
			case kSoundResults:
				effectPath = [self.sounds pathForResource:kSoundTypeResults ofType:kSoundFileType];
				break;
        
			case kSoundDeletePicture:
				effectPath = [self.sounds pathForResource:kSoundTypeDeletePicture ofType:kSoundFileType];
				break;
        
			case kSoundTakePicture:
				effectPath = [self.sounds pathForResource:kSoundTypeTakePicture ofType:kSoundFileType];
				break;
		}
    
		NSURL *effectURL = [NSURL fileURLWithPath:effectPath];
		SystemSoundID soundID;
		AudioServicesCreateSystemSoundID((__bridge CFURLRef)effectURL, &soundID);
		AudioServicesPlaySystemSound(soundID);
	}
}

- (BOOL)playAudio:(NSURL *)path {
	if (!_audioEnabled)
		return NO;
	if (self.audioPlayer.isPlaying) {
		DLog(@"Cannot play %@ since another file is playing at the moment.", path);
		return NO;
	}
  
	if (_audioEnabled) {
		self.backgroundPlayer.volume /= 5;
	}
  
	NSError *error = 0;
	self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:path error:&error];
	[self.audioPlayer setDelegate:self];
	[self.audioPlayer setNumberOfLoops:0];
	[self.audioPlayer play];
  
	if (error) {
		[self.audioPlayer stop];
		[self.audioPlayer setDelegate:0];
		[self setAudioPlayer:0];
	}
  
	return !error;
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	if (player == self.audioPlayer) {
		if (_audioEnabled) {
			self.backgroundPlayer.volume *= 5;
		}
	}
}

@end
