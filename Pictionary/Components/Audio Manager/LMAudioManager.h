//  GameAudioManager.h
//  Created by Dimitar Tasev in May 2014.
//  Copyright (c) 2012 Dimitar Tasev. All rights reserved.


typedef enum {
	kSoundButtonTap,
	kSoundSwipe,
	kSoundError,
	kSoundSuccess,
	kSoundResults,
	kSoundDeletePicture,
	kSoundTakePicture,
} ESoundType;


@interface LMAudioManager : NSObject

- (void)playBackgroundMusic;
- (void)pauseBackgroundMusic;
- (void)playSound:(ESoundType)type;
- (BOOL)playAudio:(NSURL *)path;
- (void)setEnabled:(BOOL)enabled;

@end


#ifdef AUDIO_SINGLETON

@interface LMAudioManager ()
+ (id)sharedInstance;
@end

#define AUDIO_MANAGER ((LMAudioManager *)[LMAudioManager sharedInstance])
#endif
