//  CountryManager.h
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


@interface LMCountryManager : NSObject

- (NSString *)countryNameFromMSISDN:(NSString *)msisdn;
- (NSString *)countryNameFromCode:(NSString *)code;
- (NSString *)countryCodeFromMSISDN:(NSString *)msisdn;
- (NSString *)countryCodeFromName:(NSString *)name;
- (NSString *)countryPrefixFromName:(NSString *)name;
- (NSString *)countryPrefixFromCode:(NSString *)code;

@end


#ifdef COUNTRY_SINGLETON

@interface LMCountryManager ()
+ (id)sharedInstance;
@end

#define COUNTRY_MANAGER ((LMCountryManager *)[LMCountryManager sharedInstance])
#endif
