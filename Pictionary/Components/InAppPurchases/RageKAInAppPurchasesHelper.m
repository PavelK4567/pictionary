//
//  RageKAInAppPurchasesHelper.m
//  Created by Kiril Kiroski in May 2014
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "RageKAInAppPurchasesHelper.h"

@implementation RageKAInAppPurchasesHelper

+ (RageKAInAppPurchasesHelper *)sharedInstance {
	static dispatch_once_t once;
	static RageKAInAppPurchasesHelper *sharedInstance;
	dispatch_once(&once, ^{
    NSSet *productIdentifiers =
    [NSSet setWithObjects:@"com.razeware.inapprage.drummerrage", @"com.razeware.inapprage.itunesconnectrage", @"com.razeware.inapprage.nightlyrage",
     @"com.razeware.inapprage.studylikeaboss", @"com.razeware.inapprage.updogsadness", nil];
    sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
	});
	return sharedInstance;
}

@end
