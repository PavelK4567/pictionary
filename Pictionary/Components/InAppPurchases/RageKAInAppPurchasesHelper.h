//
//  RageKAInAppPurchasesHelper.h
//  Created by Kiril Kiroski in May 2014
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "KAInAppPurchasesHelper.h"

@interface RageKAInAppPurchasesHelper : KAInAppPurchasesHelper

+ (RageKAInAppPurchasesHelper *)sharedInstance;

@end

#define PURCHASES_MANAGER ((RageKAInAppPurchasesHelper *)[RageKAInAppPurchasesHelper sharedInstance])
