//
//  LMSearchDatasource.m
//  Pictionary
//
//  Created by Darko Trpevski on 11/12/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import "LMSearchDatasource.h"

@implementation LMSearchDatasource

#pragma mark - MLPAutoCompleteTextField DataSource

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  possibleCompletionsForString:(NSString *)string
             completionHandler:(void (^)(NSArray *))handler
{
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
  dispatch_async(queue, ^{
    if (self.simulateLatency) {
      CGFloat seconds = arc4random_uniform(4) + arc4random_uniform(4);
      sleep(seconds);
    }

    NSArray *completions;
    completions = [self changeSearchString:string];
    handler(completions);
  });
}


#pragma mark - SearchWordsList

- (NSArray *)searchWordsList
{
  self.nameItems = nil;
  self.nameItems = [NSMutableArray new];

  for (NSInteger i = 0; i < [ITEM_MANAGER itemCount]; i++)
  {
      
    LMItem *item = [ITEM_MANAGER itemAtIndex:i];
    [self.nameItems addObject:item.localizedString];
    [self.nameItems addObject:item.nativeString];
      
    for (NSString *tagUUID in item.items)
    {
      if ([self isNotDuplicate:[[ITEM_MANAGER tagWithId:tagUUID] native]])
        [self.nameItems addObject:[[ITEM_MANAGER tagWithId:tagUUID] native]];
      if ([self isNotDuplicate:[[ITEM_MANAGER tagWithId:tagUUID] localized]])
        [self.nameItems addObject:[[ITEM_MANAGER tagWithId:tagUUID] localized]];
    }
  }

  return self.nameItems;
}
#pragma mark Items Results For input or selected Text

- (NSArray *)resultItemsForString:(NSString *)searchString
{
    
  NSMutableArray *resultItemsArray = [NSMutableArray new];

  for (NSInteger i = 0; i < [ITEM_MANAGER itemCount]; i++)
  {
    LMItem *item = [ITEM_MANAGER itemAtIndex:i];

    if ([item.nativeString rangeOfString:searchString].location != NSNotFound)
    {
      [resultItemsArray addObject:item];
    }
    else if ([item.localizedString rangeOfString:searchString].location != NSNotFound)
    {
      [resultItemsArray addObject:item];
    }
    else
    {
      for (NSString *tagUUID in [item.items mutableCopy])
      {
        if ([[[ITEM_MANAGER tagWithId:tagUUID] native] rangeOfString:searchString].location != NSNotFound)
        {
          [resultItemsArray addObject:item];
        }
        else if ([[[ITEM_MANAGER tagWithId:tagUUID] localized] rangeOfString:searchString].location != NSNotFound)
        {
          [resultItemsArray addObject:item];
        }
      }
    }
  }

  return [resultItemsArray mutableCopy];
}

#pragma mark - IsNotDuplicate

- (BOOL)isNotDuplicate:(NSString *)chekString
{
  for (NSString *name in [self.nameItems mutableCopy])
  {
    if ([name isEqualToString:chekString])
    {
      return NO;
    }
  }
  return YES;
}


#pragma mark - ChangeSearchString

- (NSArray *)changeSearchString:(NSString *)updateString
{
    
  self.itemsNamesForSearch = nil;
  self.itemsNamesForSearch = [NSMutableArray new];
  for (NSString *tempObjectName in [[self searchWordsList] mutableCopy])
  {
    if ([tempObjectName rangeOfString:updateString options:NSCaseInsensitiveSearch].location != NSNotFound)
      [self.itemsNamesForSearch addObject:tempObjectName];
  }

  NSSortDescriptor *sortDescriptor;
  sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"length" ascending:YES];
  NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];

  [self.itemsNamesForSearch sortUsingDescriptors:sortDescriptors];

  return [self.itemsNamesForSearch mutableCopy];
}


#pragma mark - HasSearchResult

- (BOOL)hasSearchResultsForString:(NSString *)searchString
{
  NSArray *result = [self resultItemsForString:searchString];
  BOOL hasItems = NO;

  for (NSString *tempObjectName in [[self searchWordsList] mutableCopy])
  {
    if([tempObjectName isEqualToString:searchString] && [result count] > 0)
      hasItems = YES;
  }
  return hasItems;
}


#pragma mark - GoogleAPICall

- (void)getGoogleImagesForString:(NSString *)searchString withCompletitionBlock:(GoogleImagesResultBlock)completition
{

  self.apiResultsArray = [NSMutableArray new];
  self.apiResultsTagsArray = [NSMutableArray new];


  __weak ASIHTTPRequest *request = [self request:searchString];

  [request setCompletionBlock:^{

    if (request.responseStatusCode == 478 && !request.error)
    {
      [UIAlertView alertWithCause:kAlertErrorTranslateWord];
      completition(nil, request.error);
    }
    else if (request.responseStatusCode == 200 && !request.error)
    {
      NSError *error = nil;
      NSString *jsonString = [[NSString alloc] initWithData:request.responseData encoding:NSUTF8StringEncoding];
      NSDictionary *responseDictionary =
        [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];

      NSArray *tagsResponseArray = [responseDictionary objectForKey:kPreferencesFileTags];
      NSArray *urlResponseArray = [responseDictionary objectForKey:kPreferencesFileUrls];

      for (int i = 0; i < [urlResponseArray count]; i++)
      {
        NSDictionary *object = [urlResponseArray objectAtIndex:i];
        NSDictionary *resultsDictionary = @{
          kParserTagImage : [object objectForKey:kParserTagImage],
          kParserTagThumbnail : [object objectForKey:kParserTagThumbnail],
          kParserTagOriginalImage : [object objectForKey:kParserTagOriginalImage]
        };

        [self.apiResultsArray addObject:resultsDictionary];
      }

      for (int i = 0; i < [tagsResponseArray count]; i++)
      {
        NSDictionary *object = [tagsResponseArray objectAtIndex:i];
        NSDictionary *tepmDictionary =
          @{ kParserTagTextLocalized : [object objectForKey:kParserTagTextNative], kParserTagTextNative : [object objectForKey:kParserTagTextTranslate] };
        [self.apiResultsTagsArray addObject:tepmDictionary];
      }

      if ([self.apiResultsArray count] > 0)
      {
        [USER_MANAGER takeGoogleCredits];
        [AUDIO_MANAGER playSound:kSoundResults];
        completition(self.apiResultsArray, nil);
      }
      else
      {
        [UIAlertView alertWithCause:kAlertGoogleImageAPINoResults];
        completition(self.apiResultsArray, nil);
      }
    }
    else
    {
      if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
      {
        [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
      }
      else
      {
        [UIAlertView alertWithCause:kAlertGoogleImageAPITechnicalError];
      }
      completition(self.apiResultsArray, nil);
    }
  }];

  [request setFailedBlock:^{

    if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled)
    {
      [UIAlertView alertWithCause:kAlertNoMoreCamFindCreditsForDisabledUser];
    }
    else
    {
      [UIAlertView alertWithCause:kAlertGoogleImageAPITechnicalError];
    }
    completition(self.apiResultsArray, request.error);
  }];

  [request startAsynchronous];
}


#pragma mark - ASIHTTPRequest

- (ASIHTTPRequest *)request:(NSString *)searchString
{

  NSDictionary *headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER deviceIdentifier], kSearchWords : searchString };
    
  if ([USER_MANAGER isGuest])
  {
    headers = @{ kUserMSISDN : @"guest", kUserDeviceId : [USER_MANAGER deviceIdentifier], kSearchWords : searchString };
  }
  if ([USER_MANAGER isNative])
  {
    headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER userMsisdn], kSearchWords : searchString };
  }
    
  ASIHTTPRequest *confirmImageRequest = [REQUEST_MANAGER post:kRequestGoogleResultsAPICall headers:headers];

  NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:searchString, @"words", nil];
  NSError *error = nil;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info options:NSJSONWritingPrettyPrinted error:&error];
  NSMutableData *body = [jsonData mutableCopy];
  
  [confirmImageRequest setPostBody:body];

  return confirmImageRequest;
}

#pragma mark -
@end
