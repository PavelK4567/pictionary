//
//  LMSearchDatasource.h
//  Pictionary
//
//  Created by Darko Trpevski on 11/12/14.
//  Copyright (c) 2014 La Mark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompleteTextFieldDataSource.h"
#import "MLPAutoCompleteTextFieldDataSource.h"
#import "LMItem.h"
#import "LMItemManager.h"
#import "LMTag.h"

@interface LMSearchDatasource : NSObject <MLPAutoCompleteTextFieldDataSource>

typedef void (^GoogleImagesResultBlock)(NSArray *result, NSError *error);

@property (strong, nonatomic) NSMutableArray *nameItems;
@property (strong, nonatomic) NSMutableArray *resultItems;
@property (strong, nonatomic) NSMutableArray *itemsNamesForSearch;
@property (strong, nonatomic) NSMutableArray *apiResultsArray;
@property (strong, nonatomic) NSMutableArray *apiResultsTagsArray;
@property (assign) BOOL simulateLatency;

- (NSMutableArray *)changeSearchString:(NSString *)updateString;
- (NSMutableArray *)resultItemsForString:(NSString *)searchString;
- (BOOL)hasSearchResultsForString:(NSString *)searchString;
- (void)getGoogleImagesForString:(NSString *)searchString
           withCompletitionBlock:(GoogleImagesResultBlock)completition;
@end
