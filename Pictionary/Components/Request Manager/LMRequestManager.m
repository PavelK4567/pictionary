//  LMRequestManager.m
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMRequestManager.h"
#import <ASIHTTP/ASIDownloadCache.h>


@implementation LMRequestManager


#ifdef REQUEST_SINGLETON
SINGLETON_GCD(LMRequestManager)
#endif


- (ASIHTTPRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSURL *url = [self urlForType:type headers:headers];
	ASIHTTPRequest *request;
	if (url) {
		request = [ASIHTTPRequest requestWithURL:url usingCache:[ASIDownloadCache sharedCache] andCachePolicy:ASIAskServerIfModifiedCachePolicy];
		NSArray *keys = [headers allKeys];
		for (NSString *key in keys) {
			[request addRequestHeader:key value:headers[key]];
			return request;
		}
	}
	return 0;
}

- (NSURL *)urlForType:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSString *path = 0;
	switch (type) {
		case kRequestLoginUserAPICall:
			path = [NSString stringWithFormat:@"%@/sendLoginToken?groupID=5&msisdn=%@", kApiBaseUrl, headers[kUserMSISDN]];
			break;
      
		case kRequestLoginTokenAPICall:
			path = [NSString stringWithFormat:@"%@/sendLoginToken?groupID=5&msisdn=%@", kApiBaseUrl, headers[kUserMSISDN]];
			break;
      
		case kRequestShowTokenAPICall:
			path = [NSString stringWithFormat:@"%@/getToken?msisdn=%@&groupID=5", kApiBaseUrl, headers[kUserMSISDN]];
			break;
      
      /*case kRequestConfirmLoginTokenAPICall:
		   path = [NSString stringWithFormat:@"%@/confirmLoginToken?msisdn=%@&siteID=23&productID=14&token=%@&OS=IOS&OSVersion=%@&PhoneModel=%@",
		   kApiBaseUrl,
		   [headers objectForKey:kUserMSISDN], [headers objectForKey:kUserCode], [[UIDevice currentDevice] systemVersion],
		   [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
		   break;*/
		case kRequestConfirmLoginTokenAPICall:
			path = [NSString stringWithFormat:@"%@/confirmLoginToken?groupID=5&msisdn=%@&token=%@&deviceID=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],
			        [headers objectForKey:kUserCode], [headers objectForKey:kUserDeviceId]];
			break;
      
		case kRequestGetUserStatusAPICall:
			path =
      [NSString stringWithFormat:@"%@/status?groupID=5&msisdn=%@&OS=IOS&PhoneModel=%@&Version=%@&deviceID=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],
       [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
       [[UIDevice currentDevice] systemVersion], [headers objectForKey:kUserDeviceId]];
			break;
      
    case kRequestGetUserNativeStatusAPICall:
        path =
      [NSString stringWithFormat:@"%@/status?groupID=4&msisdn=%@&OS=IOS&PhoneModel=%@&Version=%@&deviceID=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],
       [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
       [[UIDevice currentDevice] systemVersion], [headers objectForKey:kUserDeviceId]];
      break;
    case kRequestSubscriptionForNativeUserAPICall:
      path =
      [NSString stringWithFormat:@"%@/subscription?site=47&product=14&msisdn=%@&group=4&device=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],[headers objectForKey:kUserDeviceId]];
      break;
    case kRequestBillingForNativeUserAPICall:
      path =
      [NSString stringWithFormat:@"%@/billing?userUniqueID=%@&priceCode=%@",kApiBaseBillingUrl, [headers objectForKey:kUserMSISDN],kApiPriceCode];
      break;
		case kRequestLogoutUserAPICall:
			path = 0;
			break;
      
		case kRequestPicSeeResultsAPICall:
			// http://54.226.173.227:8080/PitionaryProxy/DoCamFindRequest?groupID=5&msisdn=1234567890&deviceID=1
      if([[headers objectForKey:kUserDeviceId] isEqualToString:[headers objectForKey:kUserMSISDN]]){
        path = [NSString
                stringWithFormat:@"%@?groupID=4&msisdn=%@&deviceID=%@", kApiCamFindUrl, [headers objectForKey:kUserMSISDN], [headers objectForKey:kUserDeviceId]];
      }else{
			path = [NSString
			        stringWithFormat:@"%@?groupID=5&msisdn=%@&deviceID=%@", kApiCamFindUrl, [headers objectForKey:kUserMSISDN], [headers objectForKey:kUserDeviceId]];
      }
			break;
      
		case kRequestGoogleResultsAPICall:
			// http://54.226.173.227:8080/PitionaryProxy/DoImageSearchRequest?groupID=5&msisdn=1234567890&deviceID=1&words=car%20cheeks.
      if([[headers objectForKey:kUserDeviceId] isEqualToString:[headers objectForKey:kUserMSISDN]]){
        path = [NSString stringWithFormat:@"%@?groupID=4&msisdn=%@&deviceID=%@&words=%@", kApiGoogleResultsUrl, [headers objectForKey:kUserMSISDN],
                [headers objectForKey:kUserDeviceId], [self encodeToPercentEscapeString:[headers objectForKey:kSearchWords]]];
      }else{
			path = [NSString stringWithFormat:@"%@?groupID=5&msisdn=%@&deviceID=%@&words=%@", kApiGoogleResultsUrl, [headers objectForKey:kUserMSISDN],
			        [headers objectForKey:kUserDeviceId], [self encodeToPercentEscapeString:[headers objectForKey:kSearchWords]]];
      }
        break;
      
		default:
			break;
	}
	if (path) {
		return [NSURL URLWithString:path];
	}
	return 0;
}
- (NSString *)encodeToPercentEscapeString:(NSString *)string {
	return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)string, NULL,
	                                                                             (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8);
}
@end
