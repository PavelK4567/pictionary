//  LMRequestManager.h
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


typedef enum {
	kRequestLoginUserAPICall,
	kRequestLoginTokenAPICall,
	kRequestShowTokenAPICall,
	kRequestConfirmLoginTokenAPICall,
	kRequestGetUserStatusAPICall,
  kRequestGetUserNativeStatusAPICall,
  kRequestSubscriptionForNativeUserAPICall,
  kRequestBillingForNativeUserAPICall,
	kRequestLogoutUserAPICall,
	kRequestPicSeeResultsAPICall,
	kRequestGoogleResultsAPICall,
} ELMRequestType;


@interface LMRequestManager : NSObject

- (ASIHTTPRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers;

@end


#ifdef REQUEST_SINGLETON

@interface LMRequestManager ()
+ (id)sharedInstance;
@end

#define REQUEST_MANAGER ((LMRequestManager *)[LMRequestManager sharedInstance])
#endif
