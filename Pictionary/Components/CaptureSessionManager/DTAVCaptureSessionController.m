//
//  DTAVCaptureSessionController.m
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 Darko Trpevski. All rights reserved.
//

#import "DTAVCaptureSessionController.h"
#import <ImageIO/ImageIO.h>


@implementation DTAVCaptureSessionController


@synthesize isBackCameraOn = _isBackCameraOn;


#pragma mark -


- (id)init
{
    self = [super init];
	if (self) {
        [self loadSessions];
        [self loadImageOutput];
        [self loadVideoPreviewLayer];
	}
	return self;
}


#pragma mark -


- (void)loadWithCaptureDevicePosition:(AVCaptureDevicePosition)position
{
    _isBackCameraOn = (position == AVCaptureDevicePositionBack);

    AVCaptureDeviceInput *deviceInputCamera = [self loadCaptureDeviceInputWithMediaType:AVMediaTypeVideo position:position];

    NSMutableArray *inputs = [_captureSession.inputs mutableCopy];
    for (AVCaptureDeviceInput *input in inputs)
        [_captureSession removeInput:input];

    if ([_captureSession canAddInput:deviceInputCamera])
        [_captureSession addInput:deviceInputCamera];
}


#pragma mark -


- (void)loadForView:(UIView *)view
{
    CGRect layerRect = view.bounds;
	[_previewLayer setBounds:layerRect];
	[_previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
	[view.layer addSublayer:_previewLayer];
}


#pragma mark -


- (void)startRunning
{
    [_captureSession startRunning];
}


- (void)stopRunning
{
    [_captureSession stopRunning];
}


#pragma mark -


- (void)loadSessions
{
    if (!_captureSession)
    {
        _captureSession = [[AVCaptureSession alloc] init];
        _captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    }
}


#pragma mark -


- (void)loadVideoPreviewLayer
{
    if (!_previewLayer)
    {
        _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
        [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    }
}


#pragma mark -


- (void)loadImageOutput
{
    if (!_imageOutput)
    {
        _imageOutput = [[AVCaptureStillImageOutput alloc] init];
        _imageOutput.outputSettings = @{ AVVideoCodecKey : AVVideoCodecJPEG };
    }

    NSMutableArray *outputs = [_captureSession.outputs mutableCopy];
    for (AVCaptureOutput *output in outputs)
        [_captureSession removeOutput:output];

    if ([_captureSession canAddOutput:_imageOutput])
    {
        [_captureSession addOutput:_imageOutput];
        [_captureSession startRunning];
    }
}


#pragma mark -


- (void)captureImageWithCompletionBlock:(void (^)(UIImage *image, NSError *error))completionBlock
{
    AVCaptureConnection *videoConnection = [DTAVCaptureSessionController connectionWithMediaType:AVMediaTypeVideo fromConnections:_imageOutput.connections];

    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [_captureSession beginConfiguration];
    [device lockForConfiguration:nil];
    if ([device isTorchModeSupported:AVCaptureTorchModeAuto])
    {
        [device setTorchMode:AVCaptureTorchModeAuto];
        [device setFlashMode:AVCaptureFlashModeAuto];
    }
    [device unlockForConfiguration];
    [_captureSession commitConfiguration];

    __weak AVCaptureSession *weakCaptureSession = _captureSession;
    [_imageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                              completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         UIImage *image = nil;

         if (error == nil)
         {
#ifdef DEBUG
            // CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
             //if (exifAttachments) NSLog(@"exifAttachments: %@", exifAttachments);
#endif

             NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
             if (imageData) image = [[UIImage alloc] initWithData:imageData];
         }

         dispatch_async(dispatch_get_main_queue(), ^{

             [weakCaptureSession stopRunning];

             if (completionBlock)
                 completionBlock(image, error);

         });
     }];
}


#pragma mark -


- (AVCaptureDeviceInput*)loadCaptureDeviceInputWithMediaType:(NSString*)mediaType position:(AVCaptureDevicePosition)position
{
    NSError *error = nil;
    AVCaptureDevice *captureDevice = [DTAVCaptureSessionController deviceForMediaType:mediaType position:position];
    AVCaptureDeviceInput *captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    return (!error) ? captureDeviceInput : nil;
}


#pragma mark -


+ (AVCaptureDevice*)deviceForMediaType:(NSString *)mediaType position:(AVCaptureDevicePosition)position
{
    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:mediaType])
        if ([device position] == position)
			return device;

    return nil;
}


#pragma mark -


+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections;
{
    for (AVCaptureConnection *connection in connections)
        for (AVCaptureInputPort *port in [connection inputPorts])
            if ([[port mediaType] isEqual:mediaType])
                return connection;
    return nil;
}


@end
