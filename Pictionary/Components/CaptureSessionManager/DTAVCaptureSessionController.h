//
//  DTAVCaptureSessionController.h
//  Pictionary
//
//  Created by Darko Trpevski on 11/11/14.
//  Copyright (c) 2014 Darko Trpevski. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface DTAVCaptureSessionController : NSObject
{
    AVCaptureStillImageOutput *_imageOutput;
    AVCaptureSession *_captureSession;
    AVCaptureVideoPreviewLayer *_previewLayer;
}

@property (nonatomic, assign) BOOL isBackCameraOn;

- (void)loadWithCaptureDevicePosition:(AVCaptureDevicePosition)position;

- (void)loadForView:(UIView *)view;

- (void)captureImageWithCompletionBlock:(void (^)(UIImage *image, NSError *error))completionBlock;

- (void)startRunning;

- (void)stopRunning;


@end
